/*********************************************************************************************************//**
 * @file    main.c
 * @brief   Main program.
 *************************************************************************************************************
 * @attention
 *
 * Firmware Disclaimer Information
 *
 * 1. The customer hereby acknowledges and agrees that the program technical documentation, including the
 *    code, which is supplied by Holtek Semiconductor Inc., (hereinafter referred to as "HOLTEK") is the
 *    proprietary and confidential intellectual property of HOLTEK, and is protected by copyright law and
 *    other intellectual property laws.
 *
 * 2. The customer hereby acknowledges and agrees that the program technical documentation, including the
 *    code, is confidential information belonging to HOLTEK, and must not be disclosed to any third parties
 *    other than HOLTEK and the customer.
 *
 * 3. The program technical documentation, including the code, is provided "as is" and for customer reference
 *    only. After delivery by HOLTEK, the customer shall use the program technical documentation, including
 *    the code, at their own risk. HOLTEK disclaims any expressed, implied or statutory warranties, including
 *    the warranties of merchantability, satisfactory quality and fitness for a particular purpose.
 *
 * <h2><center>Copyright (C) Holtek Semiconductor Inc. All rights reserved</center></h2>
 ************************************************************************************************************/

/* Includes ------------------------------------------------------------------------------------------------*/
#include "ht32.h"
/* User lib */
 #include "All_User_Lib.h"

/* Settings ------------------------------------------------------------------------------------------------*/
/* Private types -------------------------------------------------------------------------------------------*/
/* Private constants ---------------------------------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------------------------------------*/
//#define AP_ADDRESS (0x2000)
#define AP_ADDRESS (0)
/* Global variables ----------------------------------------------------------------------------------------*/
static uint8_t timeout1ms = 0, timeout10ms = 0, timeout100ms = 0, timeout1s = 0;
/* Private variables ---------------------------------------------------------------------------------------*/

/* Private function -----------------------------------------------------------------------------*/
void systickInterrupt(void); // 1ms

/* Global functions ----------------------------------------------------------------------------------------*/
/*********************************************************************************************************//**
  * @brief  Main program.
  * @retval None
  ***********************************************************************************************************/
int main(void)
{
	NVIC_SetVectorTable(NVIC_VECTTABLE_FLASH, AP_ADDRESS); /* Set Vector Table Offset                  */
	
	SysClockCfg_RegCallback(systickInterrupt);
	Gpio_Config();
	
	UART_Cfg(CONFIG_INDEX, 115200, USART_PARITY_NO, USART_WORDLENGTH_8B, USART_STOPBITS_1);
	UART_Cfg(RS232_INDEX, 115200, USART_PARITY_NO, USART_WORDLENGTH_8B, USART_STOPBITS_1);
	
	DEBUG("\r\n---------------AP---------------------\r\n");
	DEBUG("[MAIN] RST reason: %s\r\n", SystemClockCfg_GetResetReason());
	SysClockGetCK_AHB();
	
	EN25Q_Init();
	//Cellular_InitModule();
	
	WDT_Configuration(4000);
	
	NFC_Init();
	
	Buzzer_Init();
	
	DEBUG("[MAIN] Init done\r\n");
	while (1)                           /* Infinite loop                                                      */
  {
		if(timeout1ms == 1)
		{
			timeout1ms = 0;
			timeout10ms++;		
			AT_Process();
			NFC_Process();
		}
		
		if(timeout10ms >= 10)
		{
			timeout10ms = 0;
			timeout100ms++;
			UART_Process();
		}
		
		if(timeout100ms >= 10)
		{
			timeout100ms = 0;
			timeout1s++;
			
		}
		
		if(timeout1s >= 10)
		{
			timeout1s = 0;
			Gpio_Toggle_Pin(DEBUG_LED_PORT, DEBUG_LED_PIN);
			WDT_Restart();
			/***********************************************/
			//TCP_ManagerTask();
			//Cellular_ManagerTask();
		}
  }
}

/* Private functions ---------------------------------------------------------------------------------------*/
void systickInterrupt(void)
{
	timeout1ms = 1;
}
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

