/*
 * template.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef ALL_USER_LIB_H
#define ALL_USER_LIB_H

/* #include "All_User_Lib.h" */
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* Inclusion of User lib */
#include "systemClockCfg.h"
#include "gpioUser.h"
#include "PWM.h"
#include "spiUser.h"
#include "MemoryMap.h"
#include "EN25Qxxx.h"
#include "NFC_ISO15693.h"
#include "uartUser.h"
#include "uartConfigPort.h"
#include "uartInterfaces.h"
#include "AT_Command.h"
#include "CircularBuffer.h"
#include "cellular.h"
#include "TCP_HwLayer.h"
#include "TCP_DataLayer.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */

/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */

#endif
#ifdef __cplusplus
}
#endif
