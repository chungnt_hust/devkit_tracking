/*
 * spiUser.h
 *
 *  Created on: Nov 02, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef Spi_H
#define Spi_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */

/* Definition of public (external) data types go here */
typedef enum
{
	QSPI_INDEX = 0,
	SPI1_INDEX,
	MAX_SPI_INDEX
} spiIndex_t;

typedef void (*spiSelBitCfg)(CKCU_PeripClockConfig_TypeDef*);


typedef struct
{
	spiSelBitCfg clkFunc;
	/* ------------------------------ */
	HT_GPIO_TypeDef* HT_GPIOx;
	u16 GPIO_PIN;
	/* ------------------------------ */
	u32 AFIO_GPIOP_CLK;
	u32 AFIO_PIN_CLK;
	u32 AFIO_GPIOP_MOSI;
	u32 AFIO_PIN_MOSI;
	u32 AFIO_GPIOP_MISO;
	u32 AFIO_PIN_MISO;
	u32 AFIO_GPIOP_CS;
	u32 AFIO_PIN_CS;
	/* ------------------------------ */
	HT_SPI_TypeDef* SPIx;
} Spi_Use_Typedef;

/***************************  QSPI  ***************************************//**/
#define QSPI_SEL_GPIOX                    B
#define QSPI_SEL_GPION                    2
#define QSPI_SCK_GPIOX                    B
#define QSPI_SCK_GPION                    3
#define QSPI_MOSI_GPIOX                   B
#define QSPI_MOSI_GPION                   4
#define QSPI_MISO_GPIOX                   B
#define QSPI_MISO_GPION                   5
#define QSPI_IPN                         	QSPI

#define QSPI_SCK_GPIO_ID   								STRCAT2(GPIO_P,         QSPI_SCK_GPIOX)
#define QSPI_SCK_AFIO_PIN  								STRCAT2(AFIO_PIN_,      QSPI_SCK_GPION)
#define QSPI_MOSI_GPIO_ID  								STRCAT2(GPIO_P,         QSPI_MOSI_GPIOX)
#define QSPI_MOSI_AFIO_PIN 								STRCAT2(AFIO_PIN_,      QSPI_MOSI_GPION)
#define QSPI_MISO_GPIO_ID  								STRCAT2(GPIO_P,         QSPI_MISO_GPIOX)
#define QSPI_MISO_AFIO_PIN 								STRCAT2(AFIO_PIN_,      QSPI_MISO_GPION)
#define QSPI_SEL_GPIO_ID   								STRCAT2(GPIO_P,         QSPI_SEL_GPIOX)
#define QSPI_SEL_GPIO_PORT 								STRCAT2(HT_GPIO,      	QSPI_SEL_GPIOX)
#define QSPI_SEL_AFIO_PIN  								STRCAT2(AFIO_PIN_,      QSPI_SEL_GPION)
#define QSPI_SEL_GPIO_PIN  								STRCAT2(GPIO_PIN_,      QSPI_SEL_GPION)
#define QSPI_SEL_BIT_CLOCK								STRCAT2(P,      				QSPI_SEL_GPIOX)
#define QSPI_PORT													STRCAT2(HT_,            QSPI_IPN)

#define QSPI_SEL_ACTIVE    	SPI_SoftwareSELCmd(QSPI_PORT, SPI_SEL_ACTIVE);		
#define QSPI_SEL_INACTIVE   SPI_SoftwareSELCmd(QSPI_PORT, SPI_SEL_INACTIVE);

/***************************  SPI1  ***************************************//**/
#define SPI1_SEL_GPIOX                    C
#define SPI1_SEL_GPION                    4
#define SPI1_SCK_GPIOX                    C
#define SPI1_SCK_GPION                    5
#define SPI1_MOSI_GPIOX                   C
#define SPI1_MOSI_GPION                   8
#define SPI1_MISO_GPIOX                   C
#define SPI1_MISO_GPION                   9
#define SPI1_IPN                         	SPI1

#define SPI1_SCK_GPIO_ID   								STRCAT2(GPIO_P,         SPI1_SCK_GPIOX)
#define SPI1_SCK_AFIO_PIN  								STRCAT2(AFIO_PIN_,      SPI1_SCK_GPION)
#define SPI1_MOSI_GPIO_ID  								STRCAT2(GPIO_P,         SPI1_MOSI_GPIOX)
#define SPI1_MOSI_AFIO_PIN 								STRCAT2(AFIO_PIN_,      SPI1_MOSI_GPION)
#define SPI1_MISO_GPIO_ID  								STRCAT2(GPIO_P,         SPI1_MISO_GPIOX)
#define SPI1_MISO_AFIO_PIN 								STRCAT2(AFIO_PIN_,      SPI1_MISO_GPION)
#define SPI1_SEL_GPIO_ID   								STRCAT2(GPIO_P,         SPI1_SEL_GPIOX)
#define SPI1_SEL_GPIO_PORT 								STRCAT2(HT_GPIO,      	SPI1_SEL_GPIOX)
#define SPI1_SEL_AFIO_PIN  								STRCAT2(AFIO_PIN_,      SPI1_SEL_GPION)
#define SPI1_SEL_GPIO_PIN  								STRCAT2(GPIO_PIN_,      SPI1_SEL_GPION)
#define SPI1_SEL_BIT_CLOCK								STRCAT2(P,      				SPI1_SEL_GPIOX)
#define SPI1_PORT													STRCAT2(HT_,            SPI1_IPN)

#define SPI1_SEL_ACTIVE    	SPI_SoftwareSELCmd(SPI1_PORT, SPI_SEL_ACTIVE);		
#define SPI1_SEL_INACTIVE   SPI_SoftwareSELCmd(SPI1_PORT, SPI_SEL_INACTIVE);
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
//SPI_DATALENGTH_* * = 1 -> 16
void Spi_Configuration(spiIndex_t spiIndex, u32 spiDataLength, u8 mode);
u8 Spi_SendReceive(spiIndex_t spiIndex, u8 data);
void Spi_SendMulti(spiIndex_t spiIndex, u8 *p, u32 len);
void Spi_ReceiveMulti(spiIndex_t spiIndex, u8 *p, u32 len);
void Spi_SelfTest(void);
#endif
#ifdef __cplusplus
}
#endif
