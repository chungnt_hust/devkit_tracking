/*
 * spiUser.c
 *
 *  Created on: Nov 02, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "spiUser.h"

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static void clkSelBitQSPI(CKCU_PeripClockConfig_TypeDef *CKCUClock);
static void clkSelBitSPI1(CKCU_PeripClockConfig_TypeDef *CKCUClock);


/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
const Spi_Use_Typedef Spi_Use[MAX_SPI_INDEX] = 
{
	{clkSelBitQSPI, QSPI_SEL_GPIO_PORT, QSPI_SEL_AFIO_PIN, QSPI_SCK_GPIO_ID, QSPI_SCK_AFIO_PIN, QSPI_MOSI_GPIO_ID, QSPI_MOSI_AFIO_PIN, QSPI_MISO_GPIO_ID, QSPI_MISO_AFIO_PIN, QSPI_SEL_GPIO_ID, QSPI_SEL_AFIO_PIN, QSPI_PORT},
	{clkSelBitSPI1, SPI1_SEL_GPIO_PORT, SPI1_SEL_AFIO_PIN, SPI1_SCK_GPIO_ID, SPI1_SCK_AFIO_PIN, SPI1_MOSI_GPIO_ID, SPI1_MOSI_AFIO_PIN, SPI1_MISO_GPIO_ID, SPI1_MISO_AFIO_PIN, SPI1_SEL_GPIO_ID, SPI1_SEL_AFIO_PIN, SPI1_PORT}
};
/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void Spi_Configuration(spiIndex_t spiIndex, u32 spiDataLength, u8 mode)
{
  /* !!! NOTICE !!!
     Notice that the local variable (structure) did not have an initial value.
     Please confirm that there are no missing members in the parameter settings below in this function.
  */
  SPI_InitTypeDef SPI_InitStructure;

  CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
	/* Enable SEL Pin, Master, Slave & AFIO clock                                                                     */
	Spi_Use[spiIndex].clkFunc(&CKCUClock);
  CKCUClock.Bit.AFIO           = 1;
  CKCU_PeripClockConfig(CKCUClock, ENABLE);

  /* MASTER_SEL idle state is HIGH                                                                            */
  GPIO_PullResistorConfig(Spi_Use[spiIndex].HT_GPIOx, Spi_Use[spiIndex].GPIO_PIN, GPIO_PR_UP);
	
  AFIO_GPxConfig(Spi_Use[spiIndex].AFIO_GPIOP_CLK,  Spi_Use[spiIndex].AFIO_PIN_CLK,  AFIO_FUN_SPI);
  AFIO_GPxConfig(Spi_Use[spiIndex].AFIO_GPIOP_MOSI, Spi_Use[spiIndex].AFIO_PIN_MOSI, AFIO_FUN_SPI);
  AFIO_GPxConfig(Spi_Use[spiIndex].AFIO_GPIOP_MISO, Spi_Use[spiIndex].AFIO_PIN_MISO, AFIO_FUN_SPI);
	AFIO_GPxConfig(Spi_Use[spiIndex].AFIO_GPIOP_CS,   Spi_Use[spiIndex].AFIO_PIN_CS,   AFIO_FUN_SPI);

  SPI_InitStructure.SPI_Mode = SPI_MASTER;
  // SPI_InitStructure.SPI_FIFO = SPI_FIFO_ENABLE;
  SPI_InitStructure.SPI_FIFO = SPI_FIFO_DISABLE;
  SPI_InitStructure.SPI_DataLength = spiDataLength; 
  // SPI_InitStructure.SPI_SELMode = SPI_SEL_HARDWARE;
  SPI_InitStructure.SPI_SELMode = SPI_SEL_SOFTWARE;
  SPI_InitStructure.SPI_SELPolarity = SPI_SELPOLARITY_LOW;
  SPI_InitStructure.SPI_FirstBit = SPI_FIRSTBIT_MSB;
	switch(mode)
	{
		case 0:
			SPI_InitStructure.SPI_CPOL = SPI_CPOL_LOW;
			SPI_InitStructure.SPI_CPHA = SPI_CPHA_FIRST;
			break;
		case 1:
			SPI_InitStructure.SPI_CPOL = SPI_CPOL_LOW;
			SPI_InitStructure.SPI_CPHA = SPI_CPHA_SECOND;
			break;
		case 2:
			SPI_InitStructure.SPI_CPOL = SPI_CPOL_HIGH;
			SPI_InitStructure.SPI_CPHA = SPI_CPHA_FIRST;
			break;
		case 3:
			SPI_InitStructure.SPI_CPOL = SPI_CPOL_HIGH;
			SPI_InitStructure.SPI_CPHA = SPI_CPHA_SECOND;
			break;
	}

  // SPI_InitStructure.SPI_RxFIFOTriggerLevel = 1;
  SPI_InitStructure.SPI_RxFIFOTriggerLevel = 0;
  SPI_InitStructure.SPI_TxFIFOTriggerLevel = 0;
  SPI_InitStructure.SPI_ClockPrescaler = 10; // 60/10 = 6MHz
  SPI_Init(Spi_Use[spiIndex].SPIx, &SPI_InitStructure);

  SPI_SELOutputCmd(Spi_Use[spiIndex].SPIx, ENABLE);

  SPI_Cmd(Spi_Use[spiIndex].SPIx, ENABLE);
}

u8 Spi_SendReceive(spiIndex_t spiIndex, u8 data)
{
  /* Loop while Tx buffer register is empty                                                                 */
  while (!SPI_GetFlagStatus(Spi_Use[spiIndex].SPIx, SPI_FLAG_TXBE));
  SPI_SendData(Spi_Use[spiIndex].SPIx, data);
  /* Loop while Rx is not empty                                                                             */
  while (!SPI_GetFlagStatus(Spi_Use[spiIndex].SPIx, SPI_FLAG_RXBNE));
	return (SPI_ReceiveData(Spi_Use[spiIndex].SPIx));
}

void Spi_SendMulti(spiIndex_t spiIndex, u8 *p, u32 len)
{
  while(len--) Spi_SendReceive(spiIndex, *p++);
}

void Spi_ReceiveMulti(spiIndex_t spiIndex, u8 *p, u32 len)
{
  u32 i = 0;
  for(i = 0; i < len; i++) p[i] = Spi_SendReceive(spiIndex, 0);
}

/* Private functions ---------------------------------------------------------------------------------------*/
static void clkSelBitQSPI(CKCU_PeripClockConfig_TypeDef *CKCUClock)
{
	(*CKCUClock).Bit.QSPI_SEL_BIT_CLOCK           = 1;
	(*CKCUClock).Bit.QSPI_IPN		                	= 1;	
}

static void clkSelBitSPI1(CKCU_PeripClockConfig_TypeDef *CKCUClock)
{
	(*CKCUClock).Bit.SPI1_SEL_BIT_CLOCK           = 1;
	(*CKCUClock).Bit.SPI1_IPN		                	= 1;	
}
