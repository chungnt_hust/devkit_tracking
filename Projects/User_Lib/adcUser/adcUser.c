/*
 * adcUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "adcUser.h"
#include "uartUser.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define USE_INT 											0
#define USE_IVREF_OUT									0

#define ADC_VREF_LEVEL                ADC_VREF_1V215
#define VREF_VALUE_mV                 (1215)

//#define ADC_VREF_LEVEL                ADC_VREF_2V0
//#define VREF_VALUE_mV                 (2000)

//#define ADC_VREF_LEVEL                ADC_VREF_2V5
//#define VREF_VALUE_mV                 (2500)

//#define ADC_VREF_LEVEL                ADC_VREF_2V7
//#define VREF_VALUE_mV                 (2700)
/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
int16_t g_vADC;


/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */

/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
  * @brief  ADC configuration.
  * @retval None
  ***********************************************************************************************************/
void adcUser_DMAConfiguration(void)
{
	{ /* Enable peripheral clock                                                                              */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
    CKCUClock.Bit.PDMA = 1;
		CKCUClock.Bit.AFIO = 1;
    CKCUClock.Bit.ADC0 = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }
		
	{ /* Configure PDMA channel to move ADC result from ADC->DR[0] to TM CHnCCR                               */

    /* !!! NOTICE !!!
       Notice that the local variable (structure) did not have an initial value.
       Please confirm that there are no missing members in the parameter settings below in this function.
    */
    PDMACH_InitTypeDef PDMACH_InitStructure;

    PDMACH_InitStructure.PDMACH_SrcAddr = (u32)&HT_ADC0->DR[0];
    PDMACH_InitStructure.PDMACH_DstAddr = (u32)(&g_vADC);
    PDMACH_InitStructure.PDMACH_BlkCnt = 1;
    PDMACH_InitStructure.PDMACH_BlkLen = 1;
    PDMACH_InitStructure.PDMACH_DataSize = WIDTH_32BIT;
    PDMACH_InitStructure.PDMACH_Priority = VH_PRIO;
    PDMACH_InitStructure.PDMACH_AdrMod = SRC_ADR_FIX | DST_ADR_FIX | AUTO_RELOAD;
    PDMA_Config(PDMA_ADC0, &PDMACH_InitStructure);
    PDMA_EnaCmd(PDMA_ADC0, ENABLE);
		PDMA_IntConfig(PDMA_CH0, PDMA_INT_GE | PDMA_INT_TC, ENABLE);
  }
	
	/* Configure AFIO mode as ADC function                                                                    */
  AFIO_GPxConfig(VR0_GPIO_ID, VR0_AFIO_PIN, AFIO_FUN_ADC0);
	
	{ /* ADC related settings                                                                                 */
		/* CK_ADC frequency is set to (CK_AHB / 64)                                                             */
		CKCU_SetADCnPrescaler(CKCU_ADCPRE_ADC0, CKCU_ADCPRE_DIV64);

		/* Sequence length = 1                                                                   */
		ADC_RegularGroupConfig(HT_ADC0, ONE_SHOT_MODE, 1, 0);

		/* ADC conversion time = (Sampling time + Latency) / CK_ADC = (1.5 + ADST + 12.5) / CK_ADC              */
		/* Set ADST = 0, sampling time = 1.5 + ADST                                                             */
		ADC_SamplingTimeConfig(HT_ADC0, 0);

		/* Set ADC conversion sequence as channel n                                                             */
		ADC_RegularChannelConfig(HT_ADC0, VR0_ADC_CH, 0, 0);
		
		ADC_RegularTrigConfig(HT_ADC0, ADC_TRIG_BFTM1);
	}
		
  /* Issue ADC DMA request when cycle end of conversion occur                                               */
  ADC_PDMAConfig(HT_ADC0, ADC_PDMA_REGULAR_CYCLE, ENABLE);
	NVIC_EnableIRQ(PDMACH0_IRQn);
	
	ADC_Cmd(HT_ADC0, ENABLE);
}
