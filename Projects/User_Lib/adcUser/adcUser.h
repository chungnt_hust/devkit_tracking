/*
 * adcUser.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef ADCUSER_H
#define ADCUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */

#define VREF_GPIO_ID                         GPIO_PA
#define VREF_AFIO_PIN                        AFIO_PIN_0
/************************* ADC Channel 0 ***************************************//**/
#define _HTCFG_VR0_GPIOX                     A
#define _HTCFG_VR0_GPION                     0
#define VR0_ADC_CH                           (ADC_CH_0)
#define VR0_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR0_GPIOX)
#define VR0_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR0_GPION)
/************************* ADC Channel 1 ***************************************//**/
#define _HTCFG_VR1_GPIOX                     A
#define _HTCFG_VR1_GPION                     1
#define VR1_ADC_CH                           (ADC_CH_1)
#define VR1_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR1_GPIOX)
#define VR1_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR1_GPION)
/************************* ADC Channel 2 ***************************************//**/
#define _HTCFG_VR2_GPIOX                     A
#define _HTCFG_VR2_GPION                     2
#define VR2_ADC_CH                           (ADC_CH_2)
#define VR2_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR2_GPIOX)
#define VR2_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR2_GPION)
/************************* ADC Channel 3 ***************************************//**/
#define _HTCFG_VR3_GPIOX                     A
#define _HTCFG_VR3_GPION                     3
#define VR3_ADC_CH                           (ADC_CH_3)
#define VR3_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR3_GPIOX)
#define VR3_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR3_GPION)
/************************* ADC Channel 4 ***************************************//**/
#define _HTCFG_VR4_GPIOX                     A
#define _HTCFG_VR4_GPION                     4
#define VR4_ADC_CH                           (ADC_CH_4)
#define VR4_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR4_GPIOX)
#define VR4_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR4_GPION)
/************************* ADC Channel 5 ***************************************//**/
#define _HTCFG_VR5_GPIOX                     A
#define _HTCFG_VR5_GPION                     5
#define VR5_ADC_CH                           (ADC_CH_5)
#define VR5_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR5_GPIOX)
#define VR5_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR5_GPION)
/************************* ADC Channel 6 ***************************************//**/
#define _HTCFG_VR6_GPIOX                     A
#define _HTCFG_VR6_GPION                     6
#define VR6_ADC_CH                           (ADC_CH_6)
#define VR6_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR6_GPIOX)
#define VR6_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR6_GPION)
/************************* ADC Channel 7 ***************************************//**/
#define _HTCFG_VR7_GPIOX                     A
#define _HTCFG_VR7_GPION                     7
#define VR7_ADC_CH                           (ADC_CH_7)
#define VR7_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR7_GPIOX)
#define VR7_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR7_GPION)
/************************* ADC Channel 8 ***************************************//**/
#define _HTCFG_VR8_GPIOX                     D
#define _HTCFG_VR8_GPION                     4
#define VR8_ADC_CH                           (ADC_CH_8)
#define VR8_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR8_GPIOX)
#define VR8_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR8_GPION)
/************************* ADC Channel 9 ***************************************//**/
#define _HTCFG_VR9_GPIOX                     D
#define _HTCFG_VR9_GPION                     5
#define VR9_ADC_CH                           (ADC_CH_9)
#define VR9_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR9_GPIOX)
#define VR9_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR9_GPION)
/************************* ADC Channel 10 ***************************************//**/
#define _HTCFG_VR10_GPIOX                     C
#define _HTCFG_VR10_GPION                     4
#define VR10_ADC_CH                           (ADC_CH_10)
#define VR10_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR10_GPIOX)
#define VR10_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR10_GPION)
/************************* ADC Channel 11 ***************************************//**/
#define _HTCFG_VR11_GPIOX                     C
#define _HTCFG_VR11_GPION                     5
#define VR11_ADC_CH                           (ADC_CH_11)
#define VR11_GPIO_ID                          STRCAT2(GPIO_P,         _HTCFG_VR11_GPIOX)
#define VR11_AFIO_PIN                         STRCAT2(AFIO_PIN_,      _HTCFG_VR11_GPION)

/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */
extern int16_t g_vADC;

typedef struct
{
	int16_t convResult[7]; // Right aligned
} adcDataType_t;

/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void adcUser_DMAConfiguration(void);
#endif
#ifdef __cplusplus
}
#endif
