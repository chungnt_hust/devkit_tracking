/*
 * basicTimer.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "basicTimer.h"
#include "gpioUser.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */

/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
#define BFTM0_FREQ_HZ 10
#define BFTM1_FREQ_HZ 1
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */


/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
  * @brief  BFTM Configuration.
  * @retval None
  ***********************************************************************************************************/
void BasicTimer_Configuration(void)
{
		CKCU_ClocksTypeDef ClockFreq;
		CKCU_GetClocksFrequency(&ClockFreq);
	
    BFTM_DeInit(HT_BFTM0);
    NVIC_ClearPendingIRQ(BFTM0_IRQn);  
    NVIC_EnableIRQ(BFTM0_IRQn);
    {
        CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
        CKCUClock.Bit.BFTM0      = 1;
        CKCU_PeripClockConfig(CKCUClock, ENABLE);
    }
    BFTM_SetCompare(HT_BFTM0, ClockFreq.HCLK_Freq / BFTM0_FREQ_HZ - 1);  // Set to maximum value to detect the timer overflow
    BFTM_SetCounter(HT_BFTM0, 0);
    BFTM_IntConfig(HT_BFTM0, ENABLE);

		BFTM_DeInit(HT_BFTM1);
    NVIC_ClearPendingIRQ(BFTM1_IRQn);  
    NVIC_EnableIRQ(BFTM1_IRQn);
    {
        CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
        CKCUClock.Bit.BFTM1      = 1;
        CKCU_PeripClockConfig(CKCUClock, ENABLE);
    }
    BFTM_SetCompare(HT_BFTM1, ClockFreq.HCLK_Freq / BFTM1_FREQ_HZ - 1);  // Set to maximum value to detect the timer overflow
    BFTM_SetCounter(HT_BFTM1, 0);
    BFTM_IntConfig(HT_BFTM1, ENABLE);
		
    BFTM_EnaCmd(HT_BFTM0, ENABLE); // enable or disable basic function timer
		BFTM_EnaCmd(HT_BFTM1, ENABLE); // enable or disable basic function timer
}

void BFTM0_IRQHandler(void)
{
    /* Clear Interrupt Flag First */
		BFTM_ClearFlag(HT_BFTM0);

}

void BFTM1_IRQHandler(void)
{
    /* Clear Interrupt Flag First */
		BFTM_ClearFlag(HT_BFTM1);
		static u8 stt = 0;
		GPIO_WriteOutBits(DEBUG_LED_PORT, DEBUG_LED_PIN, (FlagStatus)stt);
		stt = !stt;
}
