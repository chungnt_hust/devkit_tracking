/*
 * cellular.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "cellular.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
Cell_Manager_t Cell_Manager;
Cell_Mode_t Cell_Mode;
/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */

/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static uint8_t CheckCellOK(void);
static uint8_t CheckCellIdle(void);
static void PowerOn_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer);
static void Cell_Ready_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer);
static void Get_BTS_Info_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer);

/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void Cellular_InitModule(void)
{
	UART_Cfg(CELL_INDEX, 115200, USART_WORDLENGTH_8B, USART_PARITY_NO, USART_STOPBITS_1);
	
	GPIO_WriteOutBits(CELL_PWR_ON_PORT, CELL_PWR_ON_PIN, SET);
	GPIO_WriteOutBits(CELL_VBAT_ON_PORT, CELL_VBAT_ON_PIN, RESET);
	GPIO_WriteOutBits(CELL_PWR_ON_PORT, CELL_PWR_ON_PIN, RESET);
	HAL_Delay(1000);
	GPIO_WriteOutBits(CELL_VBAT_ON_PORT, CELL_VBAT_ON_PIN, SET);
	DEBUG("[CELL] Power on Module\r\n");
	for(uint8_t i = 0; i < 4; i++)
	{
		DEBUG(".");
		HAL_Delay(1000);
	}
	GPIO_WriteOutBits(CELL_PWR_ON_PORT, CELL_PWR_ON_PIN, SET);
	for(uint8_t i = 0; i < 3; i++)
	{
		DEBUG(".");
		HAL_Delay(1000);
	}
	GPIO_WriteOutBits(CELL_PWR_ON_PORT, CELL_PWR_ON_PIN, RESET);
	
	AT_Init();
	
	//memcpy(Cell_Manager.systemMode, DEFAULT_SYSTEM_MODE, sizeof(DEFAULT_SYSTEM_MODE));
	memset(Cell_Manager.systemMode, 0, sizeof(Cell_Manager.systemMode));
	Cell_Manager.cellState = CELL_IDLE;
	Cell_Manager.getSignalLevelTimeOut = 0;
	Cell_Manager.step = 0;
	Cell_Manager.firstTimePowerOn = 0;
	Cell_Manager.waitCellReady = 0;
	Cell_Manager.cellReady = 0;
	Cell_Manager.cellNotReady = 0;
	Cell_Manager.moduleNotMounted = 0;
	Cell_Manager.initModuleDone = 1;
	Cell_Manager.onlineState = MODEM_OFFLINE;
	
	Cell_Mode = CELL_CMD_MODE;
}

/*********************************************************************************************************//**
 * @brief: Hard reset module by pull low/hig powerkey
 * @param
 * @retval  
 ************************************************************************************************************/
void Cellular_HardReset(void)
{
	
}

/*********************************************************************************************************//**
 * @brief: Switch to new mode
 * @param
 * @retval  
 ************************************************************************************************************/
void Cellular_SwitchMode(Cell_Mode_t newMode)
{
	Cell_Mode = newMode;
	
	switch(Cell_Mode)
	{
		case CELL_CMD_MODE:
			DEBUG("[CELL] Mode: CELL_CMD_MODE\r\n");
			break;
		
		case CELL_DATA_MODE:
			DEBUG("[CELL] Mode: CELL_DATA_MODE\r\n");
			break;
	}
}

/*********************************************************************************************************//**
 * @brief: Switch to new state
 * @param
 * @retval  
 ************************************************************************************************************/
void Cellular_SwitchState(Cellular_State_t newState)
{
//	Cell_Manager.cellOldState = Cell_Manager.cellState;
	Cell_Manager.cellState = newState;
	Cell_Manager.step = 0;
	
	switch(Cell_Manager.cellState)
	{
		case CELL_IDLE:
			DEBUG("[CELL] Switch: CELL_IDLE\r\n");
			break;
		case CELL_GETBTSINFOR:
			DEBUG("[CELL] Switch: CELL_GETBTSINFOR\r\n");
			break;
		case CELL_POWERON:
			DEBUG("[CELL] Switch: CELL_POWERON\r\n");
			break;
		case  CELL_RESET:
			DEBUG("[CELL] Switch: CELL_RESET\r\n");
			break;
		case  CELL_RECONNECT:
			DEBUG("[CELL] Switch: CELL_RECONNECT\r\n");
			break;
	}
}

/*********************************************************************************************************//**
 * @brief: Handle state machine every 1s
 * @param
 * @retval  
 ************************************************************************************************************/
void Cellular_ManagerTask(void)
{
	if(Cell_Manager.initModuleDone == 0) return;
	if(Cell_Manager.moduleNotMounted == 1) return;
	
	if(Cell_Manager.firstTimePowerOn == 0)
	{
		if(CheckCellOK())
		{
			if(Cell_Manager.waitCellReady == 0) DEBUG("[CELL] Wait for ready in 3s\r\n");
			if(++Cell_Manager.waitCellReady >= 3) 
			{
				Cell_Manager.waitCellReady = 0;
				Cell_Manager.firstTimePowerOn = 1;
				Cellular_SwitchState(CELL_POWERON);
			}
		}
		else 
		{
			DEBUG("[CELL] Initiating Cellular...\r\n");
			if(++Cell_Manager.cellNotReady >= 30)
			{
				Cell_Manager.moduleNotMounted = 1;
				NVIC_SystemReset();
			}
		}
	}
	
	switch(Cell_Manager.cellState)
	{
		case CELL_POWERON:
			if(Cell_Manager.step == 0)
			{
				Cell_Manager.step = 1;
				AT_SendATCommand("AT+CGSN\r\n", NULL, "OK", 1000, 3, PowerOn_Callback);
			}
			break;
			
		case CELL_GETBTSINFOR:
			if(Cell_Manager.step == 0)
			{			
				if(Cell_Mode == CELL_CMD_MODE)
				{
					Cell_Manager.step = 1;
					AT_SendATCommand("AT+CSQ\r\n", "+CSQ:", "OK", 1000, 3, Get_BTS_Info_Callback);
				}
				else if(Cell_Mode == CELL_DATA_MODE)
					AT_SendATCommand("+++", "OK", NULL, 3000, 5, Get_BTS_Info_Callback);
			}
			break;
		case CELL_IDLE:
			break;
		
		case CELL_RESET:
			break;
		
		case CELL_RECONNECT:
			break;
	}
	
//	if(Cell_Manager.cellReady == 1)
//	{
//		if(++Cell_Manager.getSignalLevelTimeOut >= 60)
//		{
//			if(CheckCellIdle() == 1)
//			{
//				Cell_Manager.getSignalLevelTimeOut = 0;
//				Cell_Manager.step = 0;
//				Cell_Manager.BTSInfor = 0;
//				Cellular_SwitchState(CELL_GETBTSINFOR);
//			}
//			else Cell_Manager.getSignalLevelTimeOut = 50; 
//		}
//	}
}

/*********************************************************************************************************//**
 * @brief  : Check ready state
 * @param
 * @retval  
 ************************************************************************************************************/
uint8_t Cellular_CheckReady2Use(void)
{
	return Cell_Manager.cellRDY2Use;
}

/*********************************************************************************************************//**
 * @brief  : Check CSQ and System mode are valid or not
 * @param
 * @retval  
 ************************************************************************************************************/
uint8_t Cellular_CheckSignalInfo(void)
{
	return Cell_Manager.BTSInfor;
}

/*********************************************************************************************************//**
 * @brief  : Clear Signal Info
 * @param
 * @retval  
 ************************************************************************************************************/
void Cellular_ClearSignalInfo(void)
{
	Cell_Manager.BTSInfor = 0;
}
/*********************************************************************************************************//**
 * @brief  : Get CSQ and System mode
 * @param
 * @retval  
 ************************************************************************************************************/
void Cellular_GetSignalLevel(void)
{
	if(Cell_Mode == CELL_CMD_MODE)
		AT_SendATCommand("[CELL] AT+CSQ\r\n", "+CSQ:", "OK", 1000, 3, Get_BTS_Info_Callback);
}

/*********************************************************************************************************//**
 * @brief  : +CPIN:
 * @param
 * @retval  
 ************************************************************************************************************/
void Cellular_Ready(void *ResponseBuffer)
{
	DEBUG("[CELL] Ready to use AT command\r\n");
	AT_SendATCommand("ATE0\r\n", NULL, "OK", 3000, 5, Cell_Ready_Callback);
}

/*********************************************************************************************************//**
 * @brief  : +CGEV:
 * @param
 * @retval  
 ************************************************************************************************************/
void Cellular_GPRS_event_reporting(void *ResponseBuffer)
{
	if(strstr((const char*)ResponseBuffer, "ME DETACH"))
	{
		DEBUG("[CELL] Not Ready\r\n");
		Cell_Manager.cellReady = 0;
		Cell_Manager.cellRDY2Use = 0;
	}
}
/* ====================== private functions =========================== */
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static uint8_t CheckCellOK(void)
{
	return Cell_Manager.cellReady;
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static uint8_t CheckCellIdle(void)
{
	if(!Cell_Manager.cellReady) return 0;
//	if(Cell_Manager.RISignal) return 0;
	if(Cell_Manager.cellState != CELL_IDLE) return 0;
	
	return 1;
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void PowerOn_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer)
{
	uint8_t len;
	char *p;
	if(event == EVENT_OK)
	{
		switch(Cell_Manager.step)
		{
			case 1:								
				p = (char*)ResponseBuffer;
				for(len = 0; len < 20; len++) 
				{
					if(p[len] >= '0' && p[len] <= '9')
						Cell_Manager.IMEI[len] = p[len];
					else 
					{
						Cell_Manager.IMEI[len] = 0;
						break;
					}
				}
				DEBUG("[CELL] IMEI: %s\r\n", Cell_Manager.IMEI);
				AT_SendATCommand("AT+CIPMODE=1\r\n", "OK", NULL, 1000, 5, PowerOn_Callback);
				break;		
			case 2:
				DEBUG("[CELL] TCP/IP: Data mode\r\n");
				Cell_Manager.cellRDY2Use = 1;
				//AT_SendATCommand("AT+CSQ\r\n", "+CSQ:", 1000, 3, Get_BTS_Info_Callback);
				break;
		}
		Cell_Manager.step++;
	}
}
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void Cell_Ready_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer)
{
	if(event == EVENT_OK)
	{
		DEBUG("[CELL] Cellular ready\r\n");
		Cell_Manager.cellReady = 1;
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void Get_BTS_Info_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer)
{
	char *token[2] = {NULL, NULL};
	if(event == EVENT_OK)
	{
		if(Cell_Mode == CELL_DATA_MODE) 
		{
			Cellular_SwitchMode(CELL_CMD_MODE);
			DEBUG("[CELL] Switch to cmd mode ok\r\n");
			return;
		}
		
		token[0] = strtok((char*)ResponseBuffer, ":");
		token[1] = strtok(NULL, ",");
		
		if(token[0] != NULL)
		{
			if(token[1] != NULL)
			{
				if(strstr(token[0], "+CSQ"))
				{				
					Cell_Manager.csq = atoi(token[1]);
					AT_SendATCommand("AT+CPSI?\r\n", "+CPSI:", "OK", 1000, 20, Get_BTS_Info_Callback);
				}
				else
				{
					memset(Cell_Manager.systemMode, 0, sizeof(Cell_Manager.systemMode));
					memcpy(Cell_Manager.systemMode, token[1], strlen(token[1]));
					char buf[50], len;
					len = sprintf(buf, "System mode: %s, csq: %d\r\n", Cell_Manager.systemMode, Cell_Manager.csq);
					buf[len] = 0;
					DEBUG("[CELL] %s", buf);
					
					if(strstr(Cell_Manager.systemMode, DEFAULT_SYSTEM_MODE)) Cell_Manager.BTSInfor = 0;
					else if(Cell_Manager.csq <= 31) Cell_Manager.BTSInfor = 1;

					Cellular_SwitchState(CELL_IDLE);
				}
			}
		}
	}
	else 
	{
		Cell_Manager.cellReady = 0;
		Cellular_SwitchState(CELL_RESET);
	}
}
