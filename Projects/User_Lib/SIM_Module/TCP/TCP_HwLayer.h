/*
 * TCP_HwLayer.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef TCP_HWLAYER_H
#define TCP_HWLAYER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define DEFAULT_CID 				0
#define DEFAULT_TCP_IPADD 	"2.tcp.ngrok.io"
#define DEFAULT_TCP_PORT 		10787

#define DEFAULT_TIMEOUT_CONNECT 180 // 180s
/* #define and enum statements go here */
typedef enum
{
	TCP_STT_DISCONNECTED = 0,
	TCP_STT_CONNECTED,
} TCP_CONNECT_STT_E;

typedef struct
{
	uint8_t connect				: 3;
	uint8_t needConnect 	: 1;
	uint8_t connectStatus : 1;
	uint8_t step					: 3;
 
	uint16_t timeoutConnection;
	char dataBuf[1024];
	char bufContent[50];	
} TCP_Manager_t;

typedef struct
{
	uint8_t cid;
	char ipadd[30];
	uint16_t port; 
} TCP_Params_t;
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void TCP_Connect(void);
void TCP_ManagerTask(void);
void TCP_Init(void);
uint8_t TCP_CheckConnStt(void);

void TCP_SocketIsStarted(void *ResponseBuffer);
void TCP_SocketIsStopped(void *ResponseBuffer);
void TCP_SocketIsClosed(void *ResponseBuffer);
void TCP_IsConnected(void *ResponseBuffer);
void TCP_IsClosed(void *ResponseBuffer);
void TCP_NetworkIsClosedUnexpected(void *ResponseBuffer);

uint8_t TCP_LogParams(char *buf);
uint8_t TCP_SetParams(char* buf);
#endif
#ifdef __cplusplus
}
#endif
