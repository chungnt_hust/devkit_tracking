/*
 * TCP_HwLayer.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "TCP_HwLayer.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define TIME_GET_SIGNAL 5
#define RETRY			 			0
#define CANCEL		   		6

#define MSG_FIELD_SEPARATOR (',')
/* Message field position */
#define MSG_FIELD_CID 	(0)
#define MSG_FIELD_IPADD (1)
#define MSG_FIELD_PORT 	(2)
#define MSG_MAX_FIELDS 	(3)
/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
TCP_Manager_t Tcp_Manager = {.connect = CANCEL, .needConnect = 0, .connectStatus = 0, .timeoutConnection = 0};
TCP_Params_t Tcp_Params;
extern Cell_Mode_t Cell_Mode;

/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
/* Enum statement for TCP client */
typedef enum
{
	TCP_DEFINE_PDP = 0, 
	TCP_CONFIG_TIMEOUT,
	TCP_NETOPEN,
	TCP_INQUIRY_IPADD,
	TCP_CIPOPEN,
	TCP_CIPCLOSE,
	TCP_NETCLOSE,
} TCP_STEP_E;

static AT_List_t AT_TCP_List[] = 
{
	{"AT+CGDCONT=%d,\"IP\"\r\n"},							// Packet Data Protocol (PDP) type IP, page 289
	{"AT+CIPTIMEOUT=10000,10000,10000\r\n"},	// Config timeout connect
	{"AT+NETOPEN\r\n"},												// Start socket service
	{"AT+IPADDR\r\n"},												// Inquire Socket PDP address
	{"AT+CIPOPEN=%d,\"TCP\",\"%s\",%d\r\n"},	// Establish Connection
	{"AT+CIPCLOSE=%d\r\n"},										// Close connection
	{"AT+NETCLOSE\r\n"},											// Stop socket service
};
 
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static uint8_t checkNETOPEN(char *buf);
static void TCP_StartSocket_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer);
static void TCP_Connected_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer);
static void TCP_Disconnected_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer);


/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void TCP_Connect(void)
{
	if(Tcp_Manager.connectStatus == TCP_STT_DISCONNECTED)
	{
		Tcp_Manager.timeoutConnection = DEFAULT_TIMEOUT_CONNECT;
		Tcp_Manager.connect = RETRY;
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void TCP_Retry(void)
{
	if(Tcp_Manager.connectStatus == TCP_STT_DISCONNECTED)
	{
		Tcp_Manager.connect = RETRY;
	}
}

/*********************************************************************************************************//**
 * @brief   : handle every 1s
 * @param
 * @retval  
 ************************************************************************************************************/
void TCP_ManagerTask(void)
{
	if(Cellular_CheckReady2Use() == 1)
	{
		if(Tcp_Manager.connect < TIME_GET_SIGNAL)
		{
			if(Cellular_CheckSignalInfo() == 0)
			{
				if(++Tcp_Manager.connect >= TIME_GET_SIGNAL) 
				{
					DEBUG(".");
					Tcp_Manager.connect = RETRY;
					Cellular_GetSignalLevel();
				}
			}
			else 
			{
				Tcp_Manager.connect = CANCEL;
				Tcp_Manager.needConnect = 1;
				Cellular_ClearSignalInfo();
			}
		}
	}
	
	if(Tcp_Manager.needConnect == 1)
	{
		DEBUG("[TCP] connecting\r\n");
		if(Cell_Mode == CELL_CMD_MODE)
		{
			Tcp_Manager.needConnect = 0;
			TCP_Init();
		}
	}
	
	if(Tcp_Manager.timeoutConnection > 0)
	{
		if(--Tcp_Manager.timeoutConnection == 0)
			Tcp_Manager.connect = CANCEL;
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void TCP_Init(void)
{
	Tcp_Manager.connectStatus = TCP_STT_DISCONNECTED;

	EN25Q_readData(FLASH_TCP_AREA, &Tcp_Params, sizeof(Tcp_Params));
	if(Tcp_Params.cid == 0xFF)
	{
		Tcp_Params.cid = DEFAULT_CID;
		memcpy(Tcp_Params.ipadd, DEFAULT_TCP_IPADD, sizeof(DEFAULT_TCP_IPADD));
		Tcp_Params.port = DEFAULT_TCP_PORT;  
	}
	
	memset(Tcp_Manager.bufContent, 0, sizeof(Tcp_Manager.bufContent));
	sprintf(Tcp_Manager.bufContent, AT_TCP_List[TCP_CIPOPEN].content, Tcp_Params.cid, Tcp_Params.ipadd, Tcp_Params.port);
	DEBUG("[TCP] init done: %s", Tcp_Manager.bufContent);

	Tcp_Manager.step = 0;
	AT_SendATCommand("AT+NETOPEN\r\n", "OK", "+NETOPEN:", 30000, 3, TCP_StartSocket_Callback); // Start socket service
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
uint8_t TCP_CheckConnStt(void)
{
	return Tcp_Manager.connectStatus;
}

/*********************************************************************************************************//**
 * @exam   : Start socket service, +NETOPEN: 0
 * @param
 * @retval  
 ************************************************************************************************************/
void TCP_SocketIsStarted(void *ResponseBuffer)
{
	uint8_t err;

	err = checkNETOPEN((char*)ResponseBuffer);
	if(err == 0)
	{
		DEBUG("[TCP] NETOPEN ok\r\n");
		AT_SendATCommand(Tcp_Manager.bufContent, "CONNECT 115200", NULL, 10000, 5, TCP_Connected_Callback); // Establish connection
	}
	else DEBUG("[TCP] NETOPEN Fail[%d]\r\n", err);
}

/*********************************************************************************************************//**
 * @exam	:	Stop socket service, +NETCLOSE: 0
 * @param
 * @retval  
 ************************************************************************************************************/
void TCP_SocketIsStopped(void *ResponseBuffer)
{
	DEBUG("[TCP] TCP_SocketIsStopped\r\n");
	Cellular_SwitchMode(CELL_CMD_MODE);
	TCP_Disconnected_Callback(EVENT_OK, ResponseBuffer);
}

/*********************************************************************************************************//**
 * @brief   : CONNECT. A connection has been established
 * @param
 * @retval  
 ************************************************************************************************************/
void TCP_IsConnected(void *ResponseBuffer)
{
	DEBUG("[TCP] A connection has been established\r\n");
	Tcp_Manager.connectStatus = TCP_STT_CONNECTED;
	GPIO_WriteOutBits(CELL_LED_PORT, CELL_LED_PIN, LED_ON);
	
	Cellular_SwitchMode(CELL_DATA_MODE);
}

/*********************************************************************************************************//**
 * @brief   : CLOSED. A connection has been closed
 * @param
 * @retval  
 ************************************************************************************************************/
void TCP_IsClosed(void *ResponseBuffer)
{
	DEBUG("[TCP] TCP_IsClosed\r\n");
	
	Cellular_SwitchMode(CELL_CMD_MODE);
	
	Tcp_Manager.step = 0;
	Tcp_Manager.timeoutConnection = DEFAULT_TIMEOUT_CONNECT;
	AT_SendATCommand("AT+NETOPEN?\r\n", "+NETOPEN:", "OK", 2000, 3, TCP_Disconnected_Callback); // Query socket status
}

/*********************************************************************************************************//**
 * @exam:	+IPCLOSE: 1,1   , Socket is closed passively
 * @param
 * @retval  
 ************************************************************************************************************/
void TCP_SocketIsClosed(void *ResponseBuffer)
{
	char *token;
	uint8_t reason;
	token = strtok((char*)ResponseBuffer, ":");
	token = strtok(NULL, ",");
	token = strtok(NULL, "\r\n");
	reason = atoi(token);
	DEBUG("[TCP] Socket is closed\r\n");
	if(reason == 0) DEBUG("[TCP] Closed by local, active\r\n");
	else if(reason == 1) DEBUG("[TCP] Closed by remote, passive\r\n");
	else if(reason == 2) DEBUG("[TCP] Closed for sending timeout or DTR off\r\n");
	
	Cellular_SwitchMode(CELL_CMD_MODE);
	
	Tcp_Manager.step = 0;
	Tcp_Manager.timeoutConnection = DEFAULT_TIMEOUT_CONNECT;
	AT_SendATCommand("AT+NETOPEN?\r\n", "+NETOPEN:", "OK", 2000, 3, TCP_Disconnected_Callback); // Query socket status
}

/*********************************************************************************************************//**
 * @exam:	+CIPEVENT: NETWORK CLOSED UNEXPECTEDLY
 * @param
 * @retval  
 ************************************************************************************************************/
void TCP_NetworkIsClosedUnexpected(void *ResponseBuffer)
{
	DEBUG("[TCP] TCP_NetworkIsClosedUnexpected\r\n");
	
	Cellular_SwitchMode(CELL_CMD_MODE);
	
	Tcp_Manager.step = 0;
	HAL_Delay(3000);
	Tcp_Manager.timeoutConnection = DEFAULT_TIMEOUT_CONNECT;
	AT_SendATCommand("AT+NETOPEN?\r\n", "+NETOPEN:", "OK", 10000, 3, TCP_Disconnected_Callback); // Query socket status
}

/*********************************************************************************************************//**
 * @exam:	Log all params 
 * @param typedef struct
{
	uint8_t cid;
	char ipadd[30];
	uint16_t port;  
	uint8_t needConnect;
	uint8_t connectStatus;
	char dataBuf[1024];
	uint8_t step;
	char bufContent[50];
} TCP_Manager_t;
 * @retval  
 ************************************************************************************************************/
uint8_t TCP_LogParams(char *buf)
{
	if(strstr(buf, "GETTCP"))
	{		
		EN25Q_readData(FLASH_TCP_AREA, &Tcp_Params, sizeof(Tcp_Params));
		if(Tcp_Params.cid == 0xFF)
		{
			Tcp_Params.cid = DEFAULT_CID;
			memcpy(Tcp_Params.ipadd, DEFAULT_TCP_IPADD, sizeof(DEFAULT_TCP_IPADD));
			Tcp_Params.port = DEFAULT_TCP_PORT;  
		}
		DEBUG("==============TCP Params==============\r\n");
		DEBUG("cid: %d\r\n", Tcp_Params.cid);
		DEBUG("ipadd: %s\r\n", Tcp_Params.ipadd);
		DEBUG("port: %d\r\n", Tcp_Params.port);
		DEBUG("connect: %d\r\n", Tcp_Manager.connect);
		DEBUG("needConnect: %d\r\n", Tcp_Manager.needConnect);
		DEBUG("connectStatus: %d\r\n", Tcp_Manager.connectStatus);
		DEBUG("step: %d\r\n", Tcp_Manager.step);
		DEBUG("==============++++++++++==============\r\n");
		return 1;
	}
	return 0;
}

/*********************************************************************************************************//**
 * @exam:	Set TCP IP and port
* @param : SETTCP:<TCPServerAdd>:<Port>;
 * @retval  
 ************************************************************************************************************/
uint8_t TCP_SetParams(char* buf)
{
	char *token;
	if(strstr(buf, "SETTCP"))
	{
		token = strtok(buf, ":"); 			// SETTCP
		token = strtok(NULL, ":");  		// Cid
		Tcp_Params.cid = atoi(token);
		token = strtok(NULL, ":"); 			// TCPServerAdd
		strcpy(Tcp_Params.ipadd, token);
		token = strtok(NULL, ":"); 			// Port
		Tcp_Params.port = atoi(token);
		
		EN25Q_erase4k(FLASH_TCP_AREA);
		EN25Q_writeData(FLASH_TCP_AREA, &Tcp_Params, sizeof(Tcp_Params));
		TCP_Retry();
		return 1;
	}
	return 0;
}
/* ====================== private functions =========================== */
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static uint8_t checkNETOPEN(char *buf)
{
	char *token, *tem;
	uint8_t value;
	tem = strstr((const char*)buf, "+NETOPEN:");
	token = strtok(tem, ":"); 		// +NETOPEN
	token = strtok(NULL, "\r\n"); // <state> or <err>
	value = atoi(token);

	return value;
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static uint8_t checkNETCLOSE(char *buf)
{
	char *token, *tem;
	uint8_t value;

	tem = strstr((const char*)buf, "+NETCLOSE:");
	token = strtok(tem, ":");			// +NETCLOSE
	token = strtok(NULL, "\r\n");	// <err>
	value = atoi(token);

	return value;
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void TCP_StartSocket_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer)
{
	if(event == EVENT_OK)
	{
		DEBUG("[TCP] Checking socket\r\n");
		TCP_SocketIsStarted(ResponseBuffer);
	}
	else if(event == EVENT_ERROR)
	{
		if(strstr((const char*)ResponseBuffer, "Network is already opened"))
		{
			DEBUG("[TCP] Network is already opened\r\n");
			/* Check lai cho nay, already opened nhung query ra 0 */
			Tcp_Manager.step = 0;
			AT_SendATCommand("AT+NETOPEN?\r\n", "+NETOPEN:", "OK", 2000, 3, TCP_Disconnected_Callback); // Query socket status
		}
	}
	else if(event == EVENT_TIMEOUT)
	{
		DEBUG("[TCP] start fail\r\n");
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void TCP_Connected_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer)
{
	if(event == EVENT_OK)
	{
		DEBUG("[TCP] A connection has been established\r\n");
		Tcp_Manager.timeoutConnection = 0;
		Tcp_Manager.connectStatus = TCP_STT_CONNECTED;
		GPIO_WriteOutBits(CELL_LED_PORT, CELL_LED_PIN, LED_ON);

		Cellular_SwitchMode(CELL_DATA_MODE);
	}
	else if((event == EVENT_ERROR) || (event == EVENT_TIMEOUT))
	{
		Tcp_Manager.step = 0;
		AT_SendATCommand("AT+NETOPEN?\r\n", "+NETOPEN:", "OK", 2000, 3, TCP_Disconnected_Callback); // Query socket status
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void TCP_Disconnected_Callback(SIMCOM_ResponseEvent_t event, void *ResponseBuffer)
{
	uint8_t net_state;
	if(event == EVENT_OK)
	{	
		switch(Tcp_Manager.step)
		{
			case 0:
				net_state = checkNETOPEN((char*)ResponseBuffer);
				if(net_state == 1) 
				{
					AT_SendATCommand("AT+NETCLOSE\r\n", "OK", "+NETCLOSE:", 5000, 3, TCP_Disconnected_Callback); // Stop socket service
				}
				else 
				{
					DEBUG("[TCP] A connection has been closed[1]\r\n");
					Tcp_Manager.connectStatus = TCP_STT_DISCONNECTED;
					GPIO_WriteOutBits(CELL_LED_PORT, CELL_LED_PIN, LED_OFF);
					
					DEBUG("[TCP] Reconnect TCP\r\n");
					TCP_Retry();
				}
				break;
			case 1:
				net_state = checkNETCLOSE((char*)ResponseBuffer);
				if(net_state == 0) 
				{
					DEBUG("[TCP] A connection has been closed[2]\r\n");
					Tcp_Manager.connectStatus = TCP_STT_DISCONNECTED;
					GPIO_WriteOutBits(CELL_LED_PORT, CELL_LED_PIN, LED_OFF);
					
					DEBUG("[TCP] Reconnect TCP\r\n");
					TCP_Retry();
				}
				else DEBUG("[TCP] checkNETCLOSE [%d]\r\n", net_state);
				break;
		}		
		Tcp_Manager.step++;
	}
}
