/*
 * AT_Command.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef AT_COMMAND_H
#define AT_COMMAND_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define RX_CIRCULAR_BUF_SIZE 	 1024
#define RX_BUFFER_SIZE 				 128
//#define TX_BUFFER_SIZE 				 1536 // maximum of the data transmition
#define TX_BUFFER_SIZE 				 128
#define TX_AT_CONTENT_SIZE		 50
#define EXPECT_AT_SIZE 				 50
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */
typedef enum
{
	AT_RSP1 = 0,
	AT_RSP2 = 1,
	AT_MAX_RSP = 2
} AT_RSP_t;

typedef enum 
{
    EVENT_OK = 0,  // AT response ok
    EVENT_TIMEOUT, // Timeout response
    EVENT_ERROR,   // AT response error
} SIMCOM_ResponseEvent_t;

typedef enum
{
		SIMCOM_NO_WAIT_RSP = 0,
		SIMCOM_WAITING_RSP,
} SIMCOM_ResponseStatus_t;

typedef void (*SendATCallBack_t)(SIMCOM_ResponseEvent_t event, void *ResponseBuffer);
typedef void (*DataCallBack_t)(void *ResponseBuffer);

typedef struct 
{
		/* data */
		const char content[TX_AT_CONTENT_SIZE];
		// unsigned short len;
} AT_List_t;

typedef struct
{
		char Buffer[RX_BUFFER_SIZE];
		uint16_t BufferIndex;
} ATBuffer_t;

typedef struct
{
		char CMD[TX_BUFFER_SIZE];
		uint32_t lenCMD;
		char ExpectRsp[AT_MAX_RSP][EXPECT_AT_SIZE];
		uint8_t countRsp;
		uint32_t TimeoutAT;
		uint32_t CurrentTimeoutAT;
		uint8_t RetryCountAT;
		uint8_t waitRsp;
		SendATCallBack_t SendATCallBack;
} ATCommand_t;

typedef struct
{
	const char CMD[EXPECT_AT_SIZE];
	DataCallBack_t UrcCallBack;
} UrcCb_t;

/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void AT_AddC(char ch);
void AT_Process(void);
void AT_Init(void);
void AT_SendATCommand(const char *Command, char *ExpectRsp1, char *ExpectRsp2, uint32_t Timeout,
		uint8_t RetryCount, SendATCallBack_t CallBackFunction);
void AT_ForceRetry(void);
#endif
#ifdef __cplusplus
}
#endif
