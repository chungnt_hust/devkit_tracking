/*
 * AT_Command.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "AT_Command.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define TIMER_AT_PERIOD 1 // 1ms
/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */

/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static void SendATCommand(const char *cmd);
static void RetrySendAT(void);
static uint8_t CheckTimeout(void);

/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
static ATCommand_t ATCommand;
static ATBuffer_t ReceiveBuffer;
static ATBuffer_t ATRspBuffer;
static cicularBuf_t AT_ResBuf;
static uint8_t ATCircularBuf[RX_CIRCULAR_BUF_SIZE];

/******* URC: Unsolicited Result Code *************/
static UrcCb_t UrcList[] = 
{
	{"+CPIN: READY", 		Cellular_Ready},
	//{"CONNECT 115200",  TCP_IsConnected},
	{"CLOSED\r",        TCP_IsClosed},
	{"+IPCLOSE:", 			TCP_SocketIsClosed},
	{"+CIPEVENT:", 			TCP_NetworkIsClosedUnexpected}, 
	{"+CGEV:",					NULL},
	{"*COPN:",					NULL},
	{"+CME",						NULL},
	{"+CEREG:",					NULL},
	{"SMS DONE\r",			NULL},
	{"PB DONE\r",				NULL},
};

static uint16_t idxCb, numDataCb = sizeof(UrcList)/sizeof(UrcList[0]);
/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void AT_AddC(char ch)
{
	CBUFFER_Putc(&AT_ResBuf, ch);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void AT_Process(void)
{
	uint8_t ch;
	/*********************************************************************************//**
	* @: check timeout response
	*************************************************************************************/
	if(CheckTimeout() == 1) 
	{
		CBUFFER_Reset(&AT_ResBuf);
		return;
	}
	/*********************************************************************************//**
	*************************************************************************************/
	if(CBUFFER_Getc(&AT_ResBuf, &ch) == 1)
	{
		ReceiveBuffer.Buffer[ReceiveBuffer.BufferIndex++] = ch;

		if(ch == '\r' || ch == '\n')
		{
			if(ReceiveBuffer.BufferIndex <= 2) 
			{
				ReceiveBuffer.BufferIndex = 0;
				return;
			}
			
			DEBUG("=@=> Cell Data: %s\r\n", ReceiveBuffer.Buffer);	

			/*********************************************************************************//**
			* @: Unsolicited Result Code
			*************************************************************************************/
			for(idxCb = 0; idxCb < numDataCb; idxCb++)
			{
				if(strstr((const char*)ReceiveBuffer.Buffer, UrcList[idxCb].CMD))
				{
					DEBUG("---> URC <---\r\n");
					if(UrcList[idxCb].UrcCallBack != NULL)
						UrcList[idxCb].UrcCallBack(ReceiveBuffer.Buffer);
						
					memset(&ReceiveBuffer, 0, sizeof(ReceiveBuffer));
					return;
				}
			}
			
			/*********************************************************************************//**
			* @: waiting response of AT command which has been sent from the host    
			*************************************************************************************/		
			if((ATCommand.countRsp < AT_MAX_RSP) && (ATCommand.waitRsp == SIMCOM_WAITING_RSP))
			{
				if(ATCommand.ExpectRsp[ATCommand.countRsp][0] == 0) // No RSP
				{
					ATRspBuffer.BufferIndex += sprintf(&ATRspBuffer.Buffer[ATRspBuffer.BufferIndex], "%s\n", ReceiveBuffer.Buffer);
					ATRspBuffer.Buffer[ATRspBuffer.BufferIndex] = 0;
					
					ATCommand.countRsp++; 
					if(ATCommand.countRsp == AT_MAX_RSP) 
					{
						ATCommand.waitRsp = SIMCOM_NO_WAIT_RSP;
						ATCommand.TimeoutAT = 0;
					}
				}
				else	// RSP
				{
					if(strstr((const char*)ReceiveBuffer.Buffer, ATCommand.ExpectRsp[ATCommand.countRsp])) // RSP ok
					{
						ATRspBuffer.BufferIndex += sprintf(&ATRspBuffer.Buffer[ATRspBuffer.BufferIndex], "%s\n", ReceiveBuffer.Buffer);
						ATRspBuffer.Buffer[ATRspBuffer.BufferIndex] = 0;
								
						if((ATCommand.ExpectRsp[AT_RSP2][0] == 0) || (ATCommand.countRsp == AT_RSP2)) // Rsp2 = null || rsp2 = data => callback immediately
						{
							ATCommand.waitRsp = SIMCOM_NO_WAIT_RSP;
							ATCommand.TimeoutAT = 0;
							if(ATCommand.SendATCallBack != NULL)
							{
								DEBUG("[AT] Callback data: %s\r\n", ATRspBuffer.Buffer);
								ATCommand.SendATCallBack(EVENT_OK, ATRspBuffer.Buffer);
							}
							else
							{
								DEBUG("[AT] Event ok without callback function [%d]\r\n", ATCommand.waitRsp);
							}
						}
						else ATCommand.countRsp++;
					}
					else
					{
						ATCommand.CurrentTimeoutAT = 0;
						if(ATCommand.RetryCountAT > 0)
						{							
							if(--ATCommand.RetryCountAT == 0) ATCommand.TimeoutAT = 0;
							DEBUG("[AT] ERR:");			
							CBUFFER_Reset(&AT_ResBuf);
							RetrySendAT();
						}		
						else 
						{
							if(ATCommand.SendATCallBack != NULL)
							{		
								ATCommand.TimeoutAT = 0;
								ATCommand.SendATCallBack(EVENT_ERROR, ReceiveBuffer.Buffer);
							}
							else
							{
								DEBUG("[AT] Event error without callback function\r\n");
							}
						}
					}		
				}
			}		
			memset(&ReceiveBuffer, 0, sizeof(ReceiveBuffer));
		}
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void AT_Init(void)
{
	memset(&ReceiveBuffer, 0, sizeof(ReceiveBuffer));
	memset(&ATRspBuffer, 0, sizeof(ATRspBuffer));
	CBUFFER_Init(&AT_ResBuf, ATCircularBuf, RX_CIRCULAR_BUF_SIZE);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void AT_SendATCommand(const char *Command, char *ExpectRsp1, char *ExpectRsp2, uint32_t Timeout,
		uint8_t RetryCount, SendATCallBack_t CallBackFunction)
{
	memset(&ATCommand, 0, sizeof(ATCommand));
	ATCommand.lenCMD = sprintf(ATCommand.CMD, "%s", Command);
	ATCommand.CMD[ATCommand.lenCMD] = 0;
	
	if(ExpectRsp1 != NULL) sprintf(ATCommand.ExpectRsp[AT_RSP1], "%s", ExpectRsp1);
	else ATCommand.ExpectRsp[AT_RSP1][0] = 0;
	
	if(ExpectRsp2 != NULL) sprintf(ATCommand.ExpectRsp[AT_RSP2], "%s", ExpectRsp2);
	else ATCommand.ExpectRsp[AT_RSP2][0] = 0;
	
	ATCommand.RetryCountAT = RetryCount;
	ATCommand.SendATCallBack = CallBackFunction;
	ATCommand.TimeoutAT = Timeout;
	ATCommand.CurrentTimeoutAT = 0;

	SendATCommand(ATCommand.CMD);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void AT_ForceRetry(void)
{
	ATCommand.CurrentTimeoutAT = ATCommand.TimeoutAT - 20*TIMER_AT_PERIOD;
}

/* ====================== private functions =========================== */
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void SendATCommand(const char *cmd)
{
	DEBUG("[AT] Sending: %s", cmd);
	memset(&ATRspBuffer, 0, sizeof(ATRspBuffer));
	ATCommand.waitRsp = SIMCOM_WAITING_RSP;
	ATCommand.countRsp = AT_RSP1;

	UART_SendString(CELL_PORT, (uint8_t*)ATCommand.CMD);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void RetrySendAT(void)
{
	DEBUG("[AT] Retry ");
	SendATCommand(ATCommand.CMD);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static uint8_t CheckTimeout(void)
{
	if(ATCommand.TimeoutAT > 0 && ATCommand.CurrentTimeoutAT < ATCommand.TimeoutAT)
	{
		ATCommand.CurrentTimeoutAT += TIMER_AT_PERIOD;
		if(ATCommand.CurrentTimeoutAT >= ATCommand.TimeoutAT)
		{
//			ATCommand.CurrentTimeoutAT -= ATCommand.TimeoutAT;
			ATCommand.CurrentTimeoutAT = 0;
			if(ATCommand.RetryCountAT > 0)
			{							
				ATCommand.RetryCountAT--;				
				DEBUG("[AT] Timeout %d", ATCommand.RetryCountAT);
				RetrySendAT();
			}
			else
			{					
				ATCommand.waitRsp = SIMCOM_NO_WAIT_RSP;
				if(ATCommand.SendATCallBack != NULL)
				{
					ATCommand.TimeoutAT = 0;
					ATCommand.SendATCallBack(EVENT_TIMEOUT, "AT TIMEOUT");
				}
				else
				{
					DEBUG("[AT] Event timeout without callback function\r\n");
				}
			}
			return 1;
		}
	}
	return 0;
}		
