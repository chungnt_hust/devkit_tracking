/*****************************************************************************
* File Name          : BC45_Board_ASIC_Category.h
* Author             : CrystalSu
* Version            : V1.0.0.0
* Date               : Feb 06, 2020
* Description        : 
******************************************************************************/   

#ifndef BC45_BOARD_ASIC_CATEGORY_H 
#define BC45_BOARD_ASIC_CATEGORY_H

#include <stdint.h>
//#include "DES.h"
#include "BC45_Chip_Function.h"

#define LEN_HARDWARE_PARAM_BYTE				16
#define HARDWARE_PARAM_START_ADDR			0x60

#define HARDWARE_CHECKSUM_INDEX 				LEN_HARDWARE_PARAM_BYTE - 1 
#define ASIC_EXE_TIMEOUT_VAL_INDEX_L		LEN_HARDWARE_PARAM_BYTE - 2
#define ASIC_EXE_TIMEOUT_VAL_INDEX_H 		LEN_HARDWARE_PARAM_BYTE - 3
#define DRIVER_CONFIG_INDEX  						LEN_HARDWARE_PARAM_BYTE - 4
#define DEVICE_ID_INDEX  								LEN_HARDWARE_PARAM_BYTE - 5 

/******************************************************************************/ 

#define Ana_Param_14443A_106 				0xA0 
#define Ana_Param_14443A_212 				0xA1 
#define Ana_Param_14443A_424 				0xA2 
#define Ana_Param_14443A_848 				0xA3 
#define Ana_Param_14443B_106 				0xB0 
#define Ana_Param_14443B_212 				0xB1 
#define Ana_Param_14443B_424 				0xB2 
#define Ana_Param_14443B_848 				0xB3 
#define Ana_Param_Reserved_212      0xC0
#define Ana_Param_Reserved_424      0xC1
#define Ana_Param_15693_1sub_lo     0xD0
#define Ana_Param_15693_1sub_hi     0xD1
#define Ana_Param_15693_1sub_uhi    0xD2
#define Ana_Param_15693_2sub_lo     0xD3
#define Ana_Param_15693_2sub_hi     0xD4


#define Ana_Param_GsCfgMod  				0x00 
#define Ana_Param_BitPhase  				0x01 
#define Ana_Param_Rx_Threshold 			0x02 
#define Ana_Param_BPSK_Demod  		  0x03
#define Ana_Param_BPSK_RxControl_3  0x04
#define Ana_Param_BPSK_AnaAdjust1  	0x05
#define Ana_Param_BPSK_AnaAdjust2  	0x06
#define Ana_Param_BPSK_AnaAdjust3  	0x07

//-------------------------------------------------------------------------//
//------------------------ Define Board ---------------------------//
//-------------------------------------------------------------------------//
//#define DEFAULT_ASIC_EXE_TIMEOUT		3000 
//#define MINIMUM_ASIC_EXE_TIMEOUT		500 
#define DEFAULT_ASIC_EXE_TIMEOUT		2 
#define MINIMUM_ASIC_EXE_TIMEOUT		1 

//-------------------------------------------------------------------------//
//---------------------- Board ASIC Command ------------------------//
//-------------------------------------------------------------------------//

// BC45_Board_ASIC Cmd
#define BC45_BOARD_ASIC_CMD_TYPE		0x01

#define Reset_BC45								0x20
#define Read_Reg_BC45							0x21
#define Write_Reg_BC45						0x22	 								
#define Read_Mutiple_Reg_BC45		  0x23												
#define Write_Mutiple_Reg_BC45	  0x24												
//#define Read_E2_BC45							0x25												
//#define Write_E2_BC45							0x26												
//#define Load_E2_Config_BC45		    0x27												
#define Cal_CRC											0x28												

#define ON_Field_BC45							0x30
#define OFF_Field_BC45						0x31

#define Set_TimeOut_Wait_Tag_Resp_BC45	0x42

#define BC45_Diagnose_Cmd								0xF0		
#define BC45_Init_Standard_Ana_Param		0xF1
#define BC45_Set_Standard_Ana_Param	    0xF2
#define BC45_Get_Standard_Ana_Param 		0xF3
#define BC45_Get_ASIC_Version 					0xF4

/// HTK Proprietary & Test CMD ///////
#define Write_Read_Reg_Check	            0xE0
#define Reset_BC45_Without_Init	      	0xE1

//-------------------------------------------------------------------------//
//-------------------------------------------------------------------------//

/******************************************************************************/ 

uint8_t BC45_Board_ASIC_Command( uint8_t Command, uint8_t *Param, uint16_t LenParam, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

uint8_t Read_Device_ID( uint8_t *Device_ID ) ;
uint8_t BC45_Board_Write_Device_ID( uint8_t NEW_DEVICE_ID ) ;

uint8_t Read_Driver_Config( uint8_t *Driver_Config ) ;
uint8_t BC45_Board_Write_Driver_Config( uint8_t Driver_Config ) ;

uint16_t Read_ASIC_Exe_Timeout_Val( uint16_t *ASIC_Exe_Timeout_Val ) ;
uint8_t BC45_Board_Write_ASIC_Exe_Timeout_Val( uint16_t Timeout_Value ) ;

uint8_t BC45_Board_ReadRegBC45( uint8_t Address_Rd, uint8_t *Data_Rd, uint16_t *LenData_Rd ) ;
uint8_t BC45_Board_Read_Mutiple_RegBC45( uint8_t *Address_Rd, uint8_t Num_Address_Rd, uint8_t *Data_Rd, uint16_t *LenData_Rd ) ;
uint8_t BC45_Board_WriteRegBC45( uint8_t Address_Wr, uint8_t Data_Wr ) ;
uint8_t BC45_Board_Write_Mutiple_RegBC45( uint8_t *Address_Wr, uint8_t *Data_Wr, uint8_t Num_Address_Wr ) ;

uint8_t Load_BC45_Board_Property( void ) ;
uint8_t Read_Hardware_Parameter( uint8_t Start_Addr, uint8_t Len_Read_Hardware_Param, uint8_t *Hardware_Param ) ;
uint8_t Cal_CheckSum( uint8_t *Data_Cal, uint8_t Len_Data_Cal ) ;
uint8_t Read_And_Check_Hardware_Parameter( uint8_t Start_Addr, uint8_t Len_Read_Hardware_Param, uint8_t *Hardware_Param ) ;
uint8_t Write_Hardware_Parameter( uint8_t Start_Addr, uint8_t *Hardware_Param, uint8_t Len_Write_Hardware_Param ) ;
uint8_t Write_Specific_Hardware_Parameter( uint8_t Index_Hardware_Param, uint8_t *Data, uint8_t LenData ) ;
uint8_t Initial_BC45_Board( void ) ;

void Initial_Standard_Analog_Param( void ) ;
void Config_Standard_Analog_Param ( uint8_t Standard , tStandard_Param* Standard_Param_In) ;
void Load_Standard_Analog_Param ( uint8_t Standard , tStandard_Param* Standard_Param_In) ;
uint8_t BC45_Board_Get_Analog_Param( uint8_t Standard, uint8_t *Addr_AnaParam_Rd, uint8_t Num_Address_Rd, uint8_t *Data_AnaParam_Rd, uint16_t *LenData_Rd ) ;
uint8_t BC45_Board_Set_Analog_Param( uint8_t Standard, uint8_t *Address_AnaParam_Wr, uint8_t *Data_AnaParam_Wr, uint8_t Num_Address_Wr ) ;


#endif
