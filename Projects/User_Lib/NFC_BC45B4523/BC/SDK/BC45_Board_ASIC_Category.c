/*****************************************************************************
* File Name          : BC45_Board_ASIC_Category.c
* Author             : CrystalSu
* Version            : V1.0.0.0
* Date               : Feb 10, 2020
* Description        : 
*                       - Initial_Standard_Analog_Param
*                       - Config_Standard_Analog_Param
*                       - Load_Standard_Analog_Param
*                       - BC45_Board_Get_Analog_Param
*                       - BC45_Board_Set_Analog_Param
******************************************************************************/   

#include <string.h>
#include "BC45_Board_Define.h"
#include "BC45_Chip_Function.h"
#include "Common_Utility_Category.h"
#include "BC45_Board_ASIC_Category.h"
#include "BC45_Spi.h"

uint8_t          Hardware_Parameter[LEN_HARDWARE_PARAM_BYTE] ;
tStandard_Param  AnalogCfg_Parameter ;


/*******************************************************************************
* Function Name : Initial_BC45_Board
* Description   : Initial BC45 Board paremeter from default values store in BC45 EEPROM 
*			      to BC45 registers and MCU variables.
*				  This routine includes BC45 Diagnosis, read paramters from BC45 EEPROM.
*				  If reading from BC45 fails by any reasons, default values are used.
* Input         : None
* Output        : None
* Return        : 
*				  	- Status
*						- _SUCCESS_				  
*						- BC45_DIAGNOSE_ERR				  
*						- CHECKSUM_HARDWARE_PARAM_FAIL
*						- ACCESS_E2_ERR
*						- ASIC_EXE_TIMEOUT
*******************************************************************************/
uint8_t Initial_BC45_Board( void )
{
	uint8_t Status;
	
	BC45_BOARD_PROPERTY.Device_ID = 0x00;
	BC45_BOARD_PROPERTY.Driver_Config_Type = 0x00;
	BC45_BOARD_PROPERTY.ASIC_Exe_Timeout_Val = DEFAULT_ASIC_EXE_TIMEOUT; // Initial Timeout 3 S
	
	// Reset BC
	BC45_RSTPD_SET();
	HAL_Delay(5);
	BC45_RSTPD_CLR();
	HAL_Delay(5);					//Hard PD mode wakeup time
	
	Initial_Standard_Analog_Param(); // Initial Parameter
	BC45_InitChip(BC45_BOARD_PROPERTY.Driver_Config_Type);
	
	Status = BC45_Diagnose();
	if(Status == _SUCCESS_) 
	{
		BC45_Config_Driver(BC45_BOARD_PROPERTY.Driver_Config_Type);
		DEBUG("[BC45_Board_ASIC] Diagnose successfully\r\n");
	}
	else // BC45_Diagnose Error
	{
		DEBUG("[BC45_Board_ASIC] Diagnose failed\r\n");
	}
	
	return Status ;
}

/*******************************************************************************
* Function Name : BC45_Board_Get_Analog_Param
* Description   : Get BC45 Register Parameter from buffer following input standard
* Input         : 
*                   - Standard
*                   - Analog_Param_Address
*                   - Number of Analog_Param_Address
* Output        : 
*                   - Analog_Param_Value
*                   - Length
* Return        :
*                   -_SUCCESS_
*******************************************************************************/
uint8_t BC45_Board_Get_Analog_Param( uint8_t Standard, uint8_t *Addr_AnaParam_Rd, uint8_t Num_Address_Rd, uint8_t *Data_AnaParam_Rd, uint16_t *LenData_Rd ) 
{
  	uint8_t  i ;
        uint8_t  Address_AnaParam ;
        
        Load_Standard_Analog_Param (Standard , &AnalogCfg_Parameter); 
	
	for ( i = 0 ; i < Num_Address_Rd ; i++ )
	{ 
          Address_AnaParam = * (Addr_AnaParam_Rd + i);
          
         switch ( Address_AnaParam  )
         {   case Ana_Param_GsCfgMod :
               *(Data_AnaParam_Rd + i) = AnalogCfg_Parameter.GsCfgMod ;
            break ;
            
            case Ana_Param_BitPhase :
               *(Data_AnaParam_Rd + i) = AnalogCfg_Parameter.BitPhase ;
            break ;
            
            case Ana_Param_Rx_Threshold :
                *(Data_AnaParam_Rd + i) = AnalogCfg_Parameter.Rx_Threshold ;
            break ;
            
            case Ana_Param_BPSK_Demod :
                *(Data_AnaParam_Rd + i) = AnalogCfg_Parameter.BPSK_Demod ;
            break ;
            
            case Ana_Param_BPSK_RxControl_3 :
                *(Data_AnaParam_Rd + i) = AnalogCfg_Parameter.RxControl_3 ;
            break ;
            
            case Ana_Param_BPSK_AnaAdjust1 :
                *(Data_AnaParam_Rd + i) = AnalogCfg_Parameter.AnaAdjust1 ;
            break ;
            
            case Ana_Param_BPSK_AnaAdjust2 :
                *(Data_AnaParam_Rd + i) = AnalogCfg_Parameter.AnaAdjust2 ;
            break ;
            
            case Ana_Param_BPSK_AnaAdjust3 :
                *(Data_AnaParam_Rd + i) = AnalogCfg_Parameter.AnaAdjust3 ;
            break ;
            
            default :
            break ;
          }	
	}
        *( LenData_Rd ) = ( uint16_t ) Num_Address_Rd ;
	
	return _SUCCESS_ ;
  
  
}

/*******************************************************************************
* Function Name : BC45_Board_Set_Analog_Param
* Description   : Write BC45 Register Parameter into buffer following input standard
* Input         : 
*                   - Standard
*                   - Addr_AnaParam_Wr
*                   - Data_AnaParam_Wr
*                   - Num_Address_Wr
* Output        : None
* Return        :
*                   -_SUCCESS_
*******************************************************************************/
uint8_t BC45_Board_Set_Analog_Param( uint8_t Standard, uint8_t *Addr_AnaParam_Wr, uint8_t *Data_AnaParam_Wr, uint8_t Num_Address_Wr ) 
{	
	uint8_t i ;
        uint8_t Address_AnaParam ;
        

        Load_Standard_Analog_Param (Standard , &AnalogCfg_Parameter); 
        
        for ( i = 0; i < Num_Address_Wr ; i++ )
        {  Address_AnaParam =  * (Addr_AnaParam_Wr + i)  ;
         
          switch ( Address_AnaParam )
          {	
            case Ana_Param_GsCfgMod :
                AnalogCfg_Parameter.GsCfgMod = *(Data_AnaParam_Wr + i);
            break ;
            
            case Ana_Param_BitPhase :
                AnalogCfg_Parameter.BitPhase = *(Data_AnaParam_Wr + i);
            break ;
            
            case Ana_Param_Rx_Threshold :
                AnalogCfg_Parameter.Rx_Threshold = *(Data_AnaParam_Wr + i);
            break ;
            
            case Ana_Param_BPSK_Demod :
                AnalogCfg_Parameter.BPSK_Demod = *(Data_AnaParam_Wr + i);
            break ;
            
            case Ana_Param_BPSK_RxControl_3 :
                AnalogCfg_Parameter.RxControl_3 = *(Data_AnaParam_Wr + i);
            break ;
            
            case Ana_Param_BPSK_AnaAdjust1 :
                AnalogCfg_Parameter.AnaAdjust1 = *(Data_AnaParam_Wr + i);
            break ;
            
            case Ana_Param_BPSK_AnaAdjust2 :
                AnalogCfg_Parameter.AnaAdjust2 = *(Data_AnaParam_Wr + i);
            break ;
            
            case Ana_Param_BPSK_AnaAdjust3 :
                AnalogCfg_Parameter.AnaAdjust3 = *(Data_AnaParam_Wr + i);
            break ;
            
            default :
            break ;
          };
        }
        
        Config_Standard_Analog_Param (Standard , &AnalogCfg_Parameter);
              
        return _SUCCESS_ ;

}

/*******************************************************************************
* Function Name : Initial_Standard_Analog_Param
* Description   : Initialze some BC45 Register Parameter for all standard into Firmware Buffer
* Input         : None      
* Output        : None
* Return        : None
*******************************************************************************/
void Initial_Standard_Analog_Param( void  ) 
{	
	AnalogCfg_Parameter.GsCfgMod     = 0x10 ;
	AnalogCfg_Parameter.Rx_Threshold = 0x4C ;
	AnalogCfg_Parameter.BPSK_Demod   = 0x02 ;
	AnalogCfg_Parameter.RxControl_3  = 0xF0 ;
	
	AnalogCfg_Parameter.AnaAdjust1   = 0xBB ;
	
	AnalogCfg_Parameter.AnaAdjust2   = 0x19 ; // Gain State 3
	AnalogCfg_Parameter.AnaAdjust3   = 0x00 ;
	
	AnalogCfg_Parameter.BitPhase     = 0x40 ;
	Config_Standard_Analog_Param (Ana_Param_14443A_106 , &AnalogCfg_Parameter);
	
	AnalogCfg_Parameter.BitPhase     = 0x88 ;
	Config_Standard_Analog_Param (Ana_Param_14443A_212 , &AnalogCfg_Parameter);
	
	AnalogCfg_Parameter.BitPhase     = 0x32 ;
	Config_Standard_Analog_Param (Ana_Param_14443A_424 , &AnalogCfg_Parameter);

	AnalogCfg_Parameter.BitPhase     = 0x40 ;
	Config_Standard_Analog_Param (Ana_Param_14443A_848 , &AnalogCfg_Parameter);

	AnalogCfg_Parameter.GsCfgMod     = 0x17 ;
	AnalogCfg_Parameter.BitPhase     = 0x20 ;
	Config_Standard_Analog_Param (Ana_Param_14443B_106 , &AnalogCfg_Parameter);
	Config_Standard_Analog_Param (Ana_Param_14443B_212 , &AnalogCfg_Parameter);
	Config_Standard_Analog_Param (Ana_Param_14443B_424 , &AnalogCfg_Parameter);
	Config_Standard_Analog_Param (Ana_Param_14443B_848 , &AnalogCfg_Parameter);

	AnalogCfg_Parameter.Rx_Threshold = 0x6A ;
	AnalogCfg_Parameter.AnaAdjust2   = 0x21 ; // Gain State 3
	
	AnalogCfg_Parameter.BitPhase     = 0x0E ;
	Config_Standard_Analog_Param (Ana_Param_15693_1sub_lo , &AnalogCfg_Parameter);

	AnalogCfg_Parameter.BitPhase     = 0x40 ;
	Config_Standard_Analog_Param (Ana_Param_15693_1sub_hi , &AnalogCfg_Parameter);

	AnalogCfg_Parameter.BitPhase     = 0x88 ;
	Config_Standard_Analog_Param (Ana_Param_15693_1sub_uhi , &AnalogCfg_Parameter);
	
	AnalogCfg_Parameter.Rx_Threshold = 0xAA ;
	
	AnalogCfg_Parameter.BitPhase     = 0x0E ;
	Config_Standard_Analog_Param (Ana_Param_15693_2sub_lo , &AnalogCfg_Parameter);
	
	AnalogCfg_Parameter.BitPhase     = 0x40 ;
	Config_Standard_Analog_Param (Ana_Param_15693_2sub_hi , &AnalogCfg_Parameter);

}

/*******************************************************************************
* Function Name : Load_Standard_Analog_Param
* Description   : Load some BC45 Register Parameter to buffer following input standard
* Input         :     
*                         - Stadard
*                         - AnalogCfg_Parameter(struct - output)    
* Output        : None
* Return        : None
*******************************************************************************/
void Load_Standard_Analog_Param ( uint8_t Standard , tStandard_Param* Standard_Param_Out) 
{
   switch(Standard)      
      {
      case Ana_Param_14443A_106 :
            * Standard_Param_Out = Param_14443A_106;
        break;
        
      case Ana_Param_14443A_212 :
            * Standard_Param_Out = Param_14443A_212;
        break;
        
      case Ana_Param_14443A_424 :
            * Standard_Param_Out = Param_14443A_424;
        
        break;
      
      case Ana_Param_14443A_848 :
            * Standard_Param_Out = Param_14443A_848;
      
        
      case Ana_Param_14443B_106 :
            * Standard_Param_Out = Param_14443B_106;
        break;
        
      case Ana_Param_14443B_212 :
            * Standard_Param_Out = Param_14443B_212;
        break;
        
      case Ana_Param_14443B_424 :
            * Standard_Param_Out = Param_14443B_424;
        break;
      
      case Ana_Param_14443B_848 :
            * Standard_Param_Out = Param_14443B_848;
        break;  

      case Ana_Param_15693_1sub_lo :
           * Standard_Param_Out = Param_15693_1sub_lo;   
        break;  

      case Ana_Param_15693_1sub_hi :
           * Standard_Param_Out = Param_15693_1sub_hi;   
        break;      

      case Ana_Param_15693_1sub_uhi :
           * Standard_Param_Out = Param_15693_1sub_uhi;
        break;   
        
      case Ana_Param_15693_2sub_lo :
           * Standard_Param_Out = Param_15693_2sub_lo;       
        break;  

      case Ana_Param_15693_2sub_hi :
           * Standard_Param_Out = Param_15693_2sub_hi;
        break; 
        
      default :
           * Standard_Param_Out = Param_14443A_106;
        break ;  
      }
}
  



/*******************************************************************************
* Function Name : Config_Standard_Analog_Param
* Description   : Configure some BC45 Register Parameter for each Standard following input paramater
* Input         :     
*                         - Stadard
*                         - AnalogCfg_Parameter (struct - input)    
* Output        : None
* Return        : None
*******************************************************************************/
void Config_Standard_Analog_Param( uint8_t Standard , tStandard_Param *AnalogCfg_Parameter) 
{	
 
      switch(Standard)      
      {
      case Ana_Param_14443A_106 :
            Param_14443A_106.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_14443A_106.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_14443A_106.Rx_Threshold   = AnalogCfg_Parameter->Rx_Threshold;
            Param_14443A_106.BPSK_Demod     = AnalogCfg_Parameter->BPSK_Demod;
            Param_14443A_106.RxControl_3    = AnalogCfg_Parameter->RxControl_3;
            Param_14443A_106.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_14443A_106.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_14443A_106.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        break;
        
      case Ana_Param_14443A_212 :
            Param_14443A_212.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_14443A_212.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_14443A_212.Rx_Threshold    = AnalogCfg_Parameter->Rx_Threshold;
            Param_14443A_212.BPSK_Demod      = AnalogCfg_Parameter->BPSK_Demod;
            Param_14443A_212.RxControl_3     = AnalogCfg_Parameter->RxControl_3;
            Param_14443A_212.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_14443A_212.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_14443A_212.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        break;
        
      case Ana_Param_14443A_424 :
            Param_14443A_424.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_14443A_424.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_14443A_424.Rx_Threshold    = AnalogCfg_Parameter->Rx_Threshold;
            Param_14443A_424.BPSK_Demod      = AnalogCfg_Parameter->BPSK_Demod;
            Param_14443A_424.RxControl_3     = AnalogCfg_Parameter->RxControl_3;
            Param_14443A_424.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_14443A_424.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_14443A_424.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        
        break;
      
      case Ana_Param_14443A_848 :
            Param_14443A_848.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_14443A_848.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_14443A_848.Rx_Threshold   = AnalogCfg_Parameter->Rx_Threshold;
            Param_14443A_848.BPSK_Demod     = AnalogCfg_Parameter->BPSK_Demod;
            Param_14443A_848.RxControl_3    = AnalogCfg_Parameter->RxControl_3;
            Param_14443A_848.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_14443A_848.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_14443A_848.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        break;
      
        
      case Ana_Param_14443B_106 :
            Param_14443B_106.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_14443B_106.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_14443B_106.Rx_Threshold    = AnalogCfg_Parameter->Rx_Threshold;
            Param_14443B_106.BPSK_Demod      = AnalogCfg_Parameter->BPSK_Demod;
            Param_14443B_106.RxControl_3     = AnalogCfg_Parameter->RxControl_3;
            Param_14443B_106.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_14443B_106.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_14443B_106.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        break;
        
      case Ana_Param_14443B_212 :
            Param_14443B_212.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_14443B_212.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_14443B_212.Rx_Threshold    = AnalogCfg_Parameter->Rx_Threshold;
            Param_14443B_212.BPSK_Demod      = AnalogCfg_Parameter->BPSK_Demod;
            Param_14443B_212.RxControl_3     = AnalogCfg_Parameter->RxControl_3;
            Param_14443B_212.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_14443B_212.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_14443B_212.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        break;
        
      case Ana_Param_14443B_424 :
            Param_14443B_424.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_14443B_424.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_14443B_424.Rx_Threshold    = AnalogCfg_Parameter->Rx_Threshold;
            Param_14443B_424.BPSK_Demod      = AnalogCfg_Parameter->BPSK_Demod;
            Param_14443B_424.RxControl_3     = AnalogCfg_Parameter->RxControl_3;
            Param_14443B_424.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_14443B_424.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_14443B_424.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        
        break;
      
      case Ana_Param_14443B_848 :
            Param_14443B_848.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_14443B_848.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_14443B_848.Rx_Threshold    = AnalogCfg_Parameter->Rx_Threshold;
            Param_14443B_848.BPSK_Demod      = AnalogCfg_Parameter->BPSK_Demod;
            Param_14443B_848.RxControl_3     = AnalogCfg_Parameter->RxControl_3;
            Param_14443B_848.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_14443B_848.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_14443B_848.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        break;  

      case Ana_Param_15693_1sub_lo :
            Param_15693_1sub_lo.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_15693_1sub_lo.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_15693_1sub_lo.Rx_Threshold    = AnalogCfg_Parameter->Rx_Threshold;
            Param_15693_1sub_lo.BPSK_Demod      = AnalogCfg_Parameter->BPSK_Demod;
            Param_15693_1sub_lo.RxControl_3     = AnalogCfg_Parameter->RxControl_3;
            Param_15693_1sub_lo.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_15693_1sub_lo.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_15693_1sub_lo.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;        
        break;  

      case Ana_Param_15693_1sub_hi :
            Param_15693_1sub_hi.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_15693_1sub_hi.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_15693_1sub_hi.Rx_Threshold    = AnalogCfg_Parameter->Rx_Threshold;
            Param_15693_1sub_hi.BPSK_Demod      = AnalogCfg_Parameter->BPSK_Demod;
            Param_15693_1sub_hi.RxControl_3     = AnalogCfg_Parameter->RxControl_3;
            Param_15693_1sub_hi.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_15693_1sub_hi.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_15693_1sub_hi.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        break;      

      case Ana_Param_15693_1sub_uhi :
            Param_15693_1sub_uhi.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_15693_1sub_uhi.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_15693_1sub_uhi.Rx_Threshold    = AnalogCfg_Parameter->Rx_Threshold;
            Param_15693_1sub_uhi.BPSK_Demod      = AnalogCfg_Parameter->BPSK_Demod;
            Param_15693_1sub_uhi.RxControl_3     = AnalogCfg_Parameter->RxControl_3;
            Param_15693_1sub_uhi.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_15693_1sub_uhi.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_15693_1sub_uhi.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        break;   
        
      case Ana_Param_15693_2sub_lo :
            Param_15693_2sub_lo.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_15693_2sub_lo.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_15693_2sub_lo.Rx_Threshold    = AnalogCfg_Parameter->Rx_Threshold;
            Param_15693_2sub_lo.BPSK_Demod      = AnalogCfg_Parameter->BPSK_Demod;
            Param_15693_2sub_lo.RxControl_3     = AnalogCfg_Parameter->RxControl_3;
            Param_15693_2sub_lo.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_15693_2sub_lo.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_15693_2sub_lo.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;        
        break;  

      case Ana_Param_15693_2sub_hi :
            Param_15693_2sub_hi.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_15693_2sub_hi.BitPhase      = AnalogCfg_Parameter->BitPhase;
            Param_15693_2sub_hi.Rx_Threshold    = AnalogCfg_Parameter->Rx_Threshold;
            Param_15693_2sub_hi.BPSK_Demod      = AnalogCfg_Parameter->BPSK_Demod;
            Param_15693_2sub_hi.RxControl_3     = AnalogCfg_Parameter->RxControl_3;
            Param_15693_2sub_hi.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_15693_2sub_hi.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_15693_2sub_hi.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        break; 
        
      default :
            Param_14443A_106.GsCfgMod       = AnalogCfg_Parameter->GsCfgMod;
            Param_14443A_106.Rx_Threshold   = AnalogCfg_Parameter->Rx_Threshold;
            Param_14443A_106.BPSK_Demod     = AnalogCfg_Parameter->BPSK_Demod;
            Param_14443A_106.RxControl_3    = AnalogCfg_Parameter->RxControl_3;
            Param_14443A_106.AnaAdjust1     = AnalogCfg_Parameter->AnaAdjust1;
            Param_14443A_106.AnaAdjust2     = AnalogCfg_Parameter->AnaAdjust2;
            Param_14443A_106.AnaAdjust3     = AnalogCfg_Parameter->AnaAdjust3;
        break ;  
      }
}

uint8_t Cal_CheckSum( uint8_t *Data_Cal, uint8_t Len_Data_Cal )
{
	uint8_t  i ; 
	uint8_t  CHK_SUM_CAL ;
	
	CHK_SUM_CAL	= 0x00 ;
	for ( i = 0 ; i < Len_Data_Cal ; i++ )
	{	 
		CHK_SUM_CAL ^= *( Data_Cal + i ) ;
	}

	return CHK_SUM_CAL ;
}

/*******************************************************************************
* Function Name : BC45_Board_ReadRegBC45
* Description   : Read single single registers of BC45
* Input         : 
*				  - Address_Rd
* Output        : 
*				  - Data_Resp
*				  - LenData_Resp
* Return        : 
*				  - Response
*						- _SUCCESS_				  
*******************************************************************************/
uint8_t BC45_Board_ReadRegBC45( uint8_t Address_Rd, uint8_t *Data_Rd, uint16_t *LenData_Rd )
{
	BC45_SPI_readSingleRegister(Address_Rd, Data_Rd);
	*( LenData_Rd ) = 1 ; 

	return _SUCCESS_ ;	
}

/*******************************************************************************
* Function Name : BC45_Board_Read_Mutiple_RegBC45
* Description   : Read data from multiple registers of BC45
* Input         : 
*				  - Address_Rd
*				  - Num_Address_Rd
* Output        : 
*				  - Data_Resp
*				  - LenData_Resp
* Return        : 
*				  - Response
*						- _SUCCESS_				  
*******************************************************************************/
uint8_t BC45_Board_Read_Mutiple_RegBC45( uint8_t *Address_Rd, uint8_t Num_Address_Rd, uint8_t *Data_Rd, uint16_t *LenData_Rd ) 
{ 
	uint8_t  i ;
	
	for ( i = 0 ; i < Num_Address_Rd ; i++ )
	{
		BC45_SPI_readSingleRegister(*( Address_Rd + i ), ( Data_Rd + i ));
	}
	*( LenData_Rd ) = ( uint16_t ) Num_Address_Rd ;
	
	return _SUCCESS_ ;
}

/*******************************************************************************
* Function Name : BC45_Board_Read_Mutiple_RegBC45
* Description   : Write single data to single registers of BC45
* Input         : 
*				  - Address_Wr
*				  - Data_Wr
* Output        : 
* Return        : 
*				  - Response
*						- _SUCCESS_				  
*******************************************************************************/
uint8_t BC45_Board_WriteRegBC45( uint8_t Address_Wr, uint8_t Data_Wr )
{
	BC45_SPI_writeSingleRegister(Address_Wr, Data_Wr);

	return _SUCCESS_ ;	
}

/*******************************************************************************
* Function Name : BC45_Board_Write_Mutiple_RegBC45
* Description   : Write multiple data to multiple register of BC45
* Input         : 
*				  - Address_Wr
*				  - Data_Wr
*				  - Num_Address_Wr
* Output        : 
* Return        : 
*				  - Response
*						- _SUCCESS_				  
*******************************************************************************/
uint8_t BC45_Board_Write_Mutiple_RegBC45( uint8_t *Address_Wr, uint8_t *Data_Wr, uint8_t Num_Address_Wr ) 
{ 
	uint8_t  i ;
	
	for ( i = 0 ; i < Num_Address_Wr ; i++ )
	{
		BC45_SPI_writeSingleRegister(*( Address_Wr + i ), *( Data_Wr + i ));
	}
	
	return _SUCCESS_ ;
}
