/*****************************************************************************
* File Name          : ISO14443B_Function.h
* Author             : Crystal Su
* Version            : V1.0.0.0
* Date               : March 25, 2020
* Description        : 
******************************************************************************/   

#ifndef ISO14443B_CATEGORY_H 
#define ISO14443B_CATEGORY_H

#include <stdint.h>

#define APf			0x05  

// Length of response data for each command
#define LEN_ISO14443B_REQUEST_RF_RESP	12
#define LEN_ISO14443B_WAKEUP_RF_RESP	12

//-------------------------------------------------------------------------//
//-------------------------- ISO14443B Command ----------------------------//
//-------------------------------------------------------------------------//

// Config Reader Cmd
#define B_Config_ISO14443B			0x00
#define B_Config_Speed_Reader		0x01
#define B_Get_Speed_Reader			0x02
#define B_SOFEOF_Sel				0x03

// Standard Cmd
#define B_Request					0x10
#define B_WakeUp					0x11
#define B_ATTRIB					0x12
#define B_Halt						0x13

// Combo Cmd  
#define B_WakeUp_ATTRIB               0xB0

// Special Cmd
#define B_Transparent_With_CRC		    0xC0
#define B_Transparent_Without_CRC	    0xC1
#define B_Transparent_Config_CRC	    0xC3
#define B_Transparent_With_RxMultiple	    0xC4

// Proprietary Cmd
#define B_EMV_Load_Modulation                 0xE1

//-------------------------------------------------------------------------//
//-------------------------------------------------------------------------//

typedef struct Struct_tISO14443B_Property
{

	uint8_t  Speed ;	
	
} tISO14443B_Property ;


uint8_t ISO14443B_Command( uint8_t Command, uint8_t *Param, uint16_t LenParam, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

uint8_t ISO14443B_Config ( uint8_t Speed ) ;
uint8_t ISO14443B_Get_Speed_Reader( uint8_t *Speed, uint16_t *LenSpeed ) ;

uint8_t ISO14443B_Request( uint8_t AFI, uint8_t Num_Slots_N, uint8_t *Slot_Num, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443B_WakeUp ( uint8_t AFI, uint8_t Num_Slots_N, uint8_t *Slot_Num, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443B_ATTRIB( uint8_t *PUPI, uint8_t *Param, uint8_t *Higher_Layer, uint16_t LenHigher_Layer, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443B_Halt( uint8_t *PUPI, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

uint8_t ISO14443B_WakeUp_ATTRIB ( uint8_t AFI, uint8_t *Param , uint8_t *Higher_Layer, uint16_t LenHigher_Layer , uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443B_EMV_Load_Modulation ( uint8_t Reset_Field_En , uint8_t Run_All_Step , uint8_t CMD_Category ,  uint8_t First_Step,  uint8_t No_Of_Step, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

#endif
