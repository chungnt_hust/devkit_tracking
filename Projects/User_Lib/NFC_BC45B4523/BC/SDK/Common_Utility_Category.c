/**
  ******************************************************************************
  * @file    Common_Utility_Category.c
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  *
**/

#include "Common_Utility_Category.h"

uint16_t UpdateCRC( uint8_t data, uint16_t *lpwCRC )
{	
	data 	= data ^ ( uint8_t )( *(lpwCRC) & 0x00FF ) ;
	data 	= data ^ ( data << 4 ) ;
	*(lpwCRC) = ( *(lpwCRC) >> 8 ) ^ ( ( (uint8_t)data ) << 8 ) ^ ( ( (uint16_t)data ) << 3 ) ^ ( ( (uint16_t)data ) >> 4 ) ;

	return *( lpwCRC ) ;
}

uint16_t Reverse_Data_16Bit( uint16_t Data )
{	
	uint8_t   i ;
	uint16_t  Reverse_Data ;
	
	Reverse_Data = 0 ;
	
	for ( i = 0 ; i < 16 ; i++ )
	{	
		Reverse_Data <<= 1 ;
		if( ( Data & 1 ) == 1 )
		{	
			Reverse_Data |= 1 ;
		}
		Data >>= 1 ;  
	};	
	
	return Reverse_Data ;
}
