/*****************************************************************************
* File Name          : ISO14443A_Category.h
* Author             : Crystal Su
* Version            : V1.0.0.0
* Date               : March 25, 2020
* Description        : 
******************************************************************************/     

#ifndef ISO14443A_CATEGORY_H 
#define ISO14443A_CATEGORY_H

#include <stdint.h>

#define A_CASCADE_LEVEL_1			1
#define A_CASCADE_LEVEL_2			2
#define A_CASCADE_LEVEL_3			3

#define A_UID_SINGLE_SIZE			4
#define A_UID_DOUBLE_SIZE			7	
#define A_UID_TRIPLE_SIZE			10

#define SAK_UID_COMPLETE_MASK		0x04
#define SAK_COMPLIANT_4_MASK		0x20

// Req_Mode
#define REQ_MODE_REQA				0x00
#define REQ_MODE_WUPA				0x01

// Mifare -- Authen cmd
#define USE_KEY_A				0x00
#define USE_KEY_B				0x01

#define MIFARE_UL_3DES_AUTH_HEADER_1            0xAF
#define MIFARE_UL_3DES_AUTH_HEADER_2            0x00

#define MIFARE_ACK				0x0A
#define MIFARE_NACK_NOT_ALLOWED			0x04
#define MIFARE_NACK_TRANSMISSION_ERR	        0x05
#define MIFARE_NACK_OTHERS	                0x00

// RF Cmd
#define ISO14443A_REQUEST_RF_CMD			0x26		
#define ISO14443A_WAKE_UP_RF_CMD			0x52
#define ISO14443A_CASCADE_LEVEL_1_RF_CMD	0x93
#define ISO14443A_CASCADE_LEVEL_2_RF_CMD	0x95
#define ISO14443A_CASCADE_LEVEL_3_RF_CMD	0x97
#define ISO14443A_HALT1_RF_CMD				0x50
#define ISO14443A_HALT2_RF_CMD				0x00

#define ISO14443A_RATS_RF_CMD				0xE0
#define ISO14443A_PPS_RF_CMD				0xD0

#define MIFARE_AUTHENT_WITH_KEY_A_RF_CMD	0x60
#define MIFARE_AUTHENT_WITH_KEY_B_RF_CMD	0x61
#define MIFARE_WRITE_BLOCK_RF_CMD	        0xA0
#define MIFARE_READ_BLOCK_RF_CMD	        0x30
#define MIFARE_DECREMENT_RF_CMD		        0xC0
#define MIFARE_INCREMENT_RF_CMD			0xC1
#define MIFARE_RESTORE_RF_CMD			0xC2
#define MIFARE_TRANSFER_RF_CMD			0xB0

/**********************************************/
#define MIFARE_WRITE_ULTRALIGHT_RF_CMD          0xA2
#define MIFARE_ULTRALIGHT_3DES_AUTHEN1_RF_CMD	0x1A
#define MIFARE_ULTRALIGHT_3DES_AUTHEN2_RF_CMD	0x00
/**********************************************/
#define NFC_TYPE1_READ_UID_RF_CMD               0x78
#define NFC_TYPE1_READ_ALL_BYTE_RF_CMD          0x00
#define NFC_TYPE1_READ_SINGLE_BYTE_RF_CMD       0x01
#define NFC_TYPE1_WRITE_WITH_ERASE_RF_CMD       0x53
#define NFC_TYPE1_WRITE_NO_ERASE_RF_CMD         0x1A

// Length of response data for each command
#define LEN_ISO14443A_REQUEST_RF_RESP		2
#define LEN_ISO14443A_WAKEUP_RF_RESP		2
#define LEN_ISO14443A_ANTICOLL_RF_RESP		5
#define LEN_ISO14443A_SELECT_RF_RESP		1
#define LEN_ISO14443A_READ_BLOCK_RF_RESP	16
#define LEN_ISO14443A_PPS_RF_RESP		1

#define LEN_NFC_TYPE1_READ_UID_RF_RESP          6  
#define LEN_NFC_TYPE1_READ_ALL_BYTE_RF_RESP     122  
#define LEN_NFC_TYPE1_READ_SINGLE_BYTE_RF_RESP  2 
#define LEN_NFC_TYPE1_WRITE_WITH_ERASE_RF_RESP  2 
#define LEN_NFC_TYPE1_WRITE_NO_ERASE_RF_RESP    2 

#define LEN_ISO14443A_3DES_Authent_RF_RESP      9

//-------------------------------------------------------------------------//
//-------------------------- ISO14443A Command ----------------------------//
//-------------------------------------------------------------------------//

// Config Reader Cmd
#define A_Config_ISO14443A			0x00
#define A_Config_Speed_Reader		0x01
#define A_Get_Speed_Reader			0x02

// Standard Cmd
#define A_Request					0x10
#define A_WakeUp					0x11
#define A_Anticoll					0x12												
#define A_Select					0x13
#define A_Halt						0x14
#define A_RATS						0x15
#define A_PPS						0x16

// Mifare Classic Custom Cmd
#define A_Loadkey					0x30
#define A_LoadkeyE2					0x31
#define A_Authentication			0x32												
#define A_Read_Block				0x33
#define A_Write_Block				0x34
#define A_Increment					0x35
#define A_Decrement					0x36
#define A_Restore					0x37												
#define A_Transfer					0x38

// Mifare Ultraligth Custom Cmd
#define A_Read_Ultralight 					0x40
#define A_Write_Compat_Ultralight           0x41
#define A_Write_Ultralight 			        0x42
#define A_3DES_Auth_Load_Key_Ultralight 	0x43
#define A_3DES_Auth_TxCMD_Ultralight 		0x44
#define A_3DES_Auth_TxEncipher_Ultralight 	0x45
#define A_3DES_Auth_Ultralight 			    0x46

// NFC Tag Type1 Custom Cmd
#define A_NFC_Type1_Read_UID				0x50
#define A_NFC_Type1_Read_All_Byte			0x51
#define A_NFC_Type1_Read_Single_Byte		0x52
#define A_NFC_Type1_Write_With_Erase		0x53
#define A_NFC_Type1_Write_No_Erase			0x54

// Combo Cmd
#define A_Req_Anti_Sel_Loadkey_Authen		0xA0
#define A_Req_Anti_Sel_Loadkey_Authen_Read	0xA1
#define A_Req_Anti_Sel_Loadkey_Authen_Write	0xA2
#define A_Increment_Transfer				0xA3
#define A_Decrement_Transfer				0xA4
#define A_Restore_Transfer					0xA5

#define A_Req_Anti_Sel				        0xB0
#define A_Req_Anti_Sel_RATS			        0xB1


// Special Cmd
#define A_Transparent_With_CRC		      	0xC0
#define A_Transparent_Without_CRC	      	0xC1
#define A_Transparent_Config_CRC	      	0xC3
#define A_Transparent_With_RxMultiple	    0xC4

#define A_Transparent_NFC_Type1_With_CRC    0xD0
#define A_Transparent_NFC_Type1_Without_CRC 0xD1
#define A_Transparent_NFC_Type1_Config_CRC  0xD2

// Proprietary Cmd
#define A_Anticoll_Check		      		0xE0
#define A_EMV_Load_Modulation               0xE1
#define A_Polling_Tags                      0xE2
  
//-------------------------------------------------------------------------//
//-------------------------------------------------------------------------//

typedef struct Struct_tISO14443A_Property
{

	uint8_t  Speed ;	
	
} tISO14443A_Property ;

uint8_t ISO14443A_Command( uint8_t Command, uint8_t *Param, uint16_t LenParam, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

uint8_t ISO14443A_Config( uint8_t Speed ) ; 
uint8_t ISO14443A_Get_Speed_Reader( uint8_t *Speed, uint16_t *LenSpeed ) ;

uint8_t ISO14443A_Request( uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_WakeUp( uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Anticoll( uint8_t Level, uint8_t CollMask_Value, uint8_t *NumColl, uint8_t *Coll_Pos, uint8_t *UID, uint16_t *LenUID ) ;
uint8_t ISO14443A_Select( uint8_t Level, uint8_t *UID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Halt( uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_RATS( uint8_t Parameter_Byte, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_PPS( uint8_t CID, uint8_t PPS0, uint8_t PPS1, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

// --------------- Mifare Cmd ----------------------------------------------------------------------------------------------------------//
uint8_t ISO14443A_Load_Key( uint8_t * Key ) ;
uint8_t ISO14443A_Authentication( uint8_t Select_Key, uint8_t Block_Num, uint8_t *UID, uint8_t LenUID ) ;
uint8_t ISO14443A_Read_Mifare_Block( uint8_t Block_Num, uint8_t *Data_Resp, uint16_t *LenData_Resp, uint8_t Check_Crypto );
uint8_t ISO14443A_Write_Mifare_Block( uint8_t Block_Num, uint8_t *Data_Wr, uint8_t *Data_Resp, uint16_t *LenData_Resp, uint8_t Check_Crypto );
uint8_t ISO14443A_Mifare_CMD_On_Value_Block( uint8_t Mifare_Cmd, uint8_t Block_Num, uint8_t *Value, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Decrement( uint8_t Block_Num, uint8_t *Decrement_Value, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Increment( uint8_t Block_Num, uint8_t *Increment_Value, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Restore( uint8_t Block_Num, uint8_t *Restore_Value, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Transfer( uint8_t Block_Num, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Write_Ultralight( uint8_t Block_Num, uint8_t *Data_Wr, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_3DES_Authent_Ultralight ( uint8_t *Key_in  ) ;
uint8_t ISO14443A_3DES_Authent_Ultralight_LoadKey ( uint8_t *Key_in ) ;
uint8_t ISO14443A_3DES_Authent_Ultralight_TxCMD   ( uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_3DES_Authent_Ultralight_TxEncipher ( uint8_t *Data, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

// --------------- NFC_Type1 Cmd -----------------------------------------------------------------------------------------------------//
uint8_t ISO14443A_NFC_Type1_Read_UID( uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_NFC_Type1_Read_All_Byte   ( uint8_t *UID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_NFC_Type1_Read_Single_Byte( uint8_t Address, uint8_t *UID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_NFC_Type1_Write_With_Erase( uint8_t Address, uint8_t Data, uint8_t *UID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_NFC_Type1_Write_No_Erase( uint8_t Address, uint8_t Data, uint8_t *UID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

// --------------- Combo Cmd ----------------------------------------------------------------------------------------------------------//
uint8_t ISO14443A_Req_Anti_Select( uint8_t Index_Cmd, uint8_t Req_Mode, uint8_t CollMask_Value, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_LoadKey_Authent( uint8_t Index_Cmd, uint8_t Select_Key, uint8_t *Key, uint8_t Block_Num, uint8_t *UID, uint8_t LenUID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Req_Anti_Select_Authent( uint8_t Req_Mode, uint8_t CollMask_Value, uint8_t Select_Key, uint8_t Block_Num, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Req_Anti_Select_LoadKey_Authent( uint8_t Req_Mode, uint8_t CollMask_Value, uint8_t Select_Key, uint8_t *Key, uint8_t Block_Num, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Req_Anti_Select_LoadKey_Authent_Read( uint8_t Req_Mode, uint8_t CollMask_Value, uint8_t Select_Key, uint8_t *Key, uint8_t Block_Num, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Req_Anti_Select_LoadKey_Authent_Write( uint8_t Req_Mode, uint8_t CollMask_Value, uint8_t Select_Key, uint8_t *Key, uint8_t Block_Num, uint8_t *Data_Wr, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Mifare_Combo_Inc_Dec_Res__Transfer( uint8_t Mifare_Cmd, uint8_t Block_Num, uint8_t *Value, uint8_t Transfer_Block, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Decrement_Transfer( uint8_t Block_Num, uint8_t *Decrement_Value, uint8_t Transfer_Block, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Increment_Transfer( uint8_t Block_Num, uint8_t *Increment_Value, uint8_t Transfer_Block, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Restore_Transfer( uint8_t Block_Num, uint8_t *Restore_Value, uint8_t Transfer_Block, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Req_Anti_Select_RATS( uint8_t Req_Mode, uint8_t CollMask_Value, uint8_t Param_Byte, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

uint8_t ISO14443A_EMV_Load_Modulation ( uint8_t Reset_Field_En , uint8_t Run_All_Step , uint8_t CMD_Category ,  uint8_t First_Step,  uint8_t No_Of_Step, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO14443A_Polling_Tags ( uint8_t Polling_Option , uint8_t Reset_Field_En ,uint8_t Run_All_Step , uint8_t CMD_Category , uint8_t First_Step , uint8_t No_Of_Step, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

#endif
