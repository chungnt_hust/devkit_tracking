/*****************************************************************************
* File Name          : ISO15693_Category.c
* Author             : CrystalSu
* Version            : V1.0.0.0
* Date               : Feb 14, 2020
* Editor						 : Chungnt
* Description        : 
*                         - ISO14443B_Transparent_Configure_CRC
*                         - ISO15693_Transparent_With_RxMultiple
******************************************************************************/   

#include "BC45_Board_Define.h"
#include "Common_Utility_Category.h"
#include "ISO15693_Category.h"
#include "BC45_Chip_Function.h"

// Global variable 
tISO15693_Property  ISO15693_Property ;

/*******************************************************************************
* Function Name : ISO15693_Config
* Description   : Configure Registers and RF-Communication speed for ISO15693 Operation
* Input         : 
*				  - Speed : Tx_Speed(4 bits) + RxSpeed(4 bits)	
*						High Nibble : Tx_Speed(Bit7:4)
*							- 0000b : 1 out of 256 (1.65 kbits/s)
*							- 0001b : 1 out of 4 (26.48 kbits/s) 
*							- Other : 1 out of 4 (26.48 kbits/s) 
*						Low Nibble : RxSpeed(Bit3:0)				
*							- 0000b : 1 sub low (6.62 kbits/s)   
*							- 0001b : 1 sub high (26.48 kbits/s)
*							- 0010b : 1 sub ultra high (52.96 kbits/s)
*							- 0011b : 2 sub low (6.67 kbits/s)
*							- 0100b : 2 sub high (26.69 kbits/s)
*							- Other : 1 sub high (26.69 kbits/s)
* Output        : None
* Return        : 
*				  - Response
*				 		- _SUCCESS_ 
*******************************************************************************/
uint8_t ISO15693_Config( uint8_t Speed ) 
{
	BC45_Config_15693() ;
	BC45_Speed_15693( Speed ) ;
	
	// ON Field
	BC45_ON_RF( BC45_BOARD_PROPERTY.Driver_Config_Type ) ;
	HAL_Delay( 10 ) ;
	
	return _SUCCESS_ ;
} 

/*******************************************************************************
* Function Name	: ISO15693_Get_Speed_Reader
* Description 	: Return current RF-Speed setting
* Input         : None
* Output        : 
*				  - Speed : Tx_Speed(4 bits) + RxSpeed(4 bits)	
*						High Nibble : Tx_Speed(Bit7:4)
*							- 0000b : 1 out of 256 (1.65 kbits/s)
*							- 0001b : 1 out of 4 (26.48 kbits/s) 
*							- Other : 1 out of 4 (26.48 kbits/s) 
*						Low Nibble : RxSpeed(Bit3:0)				
*							- 0000b : 1 sub low (6.62 kbits/s)   
*							- 0001b : 1 sub high (26.48 kbits/s)
*							- 0010b : 1 sub ultra high (52.96 kbits/s)
*							- 0011b : 2 sub low (6.67 kbits/s)
*							- 0100b : 2 sub high (26.69 kbits/s)
*							- Other : 1 sub high (26.69 kbits/s)
*				: - LenSpeed : Length of output, always = 1
* Return        : 
*				  - Response
*						- _SUCCESS_ 
*******************************************************************************/
uint8_t ISO15693_Get_Speed_Reader( uint8_t *Speed, uint16_t *LenSpeed )
{	 
	*( Speed )    = ISO15693_Property.Speed ;
	*( LenSpeed ) = 1 ;

	return _SUCCESS_ ;	
}

/*******************************************************************************
* Function Name : Request_Flag_ISO15693
* Description   : Generate flags in resquest format of ISO15693
* Input         : 
*				  - Speed : Tx_Speed(4 bits) + RxSpeed(4 bits)	
*						High Nibble : Tx_Speed(Bit7:4)
*							- 0000b : 1 out of 256 (1.65 kbits/s)
*							- 0001b : 1 out of 4 (26.48 kbits/s) 
*							- Other : 1 out of 4 (26.48 kbits/s) 
*						Low Nibble : RxSpeed(Bit3:0)				
*							- 0000b : 1 sub low (6.62 kbits/s)   
*							- 0001b : 1 sub high (26.48 kbits/s)
*							- 0010b : 1 sub ultra high (52.96 kbits/s)
*							- 0011b : 2 sub low (6.67 kbits/s)
*							- 0100b : 2 sub high (26.69 kbits/s)
*							- Other : 1 sub high (26.69 kbits/s)
*				  - Mode_Value : Inventory_Mode, Non_Inventory_Mode from parameter packet
*				  - Mode 
*						- INVENTORY_1_SLOT_MODE(0x00)
*						- INVENTORY_16_SLOTS_MODE(0x01)
*						- NON_INVENTORY_MODE(0x02)
* Output        : None
* Return        : 
*				   - Request_Flg : Request flag of ISO15693
*******************************************************************************/
uint8_t Request_Flag_ISO15693( uint8_t Speed, uint8_t Mode_Value, uint8_t Mode )
{
	uint8_t  Request_Flg ;
	uint8_t  Speed_Rx ;
	uint8_t  Operation_Mode ;
	uint8_t  AFI_Mode ;
	
	Request_Flg = 0x00 ;
	
	Speed_Rx = ( Speed & 0x0F ) ;
	if ( Speed_Rx == SPEED_1_SUB_LOW ) 	 // 1 sub low
	{	
		;	
	}
	else if	( Speed_Rx == SPEED_1_SUB_ULTRA_HIGH ) // 1 Ultra high	
	{	
		Request_Flg |= DATA_RATE_FLAG_SET_MASK ;	
	}
	else if	( Speed_Rx == SPEED_2_SUB_LOW ) // 2 sub low	
	{	
		Request_Flg |= SUB_CARRIER_FLAG_SET_MASK ;	
	}
	else if	( Speed_Rx == SPEED_2_SUB_HIGH ) // 2 sub high		
	{	
		Request_Flg |= ( DATA_RATE_FLAG_SET_MASK | SUB_CARRIER_FLAG_SET_MASK ) ;	
	}
	else 						// 1 sub high
	{	
		Request_Flg |= DATA_RATE_FLAG_SET_MASK ;	
	};

	if ( Mode == INVENTORY_1_SLOT_MODE ) 
	{
		Request_Flg |= ( INVENTORY_FLAG_SET_MASK | NB_SLOTS_FLAG_SET_MASK )  ;	

		AFI_Mode = ( Mode_Value & AFI_MODE_MASK ) ;
		if ( AFI_Mode == CHECK_AFI_MODE )
		{
			Request_Flg |= AFI_FLAG_SET_MASK  ;	
		}
	}
	else if ( Mode == INVENTORY_16_SLOTS_MODE ) 
	{
		Request_Flg |= INVENTORY_FLAG_SET_MASK  ;	

		AFI_Mode = ( Mode_Value & AFI_MODE_MASK ) ;
		if ( AFI_Mode == CHECK_AFI_MODE )
		{
			Request_Flg |= AFI_FLAG_SET_MASK  ;	
		}
	}
	else // Non-Inventory mode
	{
		Operation_Mode = ( Mode_Value & OPERATION_MODE_MASK ) ;
		if ( Operation_Mode == ADDR_MODE )
		{
			Request_Flg |= ADDR_FLAG_SET_MASK  ;	
		}
		else if ( Operation_Mode == SEL_MODE )
		{
			Request_Flg |= SEL_FLAG_SET_MASK  ;	
		}
	};

	if ( (Mode_Value & RFU_FLAG_IN_MODE_MASK) == RFU_FLAG_IN_MODE_MASK )
	{	
		Request_Flg |= RFU_FLAG_SET_MASK  ;	
	}

	if ( (Mode_Value & OPTION_FLAG_IN_MODE_MASK) == OPTION_FLAG_IN_MODE_MASK )
	{	
		Request_Flg |= OPTION_FLAG_SET_MASK  ;	
	}

	if ( (Mode_Value & PROTOCOL_EX_FLAG_IN_MODE_MASK) == PROTOCOL_EX_FLAG_IN_MODE_MASK )
	{	
		Request_Flg |= PROTOCOL_EX_FLAG_SET_MASK  ;	
	}

	return Request_Flg ;
}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//



//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
//--------------------------ISO15693 Standard Cmd --------------------------------------------------------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

/*******************************************************************************
* Function Name	: ISO15693_Inv_Req_1_Slot
* Description 	: Perform Inventory command in ISO15693 standard
*				: Require correct speed setting before runing this function by call function BC45_Speed_15693
* Input         : 
*				  - Speed : Tx_Speed(4 bits) + RxSpeed(4 bits) (This parameter is also the same value for function BC45_Speed_15693) 
*						High Nibble : Tx_Speed(Bit7:4)
*							- 0000b : 1 out of 256 (1.65 kbits/s)
*							- 0001b : 1 out of 4 (26.48 kbits/s) 
*							- Other : 1 out of 4 (26.48 kbits/s) 
*						Low Nibble : RxSpeed(Bit3:0)				
*							- 0000b : 1 sub low (6.62 kbits/s)   
*							- 0001b : 1 sub high (26.48 kbits/s)
*							- 0010b : 1 sub ultra high (52.96 kbits/s)
*							- 0011b : 2 sub low (6.67 kbits/s)
*							- 0100b : 2 sub high (26.69 kbits/s)
*							- Other : 1 sub high (26.69 kbits/s)
*				  - Inv_Mode : RFU_Flg(1 bit) + Option Flg(1 bit) + Protocol Extension Flg(1 bit) + '0' + AFI mode(4 bits)
*						RFU_Flag : Bit in request flag of ISO15693 (Bit7)
*							- 0b = Clear RFU_Flag bit
*							- 1b = Set RFU_Flag bit
*						Option_Flag : Bit in request flag of ISO15693 (Bit6)
*							- 0b = Clear Option_Flag bit
*							- 1b = Set Option_Flag bit
*						Protocol_Extension_Flag : Bit in request flag of ISO15693 (Bit5)
*							- 0b = Clear Protocol_Extension_Flag bit
*							- 1b = Set Protocol_Extension_Flag bit
*						Low Nibble : AFI mode (Bit3:0)
*							- 0000b : Non Check AFI
*							- 0001b : Check AFI  
*				  - AFI : Application family identifier 
*				  - Mask_Len : The mask length indicates the number of significant bits.
*				  - Mask_Value : The mask value is contained in an integer number of bytes
*					Note : If the mask length is not a multiple of 8 (bits), 
*						   the mask value MSB shall be padded with the required number of null (set to 0) bits 
*						   so that the mask value is contained in an integer number of bytes.
* Output		: 
*				  - Data_Resp : Response data
*						- if Response = _SUCCESS_
*							- 1st to Last byte : Response data from Inventory command
*						- if Response = FRAMING_ERR or COLLISION_ERR or PARITY_ERR or CRC_ERR or INVALID_RESP (RF_COMMU_ERR_CATEGORY)
*							- 1st to Last byte : Error data in BC45 FIFO (if it exists)
*						- Other (NO_RESPONSE, ASIC_EXE_TIMEOUT)
*							- No Response data  
*				  - LenData_Resp : Length of Response data
*					- Tick: state of the inv req
* Return        : 
*				  - Response
*						- _SUCCESS_  
*						- NO_RESPONSE
*						- FRAMING_ERR
*						- COLLISION_ERR
*						- PARITY_ERR
*						- CRC_ERR
*						- INVALID_RESP
*						- ASIC_EXE_TIMEOUT  
*******************************************************************************/
uint8_t ISO15693_Inv_Req_1_Slot(uint8_t Speed, uint8_t Inv_Mode, uint8_t AFI, uint8_t Mask_Len, uint8_t *Mask_Value, uint8_t *Data_Resp, uint16_t *LenData_Resp, uint16_t *Tick)
{
	uint16_t  i ;
	uint8_t   Resp = 0;
	uint8_t   Mask_Byte, Mask_Value_Pos ;

	if(*Tick == BC45_TICK_INV_REQ_EN)
	{
		//DEBUG("BC45_TICK_TXRX_EN\r\n");
		*Tick = BC45_TICK_TXRX_EN;
		Mask_Byte = 0;

		// Request Flag
		Data_TxRF[0] = Request_Flag_ISO15693( Speed, Inv_Mode, INVENTORY_1_SLOT_MODE ) ;

		Data_TxRF[1] = ISO15693_INVENTORY_RF_CMD ;		//Inventory Command
		if ( ( Inv_Mode & AFI_MODE_MASK ) == NON_CHECK_AFI_MODE )
		{
			Data_TxRF[2] = Mask_Len ;	//Mask Length
		
			Mask_Value_Pos = 3 ;
			LenData_TxRF =  3 ;
		}
		else
		{
			Data_TxRF[2] = AFI ; 			//AFI
			Data_TxRF[3] = Mask_Len ;	//Mask Length
		
			Mask_Value_Pos = 4 ;
			LenData_TxRF = 4 ;
		}

		Mask_Byte = Mask_Len / 8 ;
		if ( (Mask_Len % 8) != 0 )
		{	
			Mask_Byte = Mask_Byte + 1 ;	
		}

		for( i = 0 ; i < Mask_Byte ; i++ ) 
		{	
			Data_TxRF[Mask_Value_Pos + i] = *( Mask_Value + i ) ;		//Mask Length	
		}

		LenData_TxRF += Mask_Byte ;
		
		BC45_CRC_Setting( TxCRC_Enable, RxCRC_Enable ) ;
	}

	Resp = BC45_Transceive( &Data_TxRF[0], LenData_TxRF, &Data_RxRF[0], &LenData_RxRF, Tick ) ;	

	if ( (Resp == _SUCCESS_) && (*Tick != BC45_TICK_TIMEOUT) )
	{
		if ( LenData_RxRF == LEN_ISO15693_INVENTORY_RF_RESP )
		{
			*(LenData_Resp) = LenData_RxRF - 1 ;	
			for( i = 0 ; i < LenData_RxRF - 1 ; i++ )
			{
				*( Data_Resp + i ) = Data_RxRF[i + 1] ;		
			}
		}
		else
		{
			Resp = INVALID_RESP ;
			*(LenData_Resp) = LenData_RxRF ;	
			for( i = 0 ; i < LenData_RxRF ; i++ )
			{
				*( Data_Resp + i ) = Data_RxRF[i] ;		
			}
		}
	}
	else
	{
		*(LenData_Resp) = LenData_RxRF ;	
		for( i = 0 ; i < LenData_RxRF ; i++ )
		{
			*( Data_Resp + i ) = Data_RxRF[i] ;		
		}
	}	
	return Resp ;
}

