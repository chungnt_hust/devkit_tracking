/**
  ******************************************************************************
  * @file    Common_Utility_Category.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  *
**/

#ifndef COMMON_UTILITY_CATEGORY_H 
#define COMMON_UTILITY_CATEGORY_H 

#include <stdint.h>


//-------------------------------------------------------------------------//
//----------------------- Define FBP Response -----------------------------//
//-------------------------------------------------------------------------//

#define	_SUCCESS_					0x01 // Operation success

// Communication Protocol Err
#define UNKNOWN_CMD_TYPE			0x20 // Input command category is undefined
#define UNKNOWN_CMD				0x21 // Input command is undefined
#define PARAMETER_NOT_CORRECT		        0x22 // Parameter is incomplete or invalid

uint16_t UpdateCRC( uint8_t data, uint16_t *lpwCRC ) ;
uint16_t Reverse_Data_16Bit( uint16_t Data ) ;


#endif
