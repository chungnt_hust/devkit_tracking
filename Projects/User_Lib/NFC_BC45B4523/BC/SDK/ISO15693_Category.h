/*****************************************************************************
* File Name          : ISO15693_Category.h
* Author             : Crystal Su
* Version            : V1.0.0.0
* Date               : March 25, 2020
* Description        : 
******************************************************************************/      
#ifndef ISO15693_CATEGORY_H 
#define ISO15693_CATEGORY_H

#include <stdint.h>

#define ISO15693_LEN_UID_BYTE	8

// Speed Rx
#define RX_1_SUB_LOW			0x00
#define RX_1_SUB_HIGH			0x01
#define RX_1_SUB_ULTRA_HIGH		0x02
#define RX_2_SUB_LOW			0x03
#define RX_2_SUB_HIGH			0x04

// Inventory Mode
#define INVENTORY_MODE			0x01
#define INVENTORY_1_SLOT_MODE	0x01
#define INVENTORY_16_SLOTS_MODE	0x02

#define NON_CHECK_AFI_MODE		0x00
#define CHECK_AFI_MODE			0x01

// Non-Inventory Mode
#define NON_INVENTORY_MODE		0x00
#define NON_ADDR_MODE			0x00
#define ADDR_MODE				0x01
#define SEL_MODE				0x02

#define RFU_FLAG_IN_MODE_MASK			0x80
#define OPTION_FLAG_IN_MODE_MASK		0x40
#define PROTOCOL_EX_FLAG_IN_MODE_MASK	0x20

// Request Flag Mask
#define SUB_CARRIER_FLAG_SET_MASK	0x01
#define DATA_RATE_FLAG_SET_MASK		0x02
#define INVENTORY_FLAG_SET_MASK		0x04
#define PROTOCOL_EX_FLAG_SET_MASK	0x08

#define SEL_FLAG_SET_MASK			0x10
#define AFI_FLAG_SET_MASK			0x10
#define ADDR_FLAG_SET_MASK			0x20
#define NB_SLOTS_FLAG_SET_MASK		0x20
#define OPTION_FLAG_SET_MASK		0x40
#define RFU_FLAG_SET_MASK			0x80

// Response Flag Mask
#define ERR_FLAG_MASK				0x01

#define SM_MASK						0x03
#define PA_MASK						0x0C
#define M_MASK						0x10

#define PM_PWD0_PROTECT_RD_PWD1_PROTECT_WR	0x00
#define PM_USE_PWD0_PROTECT_ALL				0x01
#define PM_USE_PWD1_PROTECT_ALL				0x02
#define PM_USE_PWD_64_PROTECT_ALL			0x03

#define SM_NORMAL					0x00
#define SM_PROTECT_READ				0x01
#define SM_PROTECT_WRITE			0x02
#define SM_PROTECT_READ_WRITE		0x03

#define M_USE_PWD0					0x00
#define M_USE_PWD1					0x01

#define IC_MFG_HTK					0x39
#define IC_MFG_PHILIP				0x04

#define UID_NOT_OPTIONAL			0x00
#define UID_OPTIONAL				0x01

#define AFI_MODE_MASK				0x0F
#define OPERATION_MODE_MASK			0x0F

#define ISO15693_INVENTORY_RF_CMD				0x01
#define ISO15693_STAY_QUIET_RF_CMD				0x02
#define ISO15693_READ_SINGLE_BLOCK_RF_CMD		0x20
#define ISO15693_WRITE_SINGLE_BLOCK_RF_CMD		0x21
#define ISO15693_LOCK_BLOCK_RF_CMD				0x22
#define ISO15693_READ_MULTIPLE_BLOCK_RF_CMD		0x23
#define ISO15693_WRITE_MULTIPLE_BLOCK_RF_CMD	0x24
#define ISO15693_SELECT_RF_CMD					0x25
#define ISO15693_RESET_TO_READY_RF_CMD			0x26
#define ISO15693_WRITE_AFI_RF_CMD				0x27
#define ISO15693_LOCK_AFI_RF_CMD				0x28
#define ISO15693_WRITE_DSFID_RF_CMD				0x29
#define ISO15693_LOCK_DSFID_RF_CMD				0x2A
#define ISO15693_GET_SYSTEM_INFO_RF_CMD			0x2B
#define ISO15693_GET_MULTI_BLOCK_SECURE_RF_CMD	0x2C

#define ISO15693_SET_EAS_HTK_RF_CMD				0xA2
#define ISO15693_RESET_EAS_HTK_RF_CMD			0xA3
#define ISO15693_LOCK_EAS_HTK_RF_CMD			0xA4
#define ISO15693_EAS_ALARM_HTK_RF_CMD			0xA5
#define ISO15693_KILL_HTK_RF_CMD				0xA6
#define ISO15693_SET_PASSWORD_HTK_RF_CMD		0xB3
#define ISO15693_WRITE_PASSWORD_HTK_RF_CMD		0xB4
#define ISO15693_LOCK_PASSWORD_HTK_RF_CMD		0xB5
#define ISO15693_SET_PASSWORD_MODE_HTK_RF_CMD	0xB6
#define ISO15693_LOCK_PASSWORD_MODE_HTK_RF_CMD	0xB7
#define ISO15693_GET_PASSWORD_MODE_HTK_RF_CMD	0xB8
#define ISO15693_SET_OTP_HTK_RF_CMD				0xC0
#define ISO15693_GET_OTP_HTK_RF_CMD				0xC1
#define ISO15693_WRITE_OTP_UID_HTK_RF_CMD		0xC2
#define ISO15693_READ_OTP_UID_HTK_RF_CMD		0xC3

// Length of response data for each command
#define LEN_ISO15693_INVENTORY_RF_RESP			10

//-------------------------------------------------------------------------//
//--------------------------- ISO15693 Command ----------------------------//
//-------------------------------------------------------------------------//

// Config Reader Cmd
#define D_Config_ISO15693			0x00
#define D_Config_Speed_Reader		0x01
#define D_Get_Speed_Reader			0x02

// Standard Cmd
#define D_Inventory_1_Slot			0x10
#define D_Inventory_16_Slots		0x11
#define D_Stay_Quiet				0x12
#define D_Read_Single_Block			0x13
#define D_Write_Single_Block		0x14
#define D_Lock_Block				0x15
#define D_Read_Multiple_Blocks		0x16
#define D_Write_Multiple_Blocks		0x17
#define D_Select					0x18
#define D_Reset_to_Ready			0x19
#define D_Write_AFI					0x1A
#define D_Lock_AFI					0x1B
#define D_Write_DSFID				0x1C
#define D_Lock_DSFID				0x1D
#define D_Get_System_Information	0x1E
#define D_Get_multiple_block_security_status	0x1F

// SIC5600 Custom Cmd
#define D_Set_EAS_HTK				0x30
#define D_Reset_EAS_HTK				0x31
#define D_Lock_EAS_HTK				0x32
#define D_EAS_Alarm_HTK				0x33
#define D_Kill_HTK					0x34
#define D_Set_Password_HTK			0x35
#define D_Write_Password_HTK		0x36
#define D_Lock_Password_HTK			0x37
#define D_Set_Password_Mode_HTK		0x38
#define D_Lock_Password_Mode_HTK	0x39
#define D_Get_Password_Mode_HTK		0x3A
#define D_Set_OTP_HTK				0x3C
#define D_Get_OTP_HTK				0x3D
#define D_Write_OTP_UID_HTK			0x3E
#define D_Read_OTP_UID_HTK			0x3F

// Special Cmd
#define D_Transparent_With_CRC		0xC0
#define D_Transparent_Without_CRC	0xC1
#define D_Send_1_Pulse			0xC2
#define D_Transparent_Config_CRC	0xC3
#define D_Transparent_With_RxMultiple	    0xC4
		
//-------------------------------------------------------------------------//
//-------------------------------------------------------------------------//

typedef struct Struct_tISO15693_Property
{

	uint8_t  Speed ;	
	
} tISO15693_Property ;

uint8_t ISO15693_Command( uint8_t Command, uint8_t *Param, uint16_t LenParam, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Check_And_Separate_Param_Inventory_Mode( uint8_t *Param_Package, uint16_t LenParam_Package, uint8_t *Inventory_Mode, uint8_t *AFI , uint8_t *Mask_Len, uint8_t *Mask_Value ) ;
uint8_t ISO15693_Check_And_Separate_Param_Non_Inventory_Mode( uint8_t *Param_Package, uint16_t LenParam_Package, uint8_t *Non_Inventory_Mode, uint8_t *UID_Frm_Param , uint8_t *Other_Param, uint16_t *Len_Other_Param ) ;
uint8_t ISO15693_Check_And_Separate_Param_Non_Inventory_Mode_UID_Is_Optional( uint8_t *Param_Package, uint16_t LenParam_Package, uint8_t *Non_Inventory_Mode, uint8_t *UID_Frm_Param , uint8_t *Other_Param, uint16_t *Len_Other_Param ) ;
uint8_t ISO15693_Check_Response( uint8_t Resp_RxRF, uint8_t *Data_RxRF, uint16_t LenData_RxRF, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

uint8_t ISO15693_Config ( uint8_t Speed ) ; 
uint8_t ISO15693_Get_Speed_Reader( uint8_t *Speed, uint16_t *LenSpeed ) ;

uint8_t Request_Flag_ISO15693( uint8_t Speed, uint8_t Mode_Value, uint8_t Mode ) ;
uint8_t ISO15693_Inv_Req_1_Slot( uint8_t Speed, uint8_t Inv_Mode, uint8_t AFI, uint8_t Mask_Len, uint8_t *Mask_Value, uint8_t *Data_Resp, uint16_t *LenData_Resp, uint16_t *Tick) ;
uint8_t ISO15693_Inv_Req_16_Slots( uint8_t Speed, uint8_t Inv_Mode, uint8_t AFI, uint8_t Mask_Len, uint8_t *Mask_Value, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

uint8_t ISO15693_Reset_to_Ready( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Stay_Quiet( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Select( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

// Standard Cmd
uint8_t ISO15693_Send_Data__NO_IC_MFG( uint8_t UID_MODE, uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t Cmd_ISO15693, uint8_t *Param_ISO15693, uint16_t LenParam_ISO15693, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Read_Single_Block( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t Block_Num, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Write_Single_Block( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t Block_Size, uint8_t *Write_Block_Param, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Lock_Block( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t Block_Num, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Read_Multiple_Blocks( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t *Read_Multi_Block_Param, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Write_Multiple_Blocks( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t Block_Size, uint8_t *Write_Multi_Block_Param, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Write_AFI( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t AFI_Value, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Lock_AFI( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Write_DSFID( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t DSFID_Value, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Lock_DSFID( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Get_System_Information( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t ISO15693_Get_Multiple_Block_Security_Status( uint8_t Speed, uint8_t Non_Inv_Mode, uint8_t *UID, uint8_t *Get_Multi_Block_Secure_Param, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

uint8_t ISO15693_Send_1_Pulse( uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;

#endif
