/*****************************************************************************
* File Name          : DES.c
* Author             : CrystalSu
* Version            : V1.0.0.0
* Date               : March 25, 2020
* Description        : Add on file for DES algorithm
******************************************************************************/                       

#include "Common_Utility_Category.h"
#include "DES.h"


//  variable 
uint8_t   DES_key[16][48] ;
uint8_t   DES_key1[8] ;
uint8_t   DES_key2[8] ;
uint8_t   DES_key3[8] ;

uint8_t IP1[]={58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4,    //initial change
           62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8,
           57, 49, 41, 33, 25, 17, 9,  1, 59, 51, 43, 35, 27, 19, 11, 3,
           61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7
 };
uint8_t IP2[]={40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63, 31,    //opp initial change
           38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29,
           36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27,
           34, 2, 42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25
 };
uint8_t s[][4][16]={{                                                                              //S-diagram array
                 {14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7},
                 {0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8},
                 {4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0},
                 {15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13}
                 },
                {
                 {15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10},
                 {3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5},
                 {0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15},
                 {13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9}
                },
                {
                 {10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8},
                 {13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1},
                 {13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7},
                 {1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12}
                },
                {
                 {7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15},
                 {13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9},
                 {10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4},
                 {3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14}
                },
                {
                 {2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9},
                 {14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6},
                 {4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14},
                 {11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3}
                },
                {
                 {12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11},
                 {10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8},
                 {9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6},
                 {4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13}
                },
                {
                 {4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1},
                 {13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6},
                 {1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2},
                 {6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12}
                },
                {
                 {13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7},
                 {1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2},
                 {7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8},
                 {2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11}
                }
};
uint8_t Ex[48]={  32,1,2,3,4,5,                                                               //Expand array
              4,5,6,7,8,9,
              8,9,10,11,12,13,
              12,13,14,15,16,17,
              16,17,18,19,20,21,
              20,21,22,23,24,25,
              24,25,26,27,28,29,
              28,29,30,31,32,1
 };
uint8_t P[32]={16,7,20,21,                                                                //P-change
           29,12,28,17,
           1,15,23,26,
           5,18,31,10,
           2,8,24,14,
           32,27,3,9,
           19,13,30,6,
           22,11,4,25
 };
uint8_t PC1[56]={57,49,41,33,25,17,9,                                                //PC-1 in keyBuild
             1,58,50,42,34,26,18,
             10,2,59,51,43,35,27,
             19,11,3,60,52,44,36,
             63,55,47,39,31,23,15,
             7,62,54,46,38,30,22,
             14,6,61,53,45,37,29,
             21,13,5,28,20,12,4
 };
uint8_t PC2[48]={14,17,11,24,1,5,                                                     //PC-2 in keyBuild
             3,28,15,6,21,10,
             23,19,12,4,26,8,
             16,7,27,20,13,2,
             41,52,31,37,47,55,
             30,40,51,45,33,48,
             44,49,39,56,34,53,
             46,42,50,36,29,32
 };
 


/*******************************************************************************
* Function Name	: DES_Load_Key
* Description 	: Perform Load Key 
* Input         : 
*				  - Key : 6 Bytes 
* Output        : None
* Return        : 
*				  - Response
*						- _SUCCESS_  
*						- KEY_ERR  
*						- ASIC_EXE_TIMEOUT  
*******************************************************************************/
uint8_t DES_Load_Key (uint8_t *Key_3DES)
{
        uint8_t   i ;
        
        for (i = 0; i < 8; i++)
        { 
          DES_key1[i] =   *(Key_3DES + i);
          DES_key2[i] =   *(Key_3DES + i + 8);
          DES_key3[i] =   *(Key_3DES + i + 16);
        }
        
        return _SUCCESS_;
}



/*******************************************************************************
* Function Name	: DES_Deciphered
* Description 	: Perform 3DES_Deciphered command
* Input         : 16 bytes data, the first 8 bytes from the respense of ISO14443A_3DES_Authent CMD and the second 8 bytes SV(start value) first time :all is 0
* Output        : 
*				  - Data_Resp : Response data
*						Left relotate 8 bits the  deciphered data
*				  - LenData_Resp : Length of Response data
* Return        : 
*				  - Response
*						- _SUCCESS_  
*******************************************************************************/
uint8_t DES_Deciphered (uint8_t *Data_Sv ,uint8_t *Data_Resp, uint16_t *LenData_Resp)	
{	
	uint8_t  i ;
        uint8_t  Data[8] ;
        uint8_t  Sv[8] ;

        for ( i = 0 ; i < 8 ; i++ )	
        {
	    Data[ i ] = *( Data_Sv + i ) ; 	
            Sv  [ i ] = *( Data_Sv + i + 8 );
	}
        
        Decode ( Data , DES_key3 );
        Encode ( Data , DES_key2 ); 
        Decode ( Data , DES_key1 ); 
        
        for( i = 0 ; i < 8 ; i++ )
	{
	    *( Data_Resp + i ) = Data[i] ^ Sv[i] ;		
	}
        
        *( LenData_Resp ) = 8 ;
        
        return 	_SUCCESS_;
}



/*******************************************************************************
* Function Name	: DES_Enciphered
* Description 	: Perform 3DES_Enciphered command
* Input         : 16 bytes data, the first 8 bytes from the respense of ISO14443A_3DES_Authent CMD and the second 8 bytes SV(start value) 
* Output        : 
*				  - Data_Resp : Response data
*						Left relotate 8 bits the  deciphered data
*				  - LenData_Resp : Length of Response data
* Return        : 
*				  - Response
*						- _SUCCESS_  
*******************************************************************************/
uint8_t DES_Enciphered(uint8_t *Data_Sv,uint8_t *Data_Resp, uint16_t *LenData_Resp)// Data_Sv 16 Bytes	
{	
	uint8_t  i ;
        uint8_t   Data[8] ;
        uint8_t   Sv[8] ;
        
        for ( i = 0 ; i < 8 ; i++ )	
        {
	    Data[ i ] = *( Data_Sv + i ) ; 
            Sv  [ i ] = *( Data_Sv + i + 8 );
            Data[ i ] = Data[ i ] ^ Sv[i] ;	
	}
        
        Encode( Data , DES_key1 );
        Decode( Data , DES_key2 ); 
        Encode( Data , DES_key3 ); 
        
	for( i = 0 ; i < 8 ; i++ )
	{
	    *( Data_Resp + i ) = Data[i];		
	}
         
        *( LenData_Resp ) = 8 ;
        return 	_SUCCESS_;
}



/*******************************************************************************
* Function Name	: Encode
* Description 	: Perform DES_Eeciphered 
* Input         : 8 bytes Data and  8 bytes key
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/

void Encode(uint8_t *str ,uint8_t *keychar)          //encode: input 8 chars,8 keychars
{          
       uint8_t lData[32],rData[32],temp[32];
       uint8_t i,j;
       
       keyBuild(keychar);
       EncodeData(lData , rData , str);
       for( i = 0 ; i < 16 ; i++ )
       {
          for( j = 0 ; j < 32 ; j++ )
          {
            temp[j] = rData[j];
          }
          F_Function (rData,DES_key[i]);
          for( j = 0 ; j < 32 ; j++)
          {
            rData[j] = rData[j] ^ lData[j];
          }
    
          for( j = 0 ; j < 32 ; j++ )
          {
            lData[j] = temp[j];
          }
       }

       DecodeData( str , rData , lData);

}

/*******************************************************************************
* Function Name	: Decode
* Description 	: Perform DES_Deciphered 
* Input         : 8 bytes Data and  8 bytes key
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/
void Decode(uint8_t *str,uint8_t *keychar)                //decode :input 8 chars,8 keychars
{           
       uint8_t lData[32],rData[32],temp[32];
       uint8_t i,j;
       
       keyBuild(keychar);
       EncodeData(lData,rData,str); 
       for(i=0 ; i<16 ; i++)
       {
           for(j=0 ; j<32 ; j++)
           {
              temp[j] = rData[j];
           }
           F_Function(rData,DES_key[15-i]);
           for(j=0 ; j<32 ; j++)
           {
              rData[j]  = rData[j] ^ lData[j];
           }
           for(j=0 ; j<32 ; j++)
           {
              lData[j]  = temp[j];
           }
       }
       DecodeData( str , rData , lData);
}

/*******************************************************************************
* Function Name	: keyBuild
* Description 	: Perform key function
* Input         : 8 byte key
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/
void keyBuild(uint8_t *keychar)             //create key array
{            
        uint8_t i;
        uint8_t movebit[]={1,1,2,2,2,2,2,2,
                       1,2,2,2,2,2,2,1};
        uint8_t midkey2[56];
        uint8_t midkey[64];
        
        StrtoBin(midkey,keychar);
        for(i=0 ; i<56 ; i++)
        {
          midkey2[i]  = midkey[PC1[i]-1];
        }
        for(i=0;i<16;i++)
        {
          keyCreate(midkey2,movebit[i],i);
        }
}

/*******************************************************************************
* Function Name	: StrtoBin
* Description 	: change into binary
* Input         : 
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/
void StrtoBin(uint8_t *midkey,uint8_t *keychar)       //change into binary
{     
        int trans[8],i,j,k,n;
        int temp[64];
        
        n = 0;
        for (i=0 ; i<64 ; i++)
        {
          temp[i] = keychar[i];
        }
  
        for(i=0 ; i<8 ; i++)
        {
           j=0;
           while(temp[i]!=0)
           {
              trans[j]=temp[i]%2;
              temp[i]=temp[i]/2;
              j++;
           }    
           for(k=j ; k<8 ; k++)
           {
             trans[k] = 0;
           }
           for(k=0 ; k<8 ; k++)
           {                   
             midkey[n++] = trans[7-k];	   
           }
        }  
}

/*******************************************************************************
* Function Name	: keyCreate
* Description 	: midkey2 relotate create key1,key2,...key16
* Input         : midkey, relatate bit number
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/

void keyCreate(uint8_t *midkey2,uint8_t movebit,uint8_t n)
{
        uint8_t i;
        uint8_t temp[4];
        
        temp[0]=midkey2[0];
        temp[1]=midkey2[1];
        temp[2]=midkey2[28];
        temp[3]=midkey2[29];
        if(movebit==2)
        {
          for(i=0 ; i<26 ; i++)
          {
            midkey2[i]=midkey2[i+2];
            midkey2[i+28]=midkey2[i+30];
          }
          midkey2[26]=temp[0];
          midkey2[27]=temp[1];
          midkey2[54]=temp[2];
          midkey2[55]=temp[3]; 
       }
       else
       {
          for(i=0 ; i<27 ; i++)
          {
            midkey2[i]    = midkey2[i+1];
            midkey2[i+28] = midkey2[i+29];
          }
          midkey2[27]   = temp[0];
          midkey2[55]   = temp[2];
       }
       
       for(i=0 ; i<48 ; i++)
       {
         DES_key[n][i]  = midkey2[PC2[i]-1];
       }
	  
}	

/*******************************************************************************
* Function Name	: EncodeData
* Description 	:  
* Input         : lData, rData
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/

void EncodeData(uint8_t *lData,uint8_t *rData,uint8_t *str)           //encodedata function
{   
        uint8_t i,j;
        uint8_t temp[8],lint,rint;
        uint8_t data[64];
        
        lint=0,rint=0;
        for(i=0;i<4;i++)
              {
                     j=0;
                     while(str[i]!=0)
                           {
                               temp[j]=str[i]%2;
                               str[i]=str[i]/2;
                               j++;
                           }
                     while(j<8)temp[j++]=0;
                            for(j=0;j<8;j++)
                                lData[lint++]=temp[7-j];
                                j=0;
                     while(str[i+4]!=0)
                           {
                                temp[j]=str[i+4]%2;
                                str[i+4]=str[i+4]/2;
                                j++;
                           }
                     while(j<8)temp[j++]=0;
                            for(j=0;j<8;j++)rData[rint++]=temp[7-j];
              }
        for(i=0;i<32;i++)
              {
                     data[i]=lData[i];
                     data[i+32]=rData[i];
              }
    
        for(i=0;i<32;i++)
              {
                     lData[i]=data[IP1[i]-1];
                     rData[i]=data[IP1[i+32]-1];
              }
       
}


/*******************************************************************************
* Function Name	: DecodeData
* Description 	: 
* Input         : 
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/
void DecodeData(uint8_t *str,uint8_t *lData,uint8_t *rData)         //DecodeData from binary
{   
      uint8_t i;
      uint8_t a,b;
      uint8_t data[64];
     
      a=0,b=0;
      for(i=0;i<32;i++)
          {
              data[i]=lData[i];
              data[i+32]=rData[i];
          }
      for(i=0;i<32;i++)
          {
              lData[i]=data[IP2[i]-1];
              rData[i]=data[IP2[i+32]-1];
          }
      for(i=0;i<32;i++)
          {
              a=(lData[i]&0x1)+(a<<1);
              b=(rData[i]&0x1)+(b<<1);
              if((i+1)%8==0)
                  {
                      str[i/8]=a;a=0;
                      str[i/8+4]=b;b=0;
                  }
          }
}


/*******************************************************************************
* Function Name	: F_Function
* Description 	: perform F function
* Input         : 
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/
void F_Function (uint8_t *rData,uint8_t *key)             //F function
{                   
        uint8_t i;
        uint8_t rDataP[48];
        
        Expand(rData,rDataP);
        for( i=0 ; i<48 ; i++)
        {
          rDataP[i] = rDataP[i] ^ key[i];
        }
        ExchangeS(rDataP,rData);
        ExchangeP(rData);  
}


/*******************************************************************************
* Function Name	: Expand
* Description 	: Expand data to 48 bits
* Input         : 
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/
void Expand(uint8_t *rData,uint8_t *rDataP)             //Expand function
{         
      uint8_t i;
      
      for(i=0 ; i<48 ; i++)
      {
         rDataP[i] = rData [Ex[i] - 1];
      }
}

/*******************************************************************************
* Function Name	: ExchangeS
* Description 	: S-diagram change
* Input         : 
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/
void ExchangeS(uint8_t *rDataP,uint8_t *rData)          //S-diagram change
{          
      uint8_t i,n;
      uint8_t linex,liney;
      
      linex=0;
      liney=0;
      
      for(i=0;i<48;i+=6)
      {
        n=i/6; 
        linex = (rDataP[i]<<1)    + rDataP[i+5];
        liney = (rDataP[i+1]<<3)  + (rDataP[i+2]<<2)  + (rDataP[i+3]<<1)  + rDataP[i+4];    
        FillBin(rData,n,s[n][linex][liney]);
      }
   
}

/*******************************************************************************
* Function Name	: ExchangeS
* Description 	: S-diagram change
* Input         : 
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/
void ExchangeP(uint8_t *rData)    //P change
{                     
      uint8_t i;
      uint8_t temp[32];
      
      for(i=0 ; i<32 ; i++)
      {
        temp[i] = rData[i];
      }
  
      for(i=0 ; i<32 ; i++)
      {
         rData[i] = temp[P[i]-1];
      }
}
/*******************************************************************************
* Function Name	: FillBin
* Description 	: data to binary;call by S-Diagram change function
* Input         : 
* Output        : 
*				  - Data_Resp : 8 bytes enciphered  data
*						
* Return        : 
*				  
*******************************************************************************/
void FillBin(uint8_t *rData,uint8_t n,uint8_t s)    // data to binary;call by S-Diagram change function
{         
      uint8_t i;
      uint8_t temp[4];
      
      for(i=0 ; i<4 ; i++)
      {
        temp[i] = s%2;
        s       = s/2;
      }
      for(i=0 ; i<4 ; i++)
      {
        rData[n*4+i]  = temp[3-i];
      }
      
}
