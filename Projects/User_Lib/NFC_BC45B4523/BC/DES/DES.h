/*****************************************************************************
* File Name          : DES.h
* Author             : Ohmmarin Sathusen
* Version            : V1.5.0.0
* Date               : 18/09/2012
* Description        : 
******************************************************************************/        

#ifndef __DES_H__
#define __DES_H__

#include <stdint.h>

uint8_t DES_Load_Key (uint8_t *Key_3DES) ;
uint8_t DES_Deciphered(uint8_t *Data_Sv,uint8_t *Data_Resp, uint16_t *LenData_Resp);
uint8_t DES_Enciphered(uint8_t *Data_Sv,uint8_t *Data_Resp, uint16_t *LenData_Resp);
void Encode(uint8_t *str,uint8_t *keychar)  ;
void Decode(uint8_t *str,uint8_t *keychar)  ;
void keyBuild(uint8_t *keychar)   ;
void StrtoBin(uint8_t *midkey,uint8_t *keychar) ;
void keyCreate(uint8_t *midkey2,uint8_t movebit,uint8_t n);
void EncodeData(uint8_t *lData,uint8_t *rData,uint8_t *str);
void DecodeData(uint8_t *str,uint8_t *lData,uint8_t *rData) ;
void F_Function (uint8_t *rData,uint8_t *key)  ;
void Expand(uint8_t *rData,uint8_t *rDataP)  ;
void ExchangeS(uint8_t *rDataP,uint8_t *rData) ;
void ExchangeP(uint8_t *rData) ;
void FillBin(uint8_t *rData,uint8_t n,uint8_t s);



#endif



 
