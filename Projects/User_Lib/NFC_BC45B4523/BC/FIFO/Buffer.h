/*****************************************************************************
* File Name          : Buffer.h
* Author             : 
* Version            : V1.4.0.0
* Date               : 31/01/2011
* Description        : 
******************************************************************************/   

#ifndef BUFFER_H
#define BUFFER_H

#include <stdint.h>

// the cBuffer structure
typedef struct struct_tBuffer
{
	uint8_t *dataptr ;				//!< the physical memory address where the buffer is stored
	uint16_t size ;					//!< the allocated size of the buffer
	volatile uint16_t datalength ;  //!< the length of the data currently in the buffer
	uint16_t dataindex ;			//!< the index into the buffer where the data starts
} tBuffer ;
	  

// ---------------------------------------------------------------------------

//! initialize a buffer to start at a given address and have given size
void bufferInit( tBuffer* buffer, uint8_t *start, uint16_t size ) ;

// ---------------------------------------------------------------------------

//! get the first byte from the front of the buffer
uint8_t	bufferGetFromFront(tBuffer* buffer) ;

// ---------------------------------------------------------------------------

//! dump (discard) the first numbytes from the front of the buffer
void bufferDumpFromFront(tBuffer* buffer, uint16_t numbytes) ;

// ---------------------------------------------------------------------------

//! get a byte at the specified index in the buffer (kind of like array access)
// ** note: this does not remove the byte that was read from the buffer
uint8_t	bufferGetAtIndex(tBuffer* buffer, uint16_t index) ;

// ---------------------------------------------------------------------------

//! add a byte to the end of the buffer
uint8_t	bufferAddToEnd(tBuffer* buffer, uint8_t data) ;

// ---------------------------------------------------------------------------

//! check if the buffer is full/not full (returns non-zero value if not full)
uint8_t	bufferIsNotFull(tBuffer* buffer) ;

// ---------------------------------------------------------------------------

//! flush (clear) the contents of the buffer
void bufferFlush(tBuffer* buffer) ;

// ---------------------------------------------------------------------------

#endif
