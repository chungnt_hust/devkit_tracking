/*****************************************************************************
* File Name          : BC45_Chip_Function.c
* Author             : CrystalSu
* Version            : V1.0.0.0
* Date               : Feb 10, 2020
* Editor						 : Chungnt
******************************************************************************/

#include "BC45_Function.h"
#include "BC45_Chip_Function.h"

//****************************************************************************//
uint8_t	 Data_Wr[65] ; // 0-64 = 65 Bytes
uint8_t  Data_Rd[65] ; // 0-64 = 65 Bytes
volatile uint8_t  BC_Chip_Type ;

tBC45_BOARD_PROPERTY  BC45_BOARD_PROPERTY ;

tStandard_Param   Param_14443A_106 ;
tStandard_Param   Param_14443A_212 ;
tStandard_Param   Param_14443A_424 ;
tStandard_Param   Param_14443A_848 ;
tStandard_Param   Param_14443B_106 ;
tStandard_Param   Param_14443B_212 ;
tStandard_Param   Param_14443B_424 ;
tStandard_Param   Param_14443B_848 ;
tStandard_Param   Param_15693_1sub_lo ;
tStandard_Param   Param_15693_1sub_hi ;
tStandard_Param   Param_15693_1sub_uhi ;
tStandard_Param   Param_15693_2sub_lo ;
tStandard_Param   Param_15693_2sub_hi ;

uint8_t	  Data_TxRF[DATA_RF_BUFFER_SIZE] ;
uint8_t   Data_RxRF[DATA_RF_BUFFER_SIZE] ;
uint16_t  LenData_TxRF ;
uint16_t  LenData_RxRF ;

/*******************************************************************************
* Function Name  : Set RSTPD pin
* Description    : None
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void BC45_RSTPD_SET(void)
{
	// Fill GPIO port and PIN which will be use for RSTPD
	RSTPD_SET(BC45_RSTPD_GPIO_Port, BC45_RSTPD_Pin);	
}


/*******************************************************************************
* Function Name  : Clear RSTPD pin
* Description    : None
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void BC45_RSTPD_CLR(void)
{
	// Fill GPIO port and PIN which will be use for RSTPD
	RSTPD_CLR(BC45_RSTPD_GPIO_Port, BC45_RSTPD_Pin);	
}


/*******************************************************************************
* Function Name  : BC45 IRQ PIN
* Description    : Check BC45 IRQ Pin which connected to BC45 IRQ pin
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
uint8_t BC45_IRQ_PIN(void)
{
	return IRQ_PIN(BC45_IRQ_GPIO_Port, BC45_IRQ_Pin);
}


/*******************************************************************************
* Function Name  : BC45_Chip_Reset
* Description    : Reset BC45 and check Chip version
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void BC45_Chip_Reset( void ) 
{
	BC45_RSTPD_SET() ;
	HAL_Delay(10) ;
	BC45_RSTPD_CLR() ;
	HAL_Delay(10) ;
	BC_Chip_Type = BC45_Get_Chip_Version();
}

/*******************************************************************************
* Function Name : BC45_Read_FIFOLength
* Description   : Read FIFOLength Register(Reg 0x04)
* Input         : None
* Output        : None
* Return        : Data in register
*******************************************************************************/
uint8_t BC45_Read_FIFOLength( void )
{
	BC45_SPI_readSingleRegister(FIFOLength, &Data_Rd[FIFOLength]);

	return Data_Rd[FIFOLength] ;	
}

/*******************************************************************************
* Function Name : BC45_Check_RFErr
* Description   : Return error in Error Flag Resgister(Reg 0x0A) ralated to RF communication if error exists. Key Err is excluded.
*				  If More than one error occur, BC45_Check_RFErr reports only one error having the highest priority. 
*				  The priority of errors are listed as follows (The top most is the highest priority)
*						- FRAMING_ERR 
*						- COLLISION_ERR
*						- PARITY_ERR
*						- CRC_ERR
*						- BUFFER_OVERFLOW_ERR
*				  If there is no error, _SUCCESS_ is returned.
* Input         : None
* Output        : None
* Return        : 
*				  - Err_Status
*						- _SUCCESS_
*						- FRAMING_ERR 
*						- COLLISION_ERR
*						- PARITY_ERR
*						- CRC_ERR
*						- BUFFER_OVERFLOW_ERR
*******************************************************************************/
uint8_t BC45_Check_RFErr( void )
{
	uint8_t  Err_Status ;

	BC45_SPI_readSingleRegister(ErrorReg, &Data_Rd[ErrorReg]);
	
	if ( (Data_Rd[ErrorReg] & FramingErr_Mask) == FramingErr_Mask )											  
	{	// Highest priority																	  						  
		Err_Status = FRAMING_ERR ; 
	}																								  
	else if ( (Data_Rd[ErrorReg] & CollErr_Mask) == CollErr_Mask )									  
	{																		  						  
		Err_Status = COLLISION_ERR ; 
	}																		  						  
	else if ( (Data_Rd[ErrorReg] & ParityErr_Mask) == ParityErr_Mask )								  
	{																		  						  
		Err_Status = PARITY_ERR ; 
	}																								  
	else if ( (Data_Rd[ErrorReg] & CRCErr_Mask) == CRCErr_Mask )
	{	
		Err_Status = CRC_ERR ; 
	}
	else if ( (Data_Rd[ErrorReg] & FIFOOvfl_Mask) == FIFOOvfl_Mask )
	{	
		Err_Status = BUFFER_OVERFLOW_ERR ; 
	}
	else
	{	// No Err
		Err_Status = _SUCCESS_ ; 
	}
	
	return Err_Status ;
} 

/*******************************************************************************
* Function Name : BC45_WriteFIFO
* Description   : Write data into FIFO register(Reg 0x02)
* Input         :  
*				  - Data_Wr
*				  - LenData_Wr
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_WriteFIFO( uint8_t *Data_Wr, uint16_t LenData_Wr ) 
{ 	
	BC45_SPI_writeMultiData(FIFOData, Data_Wr, LenData_Wr);
}

/*******************************************************************************
* Function Name : BC45_WriteCMD
* Description   : Execute command by Writing command into Command Register(Reg 0x01)
* Input         : 
*				  - Command
*				   		- CMDIdle
*				   		- CMDTransmit
*				   		- CMDReceive
*				   		- CMDTranceive
*				   		- CMDWriteE2
*				   		- CMDReadE2
*				   		- CMDLoadConfig
*				   		- CMDCalCRC
*				   		- CMDLoadKey
*				   		- CMDLoadKeyE2
*				   		- CMDAuthent
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_WriteCMD( uint8_t CMD )
{ 	
	BC45_SPI_writeSingleRegister(CommandReg, CMD);
}

/*******************************************************************************
* Function Name : BC45_CRC_Setting
* Description   : Enable or Disable appending Tx CRC bytes and checking Rx CRC bytes, by setting or clearing TxCRC and RxCRC bit
* Input         : 
*				  - TxCRC :
*						- TxCRC_Disable
*						- TxCRC_Enable
*				  - RxCRC :
*						- RxCRC_Disable
*						- RxCRC_Enable
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_CRC_Setting( uint8_t TxCRC, uint8_t RxCRC )
{
	BC45_SPI_readSingleRegister(CRCSetting,  &Data_Rd[CRCSetting]);
	if ( TxCRC == TxCRC_Enable )
	{
		Data_Rd[CRCSetting] |= TxCRCEn_Set_Mask ; 		
	}
	else
	{
		Data_Rd[CRCSetting] &= TxCRCEn_Clear_Mask  ; 		
	}

	if ( RxCRC == RxCRC_Enable )
	{
		Data_Rd[CRCSetting] |= RxCRCEn_Set_Mask ; 		
	}
	else
	{
		Data_Rd[CRCSetting] &= RxCRCEn_Clear_Mask ; 		
	}

	Data_Wr[CRCSetting] = Data_Rd[CRCSetting] ;
	BC45_SPI_writeSingleRegister(CRCSetting, Data_Wr[CRCSetting]);
}

/*******************************************************************************
* Function Name : BC45_Flush_FIFO
* Description   : Clear the FIFO buffer by Setting FlushFIFO Bit in Control Register(0x09), bit0 (Flush FIFO bit)
* Input         : None
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_Flush_FIFO( void )
{	
	// Read Register 0x09 before write
	BC45_SPI_readSingleRegister(ControlReg, &Data_Rd[ControlReg]);
	// Set Only FlushFIFO Bit (Bit0)
	Data_Wr[ControlReg] = Data_Rd[ControlReg] | FlushFIFO_Set_Mask ;
	BC45_SPI_writeSingleRegister(ControlReg, Data_Wr[ControlReg]);
}

/*******************************************************************************
* Function Name : BC45_Disable_And_Clear_Flag_IRQ
* Description   : Disable interrupt and clear interrupt flag by Clearing Interrupt Enable and Interrupt Flag Register (Reg 0x06, 0x07)
* Input         : 
*				  - IRQ_Source : IRQ Source as defined in Interrupt enable and Interrupt flag register (BC45 Datasheet)
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_Disable_And_ClearFlag_IRQ( uint8_t IRQ_Source )
{	
	uint8_t  temp_IRQ_Source ;

	temp_IRQ_Source = 0x7F & IRQ_Source ; // Force MSB Bit is '0' 
	
	BC45_SPI_writeSingleRegister( InterruptEn, temp_IRQ_Source); //Disable Interrupt
	BC45_SPI_writeSingleRegister(InterruptRq, temp_IRQ_Source); //Clear Interrupt
}

/*******************************************************************************
* Function Name : BC45_Clear_State_When_TimeOut
* Description   : Clear BC45 internal state to idle when timer interrupt occurs 
* Input         : None
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_Clear_State_When_TimeOut( void )
{
	// Disable all interrupt enable	reg
	// Disable all interrupt flag reg
	// Write Idle Cmd
	// Flush FIFO
	
	Data_Wr[InterruptEn] = CLEAR_ALL_IRQ_SOURCE ;	// Disable All Interrupt Source  
	Data_Wr[InterruptRq] = CLEAR_ALL_IRQ_SOURCE ;	// Clear All Interrupt Flg  
	Data_Wr[CommandReg]  = CMDIdle ;	// Write Idle Cmd 

	BC45_SPI_writeSingleRegister(InterruptEn, Data_Wr[InterruptEn]);
	BC45_SPI_writeSingleRegister(InterruptRq, Data_Wr[InterruptRq]);
	BC45_SPI_writeSingleRegister(CommandReg , Data_Wr[CommandReg]);
	
	BC45_Flush_FIFO() ;
}

/*******************************************************************************
* Function Name : BC45_ON_RF
* Description   : Turn on RF field, depending on driver type, by configure TxControl register(Reg 0x11)
* Input         : 
*				  - Driver_Config_Type : 
*						- DRIVER_CONFIG_X_CC     : Diffential Close Coupling Network with internal envelope
*						- DRIVER_CONFIG_X_CCXENV : Diffential Close Coupling Network with external envelope
*						- DRIVER_CONFIG_X_CCXENV : Diffential Close Coupling Network with external envelope
*						- DRIVER_CONFIG_X_E50OUT : 50 ohm output from Class-E driver with external envelope 
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_ON_RF( uint8_t  Driver_Config_Type ) 
{
	if(Driver_Config_Type == DRIVER_CONFIG_X_E50OUT) // For Class-E Antenna
	{	
		// Read Register 0x11 before write
		BC45_SPI_readSingleRegister(TxControl, &Data_Rd[TxControl]);
		Data_Wr[TxControl] = ((Data_Rd[TxControl] & Force100ASK_Clear_Mask & TX2Inv_Clear_Mask & TX1RFEn_Clear_Mask)
                         | TX2Cw_Set_Mask | TX2RFEn_Set_Mask);
		BC45_SPI_writeSingleRegister(TxControl, Data_Wr[TxControl]);
	}
	else
	{
		// Read Register 0x11 before write
		BC45_SPI_readSingleRegister(TxControl, &Data_Rd[TxControl]);
		// Set Only Bit TX2RFEn and TX1RFEn (Bit1:0)
		Data_Wr[TxControl] = (Data_Rd[TxControl] | TX2RFEn_Set_Mask | TX1RFEn_Set_Mask);	
		BC45_SPI_writeSingleRegister(TxControl, Data_Wr[TxControl]);
	}
}

/*******************************************************************************
* Function Name : BC45_OFF_RF
* Description   : Turn off RF field, depending on driver type, by configure TxControl register(Reg 0x11)
* Input         : 
*				  - Driver_Config_Type : 
*						- DRIVER_CONFIG_X_CC     : Diffential Close Coupling Network with internal envelope
*						- DRIVER_CONFIG_X_CCXENV : Diffential Close Coupling Network with external envelope
*						- DRIVER_CONFIG_X_CCXENV : Diffential Close Coupling Network with external envelope
*						- DRIVER_CONFIG_X_E50OUT : 50 ohm output from Class-E driver with external envelope 
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_OFF_RF( uint8_t  Driver_Config_Type ) 
{
	if(Driver_Config_Type == DRIVER_CONFIG_X_E50OUT) // For Class-E Antenna
	{	
		// Read Register 0x11 before write
		BC45_SPI_readSingleRegister(TxControl, &Data_Rd[TxControl]);
		Data_Wr[TxControl] = (Data_Rd[TxControl] & (~ModSource_Clear_Mask)); 
		BC45_SPI_writeSingleRegister(TxControl, Data_Wr[TxControl]);
	}
	else
	{
		// Read Register 0x11 before write
		BC45_SPI_readSingleRegister(TxControl, &Data_Rd[TxControl]);
		// Clear only bit TX2RFEn and TX1RFEn (Bit1:0)	
		Data_Wr[TxControl] = (Data_Rd[TxControl] & TX2RFEn_Clear_Mask & TX1RFEn_Clear_Mask); 
		BC45_SPI_writeSingleRegister(TxControl, Data_Wr[TxControl]);
	}
}

/*******************************************************************************
* Function Name : BC45_Timer_Tstart_Tstop_Control
* Description   : Configure the timer start and stop in TimerControl Register(Reg 0x2B)
* Input         : 
*				  - TStartWhen : 
*						- TimerManual 
*						- TStartTxBegin 
*						- TStartTxEnd 
*				  - TStopWhen  :  
*						- TimerManual 
*						- TStopRxBegin 
*						- TStopRxEnd 
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_Timer_Tstart_Tstop_Control( uint8_t TStartWhen, uint8_t TStopWhen ) 
{	
	Data_Wr[TimerControl] = TStartWhen  | ( TStopWhen << 2 ) ;	
	BC45_SPI_writeSingleRegister(TimerControl, Data_Wr[TimerControl]);
}

/*******************************************************************************
* Function Name : BC45_Start_Timer
* Description   : Set TStartNow Bit in Control Register(0x09), bit1 (TStartNow bit)
* Input         : None
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_Start_Timer( void )
{	
	// Read Register 0x09 before write
	BC45_SPI_readSingleRegister(ControlReg, &Data_Rd[ControlReg]);
	// Set Only Bit TStartNow (Bit1)
	Data_Wr[ControlReg] = Data_Rd[ControlReg] | TStartNow_Set_Mask ;
	BC45_SPI_writeSingleRegister(ControlReg, Data_Wr[ControlReg]);
}

/*******************************************************************************
* Function Name : BC45_Stop_Timer
* Description   : Set TStopNow Bit in Control Register(0x09), bit2 (TStopNow bit)
* Input         : None
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_Stop_Timer( void )
{	
	// Read Register 0x09 before write
	BC45_SPI_readSingleRegister(ControlReg, &Data_Rd[ControlReg]);
	// Set Only Bit TStopNow (Bit2)
	Data_Wr[ControlReg] = Data_Rd[ControlReg] | TStopNow_Set_Mask ;	
	BC45_SPI_writeSingleRegister(ControlReg, Data_Wr[ControlReg]);
}

/*******************************************************************************
* Function Name : BC45_Transceive
* Description   : Transmit data and receive response data by executing transceive command
* Input         : 
*				  - Data_Tx 		: Transmitted Data
*				  - LenData_Tx 	: Length of Transmitted Data  
* Output        : 
*				  - Data_Rx 		: Received Data
*				  - LenData_Rx 	: Length of Received Data  
* Return        : 
*				  - Status
*						- _SUCCESS_
*						- NO_RESPONSE
*						- FRAMING_ERR 
*						- COLLISION_ERR
*						- PARITY_ERR
*						- CRC_ERR
*						- BUFFER_OVERFLOW_ERR
*						- ASIC_EXE_TIMEOUT
*******************************************************************************/
uint8_t BC45_Transceive(uint8_t  *Data_Tx ,uint16_t  LenData_Tx, uint8_t  *Data_Rx ,uint16_t  *LenData_Rx, uint16_t *Tick)
{
	uint8_t   Resp = 0;
	
	if(*Tick == BC45_TICK_TXRX_EN)
	{
		//DEBUG("BC45_TICK_TX_EN\r\n");
		*Tick = BC45_TICK_TX_EN;
		BC45_Disable_And_ClearFlag_IRQ( ALL_IRQ_SOURCE ) ;
		BC45_Flush_FIFO() ;

		//BC45_Timer_Tstart_Tstop_Control( TStartTxEnd, TStopRxBegin ) ;
	}

	//------------------- Transmit -------------------//
	Resp = BC45_IRQ_Execute_FIFO_Data(CMDTranceive, Data_Tx, LenData_Tx, Tick);
	
  if (( Resp == _SUCCESS_ ) && (*Tick != BC45_TICK_TIMEOUT))
	{
		//DEBUG("BC45_TICK_RX_EN\r\n");
		Resp = 0;
		*Tick = BC45_TICK_RX_EN;
	}
	
	//------------------- Recieve -------------------//
	Resp = BC45_IRQ_Read_FIFO(Data_Rx, LenData_Rx, Tick) ;
	
	if (( Resp == _SUCCESS_ ) && (*Tick != BC45_TICK_TIMEOUT))
	{
		Resp = BC45_Check_RFErr() ;	
	}
	else *( LenData_Rx ) = 0 ;			

	return Resp ;
}

/*******************************************************************************
* Function Name : BC45_IRQ_Execute_FIFO_Data
* Description   : Write data into FIFO of BC45 and execute command and wait until irq occurs 
*				  Transmitted Data can > 64 byte 

* Input         : 
*				  - CMD : Commands
*						- CMDTransmit(0x1A)
*						- CMDTransceive(0x1E)
*						- CMDWriteE2(0x01)
*						- CMDLoadKey(0x19)
*						- CMDLoadKeyE2(0x0B)
*						- Authent(0x1C)
*				  	- Data 	    : Data
*				  	- LenData		: Length of Data  
* Output        : None
* Return        : 
*				  	- Status
*							- _SUCCESS_
*							- NO_RESPONSE
*							- ASIC_EXE_TIMEOUT
*******************************************************************************/
uint8_t BC45_IRQ_Execute_FIFO_Data(uint8_t  CMD, uint8_t  *Data, uint16_t  LenData, uint16_t *Tick)
{
	static uint8_t Flg_WriteCMD = 0;
	uint8_t   Status = 0;
	uint16_t  Index = 0 ;
	
	if(*Tick == BC45_TICK_TX_EN)
	{
		//DEBUG("BC45_TICK_TX_START\r\n");
		*Tick = BC45_TICK_TX_START;
		// Enable Idle,LoAlert,Timer irq
		Data_Wr[InterruptEn] = 0x80 | TxIRQ | LoAlertIRQ | TimerIRQ | IdleIRQ ;	 
		BC45_SPI_writeSingleRegister(InterruptEn, Data_Wr[InterruptEn]);
	}
	
	if((*Tick >= BC45_TICK_TX_START) && (*Tick < BC45_TICK_TX_TIMEOUT))
	{
		*Tick = *Tick + BC45_TICK_PERIOD;
		//DEBUG("TX_START++: [%d]\r\n", *Tick);
		if ( BC45_IRQ_PIN() == 1 )  // Check IRQ pin 
		{					
			// Check irq source
			BC45_SPI_readSingleRegister(InterruptRq, &Data_Rd[InterruptRq]);// Read irq flag
			if ( ((Data_Rd[InterruptRq] & IdleIRQ) == IdleIRQ) || ((Data_Rd[InterruptRq] & TxIRQ) == TxIRQ) ) // Check irq flag that is IdleIRQ, TxIRQ or not ?
			{
				//DEBUG("TX:Transmition complete\r\n");
				// Transmition complete
				BC45_Disable_And_ClearFlag_IRQ( ALL_IRQ_SOURCE ) ;
				Flg_WriteCMD = 0;
				
				Status = _SUCCESS_ ;
			}
			else if ( (Data_Rd[InterruptRq] & TimerIRQ) == TimerIRQ )	// Check Timer irq	(Check Time Out)
			{	
				//DEBUG("TX: Time out ChipReader(BC45)\r\n");
				// Time out ChipReader(BC45)
				BC45_Clear_State_When_TimeOut() ;
				Flg_WriteCMD = 0;
				
				Status = NO_RESPONSE ;
			}
			else if ( (Data_Rd[InterruptRq] & LoAlertIRQ) == LoAlertIRQ )	// Check IRQ flag that is a LoAlertIRQ or not ?
			{
				//DEBUG("TX: else\r\n");
				if ( Index != LenData ) 
				{
					if ( (LenData - Index) > HiLevel ) // HiLevel = 64 - Water level
					{	
						// Write tx data into FIFO = HiLevel Bytes
						BC45_SPI_writeMultiData(FIFOData, (Data + Index), HiLevel);
						Index = Index + HiLevel ;					
						
						// Clear LoAlert flag
						Data_Wr[InterruptRq] = 0x00 | LoAlertIRQ ;
						BC45_SPI_writeSingleRegister(InterruptRq, Data_Wr[InterruptRq]);
					}
					else
					{	
						// Write Tx Data into FIFO = LenData - Index Bytes (Complete Writing)
						BC45_SPI_writeMultiData(FIFOData, (Data + Index), (LenData - Index));
						Index = LenData ;			
				
						// Should be disable LoAlertIRQ after write data completely.   
						// If not disable LoAlertIRQ ( It always active if not data in FIFO),It will not detect rising edged from irq pin(Cannot detect TxIRQ). 

						// Disable and Clear Flg LoAlertIRQ
						BC45_Disable_And_ClearFlag_IRQ( LoAlertIRQ ) ;
					}
				}

				if ( Flg_WriteCMD == 0 ) // Write data into FIFO before send command
				{	
					// Write Cmd	( Once time )
					Flg_WriteCMD = 1 ;
					BC45_WriteCMD( CMD ) ;
				}
			}
		}			
	}
	else if(*Tick == BC45_TICK_TX_TIMEOUT)
	{
		*Tick = BC45_TICK_TIMEOUT;
		BC45_Clear_State_When_TimeOut() ;
		Flg_WriteCMD = 0;
		
		Status = ASIC_EXE_TIMEOUT ;
	}

	return Status ;
}

/*******************************************************************************
* Function Name : BC45_IRQ_Read_FIFO
* Description   : Read FIFO and store in Data_Rx until reception data complete(Idle IRQ occur)
* Input         : 
*        	
* Output        : 
*				  - Data_Rx 	   		: Received Data 
*				  - LenData_Rx     	: Length of Received Data  
* Return        : 
*				  - Status
*						- _SUCCESS_
*						- NO_RESPONSE
*						- ASIC_EXE_TIMEOUT
*******************************************************************************/
uint8_t BC45_IRQ_Read_FIFO(uint8_t  *Data_Rx, uint16_t  *LenData_Rx, uint16_t *Tick)
{
	static uint16_t Index = 0 ; 
	uint8_t  Status = 0, Length = 0;
	
	if(*Tick == BC45_TICK_RX_EN)
	{
		//DEBUG("BC45_TICK_RX_START\r\n");
		*Tick = BC45_TICK_RX_START;
		// Enable HiAlert,Idle,Timer IRQ
		Data_Wr[InterruptEn] = 0x80 | HiAlertIRQ | IdleIRQ | TimerIRQ | RxIRQ ;
		BC45_SPI_writeSingleRegister(InterruptEn, Data_Wr[InterruptEn]);
	}
        
	if((*Tick >= BC45_TICK_RX_START) && (*Tick < BC45_TICK_RX_TIMEOUT))
	{
		*Tick = *Tick + BC45_TICK_PERIOD;
		//DEBUG("RX_START++: [%d]\r\n", *Tick);
		if ( BC45_IRQ_PIN() == 1 )  // Check external interrupt(Interrput from BC45) 
		{
			//DEBUG("RX int\r\n");			
			// Check irq source
			BC45_SPI_readSingleRegister(InterruptRq, &Data_Rd[InterruptRq]);
			if ( ((Data_Rd[InterruptRq] & IdleIRQ) == IdleIRQ ) || ((Data_Rd[InterruptRq] & RxIRQ) == RxIRQ ) )		// Check irq flag that is a recieve complete or not ?
			{
				//DEBUG("RX: Read FIFO length of data \r\n");
				// Read FIFO length of data 
				BC45_SPI_readSingleRegister(FIFOLength, &Length);

				if( ( Index + Length ) >= DATA_RF_BUFFER_SIZE )
				{
					//DEBUG("RX: Force Num_data = 0 byte\r\n");
					BC45_Clear_State_When_TimeOut() ;
					*(LenData_Rx) = 0 ;	 // Force Num_data = 0 byte
					//Flg_Rx_Complete = 1 ;
					Index = 0 ;
					Status = ASIC_EXE_TIMEOUT ;
				}
				else
				{
					//DEBUG("RX: BC45_SPI_readSingleRegister\r\n");
					BC45_SPI_readSingleRegister(DecoderControl, &Data_Rd[0x1A]);

					BC45_SPI_readMultiData(FIFOData, (Data_Rx + Index), Length);
					Index += Length ;
					*(LenData_Rx) = Index ;
					BC45_Disable_And_ClearFlag_IRQ( ALL_IRQ_SOURCE ) ;
																							
					//Flg_Rx_Complete = 1 ;
					Index = 0 ;
					Status = _SUCCESS_ ;
				}
			}
			else if ( (Data_Rd[InterruptRq] & HiAlertIRQ) == HiAlertIRQ )	// Check HiAlert irq
			{
				//DEBUG("RX: else\r\n");
				if( ( Index + HiLevel ) >= DATA_RF_BUFFER_SIZE )
				{
					BC45_Clear_State_When_TimeOut() ;
					*(LenData_Rx) = 0 ;	 // Force Num_data = 0 byte
					//Flg_Rx_Complete = 1 ;
					Index = 0 ;
					Status = ASIC_EXE_TIMEOUT ;
				}
				else
				{
					BC45_SPI_readMultiData(FIFOData, (Data_Rx + Index), HiLevel);
					Index += HiLevel ;

					Data_Wr[InterruptRq] = 0x00 | HiAlertIRQ ;	// Clear irq HiAlertIRQ flag  
					BC45_SPI_writeSingleRegister(InterruptRq, Data_Wr[InterruptRq]);
				}
			}
			else if ( (Data_Rd[InterruptRq] & TimerIRQ) == TimerIRQ )	// Check Timer irq(Check Time Out)
			{						
				//DEBUG("RX: Time out ChipReader(BC45)\r\n");
				// Time out ChipReader(BC45)
				if ( Index > 0 ) // Case of RxMultiple is set
				{
					Status = _SUCCESS_ ;
				}
				else
				{
					Status = NO_RESPONSE ;
				}
				
				BC45_Clear_State_When_TimeOut() ;
				//Flg_Rx_Complete = 1 ;
				Index = 0 ;
				*(LenData_Rx) = Index ;
			}			
		}			
	}
	else if(*Tick == BC45_TICK_RX_TIMEOUT)
	{	
		*Tick = BC45_TICK_TIMEOUT;
		// Time-Out (Microcontroller)
		if ( Index > 0 ) // Case of RxMultiple is set
		{
				Status = _SUCCESS_ ;
		}
		else
		{
			Status = ASIC_EXE_TIMEOUT ;
		}
					
		*(LenData_Rx) = Index ; 	 // Force Num_data = 0 byte
		
		//Flg_Rx_Complete = 1 ;
		Index = 0 ;
		BC45_Clear_State_When_TimeOut() ;
	}
	
	return Status ;
}

/*******************************************************************************
* Function Name : BC45_Config_Driver
* Description   : Configure Transmitter Control Register (0x11) of BC45 following driving network type
* Input         : 
*				  - Driver_Config_Type : 
*						- DRIVER_CONFIG_X_CC     : Diffential Close Coupling Network with internal envelope
*						- DRIVER_CONFIG_X_CCXENV : Diffential Close Coupling Network with external envelope
*						- DRIVER_CONFIG_X_S50OUT : Single-ended Drive with external envelope
*						- DRIVER_CONFIG_X_E50OUT : 50 ohm output from Class-E driver with external envelope 
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_Config_Driver(uint8_t  Driver_Config_Type)	 
{
	BC45_SPI_readSingleRegister( RxControl2, &Data_Rd[RxControl2]);
	if ( Driver_Config_Type == DRIVER_CONFIG_X_CCXENV ) 
	{
		Data_Wr[TxControl] = 0x58 ;	
		Data_Wr[RxControl2] = Data_Rd[RxControl2] | 0x04 ; // Set bit 2  		
	}
	else if ( Driver_Config_Type == DRIVER_CONFIG_X_S50OUT ) 
	{
		Data_Wr[TxControl] = 0x50 ;	
		Data_Wr[RxControl2] = Data_Rd[RxControl2] | 0x04 ; // Set bit 2  		
	}
	else if ( Driver_Config_Type == DRIVER_CONFIG_X_E50OUT ) 
	{
		Data_Wr[TxControl] = 0x00 ;	
		Data_Wr[RxControl2] = Data_Rd[RxControl2] | 0x04 ; // Set bit 2  		
	}
	else //  X_CC
	{
		Data_Wr[TxControl] = 0x58 ;			
		Data_Wr[RxControl2] = Data_Rd[RxControl2] & 0xFB ; // Clear bit 2  		
	}

	BC45_SPI_writeSingleRegister(TxControl, Data_Wr[TxControl]);
	BC45_SPI_writeSingleRegister(RxControl2, Data_Wr[RxControl2]);
}

/*******************************************************************************
* Function Name : BC45_InitChip
* Description   : Initialze Transmitter Control Register of BC45
* Input         : 
*				  - Driver_Config_Type : 
*						- DRIVER_CONFIG_X_CC     : Diffential Close Coupling Network with internal envelope
*						- DRIVER_CONFIG_X_CCXENV : Diffential Close Coupling Network with external envelope
*						- DRIVER_CONFIG_X_S50OUT : Single-ended Drive with external envelope
*						- DRIVER_CONFIG_X_E50OUT : 50 ohm output from Class-E driver with external envelope 
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_InitChip(uint8_t  Driver_Config_Type)	 
{	
	uint8_t page;

  BC45_Config_Driver(Driver_Config_Type);	//antenna type
  Data_Wr[ModConductance] = Param_14443B_106.GsCfgMod;	
	// Write to BC45 register
  BC45_SPI_writeSingleRegister(ModConductance, Data_Wr[ModConductance]);

  Data_Wr[RxControl2]  = 0x41;	//? this will changed the value set by 	BC45_Config_Driver
  Data_Wr[RxControl3]  = Param_14443A_106.RxControl_3;	
  Data_Wr[RxThreshold] = Param_14443A_106.Rx_Threshold;	
	// Write to BC45 register
  BC45_SPI_writeSingleRegister(RxThreshold, Data_Wr[RxThreshold]);
  BC45_SPI_writeSingleRegister(RxControl2, Data_Wr[RxControl2]);
  BC45_SPI_writeSingleRegister(RxControl3, Data_Wr[RxControl3]); // Availble for BC45B4523

	//low/high pass filter
  Data_Wr[ManFilterCtrl] = 0x19;	//Reg 0x2E
  Data_Wr[FilterCorCoef] = 0x45;	//Reg 0x2F
  
	// Write to register

  /* Select Page 1 for BC45 */
  page = 0x01;
  BC45_SPI_writeSingleRegister(0x00, page);
  /**************************/
  BC45_SPI_writeSingleRegister(ManFilterCtrl, Data_Wr[ManFilterCtrl]);
  BC45_SPI_writeSingleRegister(FilterCorCoef, Data_Wr[FilterCorCoef]);

  /* Back to Page 0 for BC45*/
  page = 0x00;
  BC45_SPI_writeSingleRegister(0x00, page);
  /**********************/
        
  Data_Wr[FIFOLevel]      = WaterLevel; // FIFO Level
	Data_Wr[IOConfig]       = 0x00;	      // Not inv irq
	Data_Wr[TestAnaSel]     = 0x2E;
	//crystal
	//Data_Wr[TxDisableCtrl]  = 0x00;	
	//Data_Wr[ISO15693Header] = 0x01;	
  //Data_Wr[AnalogAdjust1]  = Param_14443A_106.AnaAdjust1;
	Data_Wr[AnalogAdjust2]  = Param_14443A_106.AnaAdjust2;	
	Data_Wr[AnalogAdjust3]  = Param_14443A_106.AnaAdjust3;
  // Write to HTK register
	BC45_SPI_writeSingleRegister(FIFOLevel, Data_Wr[FIFOLevel]);
	BC45_SPI_writeSingleRegister(IOConfig, Data_Wr[IOConfig]);
	BC45_SPI_writeSingleRegister(TestAnaSel, Data_Wr[TestAnaSel]);
	//BC45_SPI_writeSingleRegister(ISO15693Header, Data_Wr[ISO15693Header]);// Availble for BC450
	BC45_SPI_writeSingleRegister(AnalogAdjust2, Data_Wr[AnalogAdjust2]);

        
	BC45_Disable_And_ClearFlag_IRQ(ALL_IRQ_SOURCE);	
}

/*******************************************************************************
* Function Name : BC45_Config_15693
* Description   : Configure Registers for ISO15693 Standard Operation
* Input         : None
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_Config_15693( void )
{	
	uint8_t dataReg;

	//BC45 Ref B configuration for iso15693 ==================================
	//Tx 1 of 4, Rx 1 Sub Hi
	Data_Wr[TxControl] 				= 0x5B;
	Data_Wr[CWConductance]			= 0x3F;
	Data_Wr[ModConductance]			= 0x10;
	Data_Wr[CoderControl]			= 0x2F;
	Data_Wr[ModWidth]				= 0x3F;
	Data_Wr[ModWidthSOF]			= 0x3F;
	Data_Wr[TypeBFraming]			= 0x3B;

	Data_Wr[RxControl1]				= 0x93;
	Data_Wr[DecoderControl]			= 0x30;
	Data_Wr[Bitphase]				= 0x40;
	Data_Wr[RxThreshold]			= 0x4C;//0x88;
	Data_Wr[BPSKDemod]				= 0x04;
	Data_Wr[RxControl2]				= 0x41;
	Data_Wr[RxControl3]				= 0xF3;//0xE3;

	Data_Wr[RxWait]					= 0x01;
	Data_Wr[CRCSetting]				= 0x2C;
	Data_Wr[CRCPresetLSB]			= 0xFF;
	Data_Wr[CRCPresetMSB]			= 0xFF;

	Data_Wr[FIFOLevel]				= 0x10;
	Data_Wr[TimerClock]				= 0x0A;
	Data_Wr[TimerControl]			= 0x06;
	Data_Wr[TimerReload]			= 0xFF;
	Data_Wr[ManFilterCtrl]			= 0x19;
	Data_Wr[FilterCorCoef]			= 0x45;

	Data_Wr[IOConfig]				= 0x00;
	Data_Wr[TestAnaSel]				= 0x2E;
	Data_Wr[AnalogAdjust3]			= 0x28;//0x88;//0x80;
	//Data_Wr[AnalogAdjust1]			= 0xB8;
	Data_Wr[AnalogAdjust2]			= 0x12;

	//Select register sector 0
	BC45_SPI_writeSingleRegister(PageReg, 0x00);
	BC45_SPI_writeSingleRegister(TxControl,  Data_Wr[TxControl]);
	BC45_SPI_writeSingleRegister(CWConductance,  Data_Wr[CWConductance]);
	BC45_SPI_writeSingleRegister(ModConductance, Data_Wr[ModConductance]);
	BC45_SPI_writeSingleRegister(CoderControl,  Data_Wr[CoderControl]);
	BC45_SPI_writeSingleRegister(ModWidth,  Data_Wr[ModWidth]);
	BC45_SPI_writeSingleRegister(ModWidthSOF, Data_Wr[ModWidthSOF]);
	BC45_SPI_writeSingleRegister(TypeBFraming,  Data_Wr[TypeBFraming]);

	BC45_SPI_writeSingleRegister(RxControl1,  Data_Wr[RxControl1]);
	BC45_SPI_writeSingleRegister(DecoderControl,  Data_Wr[DecoderControl]);
	BC45_SPI_writeSingleRegister(Bitphase, Data_Wr[Bitphase]);
	BC45_SPI_writeSingleRegister(RxThreshold,  Data_Wr[RxThreshold]);
	BC45_SPI_writeSingleRegister(BPSKDemod,  Data_Wr[BPSKDemod]);
	BC45_SPI_writeSingleRegister(RxControl2, Data_Wr[RxControl2]);
	BC45_SPI_writeSingleRegister(RxControl3,  Data_Wr[RxControl3]);

	BC45_SPI_writeSingleRegister(RxWait,  Data_Wr[RxWait]);
	BC45_SPI_writeSingleRegister(CRCSetting,  Data_Wr[CRCSetting]);
	BC45_SPI_writeSingleRegister(CRCPresetLSB, Data_Wr[CRCPresetLSB]);
	BC45_SPI_writeSingleRegister(CRCPresetMSB,  Data_Wr[CRCPresetMSB]);

	BC45_SPI_writeSingleRegister(FIFOLevel,  Data_Wr[FIFOLevel]);
	BC45_SPI_writeSingleRegister(TimerClock,  Data_Wr[TimerClock]);
	BC45_SPI_writeSingleRegister(TimerControl, Data_Wr[TimerControl]);
	BC45_SPI_writeSingleRegister(TimerReload,  Data_Wr[TimerReload]);

	//Select register sector 1
	BC45_SPI_writeSingleRegister(PageReg,  0x01);
	BC45_SPI_readSingleRegister(0x0F, &dataReg);
	//Check BC45 Revision
	if(dataReg == BC45_RevB)
	{
		BC45_SPI_writeSingleRegister(0x05, 0x05);
	}
	else
	{
		BC45_SPI_writeSingleRegister(0x05, 0x00);
	}
	BC45_SPI_writeSingleRegister(ManFilterCtrl,  Data_Wr[ManFilterCtrl]);
	BC45_SPI_writeSingleRegister(FilterCorCoef, Data_Wr[FilterCorCoef]);

	//Select register sector 0
	BC45_SPI_writeSingleRegister(PageReg,  0x00);
	BC45_SPI_writeSingleRegister(IOConfig,  Data_Wr[IOConfig]);
	BC45_SPI_writeSingleRegister(AnalogAdjust3,  Data_Wr[AnalogAdjust3]);
	//BC45_SPI_writeSingleRegister(AnalogAdjust1, Data_Wr[AnalogAdjust1]);
	BC45_SPI_writeSingleRegister(AnalogAdjust2,  Data_Wr[AnalogAdjust2]);

	//=========================================================================


	// Tx Session
	/*BC45_Set_ModulatorSource( ModSource_Internal ) ;
	BC45_Set_Force100ASK_Bit() ;

	Data_Wr[CWConductance] = 0x3F ;
	BC45_SPI_writeSingleRegister(CWConductance, Data_Wr[CWConductance]);

	Data_TxRF[ModWidth]    = 0x3F ;	// Set ModWidth
	Data_TxRF[ModWidthSOF] = 0x3F ;	// Set ModWidthSOF

	BC45_SPI_writeSingleRegister(ModWidth	, Data_TxRF[ModWidth]);
	BC45_SPI_writeSingleRegister(ModWidthSOF, Data_TxRF[ModWidthSOF]);
		
	// Rx Session
	Data_Wr[Bitphase]    = Param_15693_1sub_lo.BitPhase  ; // Set bitphase
    Data_Wr[RxThreshold] = Param_15693_1sub_lo.Rx_Threshold  ; // Set CollLevel, MinLevel
	Data_Wr[BPSKDemod]   = Param_15693_1sub_lo.BPSK_Demod ; // Set HP2off, TauD(01), AGC_EN(1), TauAGC(0)
    Data_Wr[RxControl3]  = Param_15693_1sub_lo.RxControl_3  ; // Set CollLevel, MinLevel
        
    BC45_SPI_writeSingleRegister(Bitphase, 		Data_Wr[Bitphase]);
    BC45_SPI_writeSingleRegister(RxThreshold, 	Data_Wr[RxThreshold]);
    BC45_SPI_writeSingleRegister(BPSKDemod,		Data_Wr[BPSKDemod]);
    BC45_SPI_writeSingleRegister(RxControl3, 	Data_Wr[RxControl3]);

	Data_Wr[RxWait] = 0x01 ;	// Set RxWait
	BC45_SPI_writeSingleRegister(RxWait, Data_Wr[RxWait]);

	// CRC
	Data_Wr[CRCSetting]   = 0x2C ;	// No Parity
	Data_Wr[CRCPresetLSB] = 0xFF ;	// Preset CRC LSB = 0xFF
	Data_Wr[CRCPresetMSB] = 0xFF ;	// Preset CRC MSB = 0xFF

	BC45_SPI_writeSingleRegister(CRCSetting, 	Data_Wr[CRCSetting]);
	BC45_SPI_writeSingleRegister(CRCPresetLSB, 	Data_Wr[CRCPresetLSB]);
	BC45_SPI_writeSingleRegister(CRCPresetMSB, 	Data_Wr[CRCPresetMSB]);

	// Timer
	Data_Wr[TimerClock]  = 0x0A ;	// Selects Timer Prescaler and set Timer Auto-restart
	Data_Wr[TimerReload] = 0xFF ;	// TReload Value

	BC45_SPI_writeSingleRegister(TimerClock, 	Data_Wr[TimerClock]);
	BC45_SPI_writeSingleRegister(TimerReload, 	Data_Wr[TimerReload]);

	BC45_Clear_Crypto1On_Bit() ;

	Data_Wr[AnalogAdjust1] = Param_15693_1sub_lo.AnaAdjust1 ;
	Data_Wr[AnalogAdjust2] = Param_15693_1sub_lo.AnaAdjust2 ;	
	Data_Wr[AnalogAdjust3] = Param_15693_1sub_lo.AnaAdjust3 ;	
        
	BC45_SPI_writeSingleRegister(AnalogAdjust1, Data_Wr[AnalogAdjust1]);
	BC45_SPI_writeSingleRegister(AnalogAdjust2, Data_Wr[AnalogAdjust2]);
	BC45_SPI_writeSingleRegister(AnalogAdjust3, Data_Wr[AnalogAdjust3]);// Avaalable only BC45B4523
	*/
}

/*******************************************************************************
* Function Name : BC45_Speed_15693
* Description   : Configure Registers Related to RF-Communication Bit Rate following ISO15693 Standard
* Input         : 
*				  - SPD_SEL : Speed selection 
*						High Nibble : Tx_Speed(Bit7:4)
*							- 0000b : 1 out of 256
*							- 0001b : 1 out of 4 
*							- Other : 1 out of 4 
*						Low Nibble : RxSpeed(Bit3:0)				
*							- 0000b : 1 sub low    
*							- 0001b : 1 sub high
*							- 0010b : 1 sub ultra high
*							- 0011b : 2 sub low
*							- 0100b : 2 sub high
*							- Other : 1 sub high
* Output        : None
* Return        : None
*******************************************************************************/
void BC45_Speed_15693( uint8_t SPD_SEL ) 
{	
	uint8_t  Tx_Speed, Rx_Speed ;
	
	Tx_Speed = ( SPD_SEL >> 4 ) ;
	Rx_Speed = ( SPD_SEL & 0x0F ) ; 

	if ( Tx_Speed == SPEED_1_OUT_OF_256 ) // Tx 1 out of 256
	{	
		Data_Wr[CoderControl] = 0x2E ;	// Set Tx Coding
	}
	else  // Tx 1 out of 4
	{	
		Data_Wr[CoderControl] = 0x2F ;	// Set Tx Coding
	}
	BC45_SPI_writeSingleRegister(CoderControl, Data_Wr[CoderControl]);
	
        
	if ( Rx_Speed == SPEED_1_SUB_LOW ) // 1 Sub, Lo Rate
	{	
		Data_Wr[RxControl1]     = 0xD3 ;	// Set SubCCar, SubCPulse
		Data_Wr[DecoderControl] = 0x30 ;	// Set Rx-Framing,Rx Coding (Manc)
		Data_Wr[RxWait] 		= 0x02 ;	// Set RxWait
		
        Data_Wr[Bitphase]       = Param_15693_1sub_lo.BitPhase  ;
        Data_Wr[RxThreshold]   = Param_15693_1sub_lo.Rx_Threshold ;
        Data_Wr[BPSKDemod]     = Param_15693_1sub_lo.BPSK_Demod ;
        Data_Wr[RxControl3]    = Param_15693_1sub_lo.RxControl_3 ;
        //Data_Wr[AnalogAdjust1] = Param_15693_1sub_lo.AnaAdjust1 ;
        Data_Wr[AnalogAdjust2] = Param_15693_1sub_lo.AnaAdjust2 ;
	    Data_Wr[AnalogAdjust3] = Param_15693_1sub_lo.AnaAdjust3 ;
                
	}
	else if	( Rx_Speed == SPEED_1_SUB_ULTRA_HIGH ) // 1 Sub, Ultra-Hi Rate
	{	
		Data_Wr[RxControl1]     = 0x73 ;	// Set SubCCar, SubCPulse
		Data_Wr[DecoderControl] = 0x30 ;	// Set Rx-Framing,Rx Coding (Manc)
		Data_Wr[RxWait] 	= 0x10 ;	// Set RxWait

        Data_Wr[Bitphase]       = Param_15693_1sub_uhi.BitPhase  ;
        Data_Wr[RxThreshold]   = Param_15693_1sub_uhi.Rx_Threshold ;
        Data_Wr[BPSKDemod]     = Param_15693_1sub_uhi.BPSK_Demod ;
        Data_Wr[RxControl3]    = Param_15693_1sub_uhi.RxControl_3 ;
        //Data_Wr[AnalogAdjust1] = Param_15693_1sub_uhi.AnaAdjust1 ;
        Data_Wr[AnalogAdjust2] = Param_15693_1sub_uhi.AnaAdjust2 ;
	    Data_Wr[AnalogAdjust3] = Param_15693_1sub_uhi.AnaAdjust3 ;
	}
	else if ( Rx_Speed == SPEED_2_SUB_LOW ) // 2 Sub, Lo Rate
	{	
		Data_Wr[RxControl1]     = 0xD3 ;	// Set SubCCar, SubCPulse
		Data_Wr[DecoderControl] = 0x32 ;	// Set Rx-Framing,Rx Coding  (FSK)
		Data_Wr[RxWait] 	= 0x02 ;	// Set RxWait
		
        Data_Wr[Bitphase]    = Param_15693_2sub_lo.BitPhase  ;
        Data_Wr[RxThreshold]   = Param_15693_2sub_lo.Rx_Threshold ;
        Data_Wr[BPSKDemod]     = Param_15693_2sub_lo.BPSK_Demod ;
        Data_Wr[RxControl3]    = Param_15693_2sub_lo.RxControl_3 ;
        //Data_Wr[AnalogAdjust1] = Param_15693_2sub_lo.AnaAdjust1 ;
        Data_Wr[AnalogAdjust2] = Param_15693_2sub_lo.AnaAdjust2 ;
	    Data_Wr[AnalogAdjust3] = Param_15693_2sub_lo.AnaAdjust3 ;
	}
	else if ( Rx_Speed == SPEED_2_SUB_HIGH ) // 2 Sub, Hi Rate
	{	
		Data_Wr[RxControl1]     = 0x93 ;	// Set SubCCar, SubCPulse
		Data_Wr[DecoderControl] = 0x32 ;	// Set Rx-Framing,Rx Coding  (FSK)
		Data_Wr[RxWait] 		= 0x08 ;	// Set RxWait

        Data_Wr[Bitphase]       = Param_15693_2sub_hi.BitPhase  ;
        Data_Wr[RxThreshold]   = Param_15693_2sub_hi.Rx_Threshold ;
        Data_Wr[BPSKDemod]     = Param_15693_2sub_hi.BPSK_Demod ;
        Data_Wr[RxControl3]    = Param_15693_2sub_hi.RxControl_3 ;
        //Data_Wr[AnalogAdjust1] = Param_15693_2sub_hi.AnaAdjust1 ;
        Data_Wr[AnalogAdjust2] = Param_15693_2sub_hi.AnaAdjust2 ;
	    Data_Wr[AnalogAdjust3] = Param_15693_2sub_hi.AnaAdjust3 ;
	}
	else  // 1 Sub, Hi Rate
	{	
		Data_Wr[RxControl1]     = 0x93 ;	// Set SubCCar, SubCPulse
		Data_Wr[DecoderControl] = 0x30 ;	// Set Rx-Framing,Rx Coding (Manc)
		Data_Wr[RxWait] 		= 0x08 ;	// Set RxWait

        Data_Wr[Bitphase]      = Param_15693_1sub_hi.BitPhase  ;
        Data_Wr[RxThreshold]   = Param_15693_1sub_hi.Rx_Threshold ;
        Data_Wr[BPSKDemod]     = Param_15693_1sub_hi.BPSK_Demod ;
        Data_Wr[RxControl3]    = Param_15693_1sub_hi.RxControl_3 ;
        //Data_Wr[AnalogAdjust1] = Param_15693_1sub_hi.AnaAdjust1 ;
        Data_Wr[AnalogAdjust2] = Param_15693_1sub_hi.AnaAdjust2 ;
	    Data_Wr[AnalogAdjust3] = Param_15693_1sub_hi.AnaAdjust3 ;
                
	}

	BC45_SPI_writeSingleRegister(RxControl1, 	Data_Wr[RxControl1]);
	BC45_SPI_writeSingleRegister(DecoderControl, Data_Wr[DecoderControl]);
	BC45_SPI_writeSingleRegister(Bitphase, 		Data_Wr[Bitphase]);
	BC45_SPI_writeSingleRegister(RxWait, 		Data_Wr[RxWait]);

	BC45_SPI_writeSingleRegister(RxThreshold, 	Data_Wr[RxThreshold]);
	BC45_SPI_writeSingleRegister(BPSKDemod, 		Data_Wr[BPSKDemod]);
	BC45_SPI_writeSingleRegister(RxControl3, 	Data_Wr[RxControl3]);
//	BC45_SPI_writeSingleRegister(AnalogAdjust1, 	Data_Wr[AnalogAdjust1]);
//	BC45_SPI_writeSingleRegister(AnalogAdjust2, 	Data_Wr[AnalogAdjust2]);
//	BC45_SPI_writeSingleRegister(AnalogAdjust3, 	Data_Wr[AnalogAdjust3]);
        
}


/*******************************************************************************
* Function Name : BC45_Diagnose
* Description   : Diagnose BC45 basic functions and interface between MCU and BC45.
*				  Diagnosis includes FIFO, SPI, IRQ PIN
* Input         : None
* Output        : None
* Return        : 
*				  	- Status
*						- _SUCCESS_				  
*						- BC45_DIAGNOSE_ERR				  
*******************************************************************************/
uint8_t BC45_Diagnose( void )
{
	uint8_t  Status ;
	uint8_t  FIFO_Data ;
	uint8_t  InterruptFlag_Data ;
	uint8_t  FIFO_Length ;
	
	// Flush , Read FIFO_Length must be 0 , Check Interrupt Flag = LoAlert is set
	BC45_Flush_FIFO() ;
	FIFO_Length = BC45_Read_FIFOLength() ;
	BC45_SPI_readSingleRegister(InterruptRq, &InterruptFlag_Data);
	if ( (FIFO_Length == 0) && (InterruptFlag_Data == LoAlertIRQ) )
	{	
		// Write FIFO , Read FIFO_Length must be 1
		BC45_SPI_writeSingleRegister( FIFOData, 0xAA ) ;
		FIFO_Length = BC45_Read_FIFOLength() ;
		if ( FIFO_Length == 1 )
		{	
			// Read FIFO , Read FIFO_Length must be 0
			BC45_SPI_readSingleRegister(FIFOData, &FIFO_Data);
			FIFO_Length = BC45_Read_FIFOLength() ;
			if ( (FIFO_Data == 0xAA) && (FIFO_Length == 0) )
			{	
				// Enalble Interrupt --> IRQ pin must be '1'
				BC45_SPI_writeSingleRegister(InterruptEn,  (0x80 | LoAlertIRQ));
				if ( BC45_IRQ_PIN() == 1 )
				{
					// Disable Interrupt --> IRQ pin must be '0'
					BC45_Disable_And_ClearFlag_IRQ( LoAlertIRQ ) ;
					if ( BC45_IRQ_PIN() == 0 )
					{
						Status = _SUCCESS_ ;						
					}
					else // IRQ pin != '0'
					{
						Status = BC45_DIAGNOSE_ERR ;
					}
				}
				else // IRQ pin != '1'
				{
					Status = BC45_DIAGNOSE_ERR ;
				}
			}
			else // Data in FIFO != 0xAA or FIFO length != 0
			{
				Status = BC45_DIAGNOSE_ERR ;
			}
		}
		else // Write FIFO = 1 byte but FIFO length != 1
		{
			Status = BC45_DIAGNOSE_ERR ;
		}
	}
	else // FIFO_Length != 0 or LoAlertFlag not set
	{
		Status = BC45_DIAGNOSE_ERR ;
	}
	
	return Status ;
}

/*******************************************************************************
* Function Name : BC45_Get_Chip_Version
* Description   : Get the ASIC Versions
*				 
* Input         : None
* Output        : None
* Return        : 
*					- ASIC Version  
*                        
*******************************************************************************/
uint8_t BC45_Get_Chip_Version( void )
{
	//Crystal
	return 0;
}
