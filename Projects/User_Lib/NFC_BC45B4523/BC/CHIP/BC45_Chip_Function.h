/*****************************************************************************
* File Name          : BC45_Chip_Function.h
* Author             : CrystalSu	
* Version            : V1.0.0.0
* Date               : Feb 11,2020
* Description        : 
******************************************************************************/                       

#ifndef BC45_CHIP_FUNCTION_H
#define BC45_CHIP_FUNCTION_H

/********************************************************************************/
#include "BC45_Spi.h"
#include <stdint.h>

/* Define BC45 check timeout state */
#define BC45_TICK_TIMEOUT			0xFFFF 
#define BC45_TICK_INV_REQ_EN 	0xFFFE
#define BC45_TICK_TXRX_EN    	0xFFFD
#define BC45_TICK_TX_EN				0xFFFC
#define BC45_TICK_RX_EN				0xFFFB
#define BC45_TICK_TX_START		1000 //
#define BC45_TICK_TX_TIMEOUT	1500 //
#define BC45_TICK_RX_START		5000 //
#define BC45_TICK_RX_TIMEOUT	5500 //
#define BC45_TICK_PERIOD			1
/*Definition BC45 pins*/
/*RSTPD pin of BC45 which connected to MCU*/
#define RSTPD_CLR(PORTx, PINx)	GPIO_ClearOutBits(PORTx, PINx)
#define RSTPD_SET(PORTx, PINx)	GPIO_SetOutBits(PORTx, PINx)
/*IRQ pin of BC45 which connected to MCU*/
#define IRQ_PIN(PORTx, PINx)		GPIO_ReadInBit(PORTx, PINx)


/******************  Define BC45 Version *******************************/
#define BC45_RevA				0x10
#define BC45_RevB				0x20


//************ General Parameter (can edit or config) ******************//
#define	 MAX_ADDR				63
#define	 WaterLevel			16
#define  HiLevel				64 - WaterLevel

//************ Register Setting **************************//
#define	 PageReg				0x00

#define	 CommandReg			0x01
#define	 FIFOData				0x02
#define	 PrimStatus			0x03
#define	 FIFOLength			0x04
#define	 SecondStatus		0x05
#define	 InterruptEn		0x06
#define	 InterruptRq		0x07

#define	 ControlReg			0x09
#define	 ErrorReg				0x0A
#define	 CollPosReg			0x0B
#define	 TimerValReg		0x0C
#define	 CRCResultLSB		0x0D
#define	 CRCResultMSB		0x0E
#define	 BitFramingReg	0x0F

#define	 TxControl			0x11
#define	 CWConductance	0x12
#define	 ModConductance	0x13
#define	 CoderControl		0x14
#define	 ModWidth				0x15
#define	 ModWidthSOF		0x16
#define	 TypeBFraming		0x17

#define	 RxControl1			0x19
#define	 DecoderControl	0x1A
#define	 Bitphase				0x1B
#define	 RxThreshold 		0x1C
#define	 BPSKDemod			0x1D
#define	 RxControl2  		0x1E
#define	 RxControl3     0x1F

#define	 RxWait					0x21
#define	 CRCSetting			0x22
#define	 CRCPresetLSB 	0x23
#define	 CRCPresetMSB 	0x24

#define	 FIFOLevel			0x29
#define	 TimerClock			0x2A
#define	 TimerControl 	0x2B
#define	 TimerReload  	0x2C
#define	 ManFilterCtrl	0x2E
#define	 FilterCorCoef	0x2F

//modify by crystal on 2020/10/7
//#define	 IOConfig				0x31
#define	 IOConfig				0x39

#define	 SSI_FValue			0x37
#define	 Device_Type		0x38

#define	 TestAnaSel			0x3A
//#define	 TxDisableCtrl  0x3B
#define	 AnalogAdjust3	0x3C
//reserved
//#define	 ISO15693Header 0x3D
//#define	 Reg_Adj_Value  0x3D
//#define	 AnalogAdjust1	0x3E
#define	 AnalogAdjust2	0x3F


//************** Command for BC45 ***********************//
#define	 CMDIdle				0x00
#define	 CMDTransmit		0x1A
#define	 CMDReceive			0x16
#define	 CMDTranceive		0x1E
#define	 CMDCalCRC			0x12
#define	 CMDLoadKey			0x19
#define	 CMDAuthent 		0x1C

#define	 CMDLoadKeyE2		0x0B
#define	 CMDWriteE2			0x01
#define	 CMDReadE2			0x03
#define	 CMDLoadConfig	0x07

//******* Mask for Control Reg (Reg 0x09) ***************************//
#define	 Crypto1On_Mask				0x08
#define	 Crypto1On_Clear_Mask	0xF7
#define	 FlushFIFO_Set_Mask		0x01
#define	 TStartNow_Set_Mask		0x02
#define	 TStopNow_Set_Mask		0x04

//******* Mask for Err Reg (Reg 0x0A) *******************************//
#define	 CollErr_Mask				0x01
#define	 ParityErr_Mask			0x02
#define	 FramingErr_Mask		0x04
#define	 CRCErr_Mask				0x08
#define	 FIFOOvfl_Mask			0x10
#define	 AccessErr_Mask			0x20
#define	 KeyErr_Mask				0x40
#define	 E2Err_Mask					0x80

//******* Mask for TxControl Reg (Reg 0x11) *************************//
#define	 ModSource_Clear_Mask	0x9F
#define	 Force100ASK_Set_Mask	0x10
#define	 Force100ASK_Clear_Mask	0xEF
#define	 TX2RFEn_Set_Mask			0x02
#define	 TX2RFEn_Clear_Mask		0xFD
#define	 TX1RFEn_Set_Mask			0x01
#define	 TX1RFEn_Clear_Mask		0xFE
#define	 TX2Inv_Clear_Mask		0xF7
#define	 TX2Cw_Set_Mask				0x04
#define	 TX2Cw_Clear_Mask			0xFB

//******* Mask for CoderControl Reg (Reg 0x14) **********************//
#define	 Send1Pulse_Set_Mask		0x80
#define	 Send1Pulse_Clear_Mask	0x7F

//******* Mask for TypeBFraming Reg (Reg 0x17) **********************//
#define	 EOFWidth_Clear_Mask			0xDF
#define	 SOFWidth_Clear_Mask			0xFC
#define	 EOF_10etu_Set_Mask				0x00
#define	 EOF_11etu_Set_Mask				0x20
#define	 SOF_10etu_Add_2etu_Set_Mask	0x00
#define	 SOF_10etu_Add_3etu_Set_Mask	0x01
#define	 SOF_11etu_Add_2etu_Set_Mask	0x02
#define	 SOF_11etu_Add_3etu_Set_Mask	0x03

//******* Mask for Decoder Control Reg (Reg 0x1A) *******************//
#define	 CollMaskVal_Mask	 			0x40
#define	 CollMaskVal_Set_Mask		0x40
#define	 CollMaskVal_Clear_Mask	0xBF

//******* Mask for Decoder Control Reg (Reg 0x1A) *******************//
#define	 ZeroAfterColl_Mask	 			0x20
#define	 ZeroAfterColl_Set_Mask	  0x20
#define	 ZeroAfterColl_Clear_Mask	0xDF

//******* Mask for Decoder Control Reg (Reg 0x1A) *******************//
#define	 RxMultiple_Mask					0x80
#define	 RxMultiple_Set_Mask	    0x80
#define	 RxMultiple_Clear_Mask	  0x7F

//******* Mask for CRCSetting Reg (Reg 0x22) ************************//
#define	 TxCRCEn_Set_Mask					0x04
#define	 TxCRCEn_Clear_Mask				0xFB
#define	 RxCRCEn_Set_Mask					0x08
#define	 RxCRCEn_Clear_Mask				0xF7

//******* Mask for TimerClock Reg (Reg 0x2A) ************************//
#define	 TPreScaler_Mask					0x1F
#define	 TPreScaler_Clear_Mask		0xE0

//******* Crypto Status **********************************//
#define	 CRYPTO_NOT_ON			      0x00
#define	 CRYPTO_ON								0x01
#define	 CHECK_DISABLE						0x00
#define	 CHECK_ENABLE			        0x01

//************* Interrupt Value for BC45 ****************//
#define	 CLEAR_ALL_IRQ_SOURCE		0x7F
#define	 ALL_IRQ_SOURCE					0x7F
#define	 TimerIRQ								0x20
#define	 TxIRQ									0x10
#define	 RxIRQ									0x08
#define	 IdleIRQ								0x04
#define	 HiAlertIRQ							0x02
#define	 LoAlertIRQ							0x01

//************* Timer Setting for BC45 ****************//
#define	 TimerManual		0x00

#define	 TStartTxBegin	0x01
#define	 TStartTxEnd		0x02

#define	 TStopRxBegin		0x01
#define	 TStopRxEnd			0x02

//************* ModSource for BC45 ****************//
#define	 ModSource_TriState	0x00
#define	 ModSource_High			0x01
#define	 ModSource_Internal	0x02
#define	 ModSource_SIGIN0		0x03

//************* CRC Setting for BC45 ****************//
#define	 TxCRC_Disable		0x00
#define	 TxCRC_Enable			0x01
#define	 RxCRC_Disable		0x00
#define	 RxCRC_Enable			0x01

//************* EOF SOF Selection *********************//
#define  SOF_10etu_Add_2etu		0x00  
#define  SOF_10etu_Add_3etu		0x01
#define  SOF_11etu_Add_2etu		0x02
#define  SOF_11etu_Add_3etu		0x03

#define  EOF_10etu	0x00
#define  EOF_11etu	0x01

//************* Speed for each Standard ****************//
//--For ISO14443A, ISO14443B-------//
// Speed Tx and Rx
#define	 SPEED_106_KBPS			0x00
#define	 SPEED_212_KBPS			0x01
#define	 SPEED_424_KBPS			0x02
#define	 SPEED_848_KBPS			0x03

//---- For ISO15693----------------//
// Speed Tx
#define	 SPEED_1_OUT_OF_256		0x00
#define	 SPEED_1_OUT_OF_4			0x01
// Speed Rx
#define	 SPEED_1_SUB_LOW			0x00
#define	 SPEED_1_SUB_HIGH			0x01
#define	 SPEED_1_SUB_ULTRA_HIGH	0x02
#define	 SPEED_2_SUB_LOW			0x03
#define	 SPEED_2_SUB_HIGH			0x04

//---- Move from BC45_Board_Define.h --//
#define DATA_RF_BUFFER_SIZE			512 
#define DATA_BUFFER_SIZE				512 
#define PACKAGE_BUFFER_SIZE			512 + 9 // (Header(1 byte) + Len_H(1) + Len_L(1) + Sequence_Num(1) + Dev_ID(1) + Cmd_Category(1) + Sub_Cmd(1) + Resp(1) + Data(512) + LRC(1))
#define BC45BOARD_BUFFER_SIZE		512

#define	_SUCCESS_					0x01 // Operation success


#define DRIVER_CONFIG_X_CC				0x00 // Diffential Close Coupling Network with internal envelope
#define DRIVER_CONFIG_X_CCXENV		0x01 // Diffential Close Coupling Network with external envelope
#define DRIVER_CONFIG_X_S50OUT		0x02 // Single-ended Drive with external envelope
#define DRIVER_CONFIG_X_E50OUT		0x03 // 50 ohm output from Class-E driver with external envelope 


// --- RF Communication Err ---//
#define NO_RESPONSE				0xE0 // No card response within given time indicating by timeout from ASIC Timer
#define FRAMING_ERR				0xE1 // Format of receive frame errors indicating by FramingErr bit in BC45's ErrorFlag register (Reg 0x0A)										
#define COLLISION_ERR			0xE2 // Bit collision is detected indicating by CollErr bit in BC45's ErrorFlag register (Reg 0x0A)											
#define PARITY_ERR				0xE3 // Parity Bit Check is invalid indicating by ParityErr bit in BC45's ErrorFlag register (Reg 0x0A)											
#define CRC_ERR						0xE4 // CRC Check is invalid indicating by CRCErr bit in BC45's ErrorFlag register (Reg 0x0A)											
#define INVALID_RESP			0xE5 // Response is invalid or unexpected from operation protocol
#define SUBC_DET_ERR    	0xE6 // Subcarrier from card is detected indicating by SubC_Det bit in SIC9410's Status register (Reg 0x05)	
                                                     // but can not recoginzed following standard (available only BC45B4523)
//-- Reader Chip(BC45) System Err --//
#define BUFFER_OVERFLOW_ERR	0xF0 // SIC9xx's FIFO overflow indicating by FIFOOvlf bit in BC45's ErrorFlag register (Reg 0x0A)	
#define ACCESS_E2_ERR				0xF1 // Accessing EEPROM error indicating by AccessErr bit in BC45's ErrorFlag register (Reg 0x0A)	
#define WRITE_E2_ERR				0xF2 // Writing EEPROM error indicating by E2Err bit in BC45's ErrorFlag register (Reg 0x0A)	
#define KEY_ERR							0xF3 // Loaded Key is in invalid format indicating by KeyErr bit in BC45's ErrorFlag register (Reg 0x0A)	 
#define ASIC_EXE_TIMEOUT		0xF4 // No ASIC response within given time indicating by timeout from MCU Timer
#define BC45_DIAGNOSE_ERR	0xF5 

// ISO14443A Err
#define A_HALT_ERR					0xA0 // Error if there is a response after sending Halt command 
#define AUTHENT_ERR					0xA1 // Error if Crypto1 bit in Control register(Reg 0x09) is not set after performing AUTHENT command 
#define NOT_AUTHENT					0xA2 // Error from Operating MIFARE command i.e. Increment when crypto1 bit is not set
#define MIFARE_ERR					0xA3 // NACK (0x04 or 0x05) from MIFARE card is received


//-- Structure from Global.h --//
typedef struct struct_tBC45_BOARD_PROPERTY
{
	uint8_t   Device_ID ;		
	uint8_t   Board_Interface ;		
	uint8_t   Driver_Config_Type ;		
	uint16_t  ASIC_Exe_Timeout_Val ;		
	uint8_t   Continuous_Run ;		
	uint16_t  Delay_Run_ms ;		
	
} tBC45_BOARD_PROPERTY ;

typedef struct struct_tStandard_Param
{
	uint8_t   GsCfgMod ;	
  uint8_t   BitPhase ;	
	uint8_t   Rx_Threshold ;  	
	uint8_t   BPSK_Demod ;
	uint8_t   RxControl_3 ;
	uint8_t   AnaAdjust1 ;
	uint8_t   AnaAdjust2 ;
  uint8_t   AnaAdjust3 ;
} tStandard_Param ;

//-- Variables from Global.h --//
extern tBC45_BOARD_PROPERTY  BC45_BOARD_PROPERTY ;

extern tStandard_Param   Param_14443A_106 ;
extern tStandard_Param   Param_14443A_212 ;
extern tStandard_Param   Param_14443A_424 ;
extern tStandard_Param   Param_14443A_848 ;
extern tStandard_Param   Param_14443B_106 ;
extern tStandard_Param   Param_14443B_212 ;
extern tStandard_Param   Param_14443B_424 ;
extern tStandard_Param   Param_14443B_848 ;
extern tStandard_Param   Param_15693_1sub_lo ;
extern tStandard_Param   Param_15693_1sub_hi ;
extern tStandard_Param   Param_15693_1sub_uhi ;
extern tStandard_Param   Param_15693_2sub_lo ;
extern tStandard_Param   Param_15693_2sub_hi ;

extern uint8_t	 Data_TxRF[DATA_RF_BUFFER_SIZE] ;
extern uint8_t   Data_RxRF[DATA_RF_BUFFER_SIZE] ;
extern uint16_t  LenData_TxRF ;
extern uint16_t  LenData_RxRF ;


////////////////////// Generally Command For Using Reader IC ////////////////////////////////
void BC45_RSTPD_SET(void);
void BC45_RSTPD_CLR(void);
void BC45_Chip_Reset( void ) ;
uint8_t BC45_IRQ_PIN(void);
	
uint8_t BC45_Read_FIFOLength( void ) ;
uint8_t BC45_Read_CollPos( void )  ;
uint8_t BC45_Read_RxLastBit( void )  ;
uint8_t BC45_ReadErrReg( void ) ;

void BC45_WriteFIFO( uint8_t *Data_Wr, uint16_t LenData_Wr ) ; 
void BC45_WriteCMD ( uint8_t CMD ) ;

uint8_t BC45_Check_RFErr( void ) ;
uint8_t BC45_Check_E2Err( void ) ;
uint8_t BC45_Check_KeyErr( void ) ;
uint8_t BC45_Check_Crypto_Bit( void ) ;
void BC45_Set_Send1Pulse_Bit( void ) ;
void BC45_Clear_Send1Pulse_Bit( void ) ;
void BC45_Clear_Crypto1On_Bit( void ) ;
void BC45_Disable_And_ClearFlag_IRQ( uint8_t IRQ_Source ) ;
void BC45_CRC_Setting( uint8_t TxCRC, uint8_t RxCRC ) ;
void BC45_Set_BitFraming( uint8_t RxAlign, uint8_t TxLastBit ) ;
void BC45_Set_CollMaskVal_Bit( void ) ;
void BC45_Clear_CollMaskVal_Bit( void ) ;
void BC45_Set_ModulatorSource( uint8_t ModulatorSource ) ;
void BC45_Set_Force100ASK_Bit( void ) ; 
void BC45_Clear_Force100ASK_Bit( void ) ; 
void BC45_Flush_FIFO( void ) ;
void BC45_Clear_IRQ( void ) ;
void BC45_Clear_State_When_TimeOut( void ) ;

void BC45_Timer_Set_Resp_Waiting_Time( uint8_t Time ) ;
void BC45_Timer_Tstart_Tstop_Control( uint8_t TStartWhen, uint8_t TStopWhen ) ;
void BC45_Start_Timer( void ) ;
void BC45_Stop_Timer ( void ) ;
void BC45_SOFEOF_Selection( uint8_t SOF_EOF_Sel ) ;
void BC45_Set_RxMultiple_Bit( void ) ;
void BC45_Clear_RxMultiple_Bit( void ) ;

uint8_t BC45_IRQ_Execute_FIFO_Data(uint8_t  CMD, uint8_t  *Data_Tx, uint16_t  LenData_Tx, uint16_t *Tick) ;
uint8_t BC45_IRQ_Read_FIFO(uint8_t  *Data_Rx, uint16_t  *LenData_Rx, uint16_t *Tick) ;
uint8_t BC45_IRQ_Read_FIFO_TimeSlot( uint8_t  *Data_Rx, uint16_t  *LenData_Rx, uint16_t  TimeOut_MCU_ms );
uint8_t BC45_Transmit( uint8_t  *Data_Tx ,uint16_t  LenData_Tx ) ;
uint8_t BC45_Receive( uint8_t  *Data_Rx ,uint16_t  *LenData_Rx ) ;
uint8_t BC45_Receive_With_RxMultiple ( uint8_t  *Data_Rx ,uint16_t  *LenData_Rx ) ;
uint8_t BC45_Receive_TimeSlot( uint8_t  *Data_Rx ,uint16_t  *LenData_Rx ) ;
uint8_t BC45_Transceive( uint8_t  *Data_Tx ,uint16_t  LenData_Tx, uint8_t  *Data_Rx ,uint16_t  *LenData_Rx, uint16_t *Tick ) ;
uint8_t BC45_Transceive_With_RxMultiple ( uint8_t  *Data_Tx ,uint16_t  LenData_Tx, uint8_t  *Data_Rx ,uint16_t  *LenData_Rx ) ;
uint8_t BC45_Transceive_NFC_Type1( uint8_t  *Data_Tx ,uint16_t  LenData_Tx, uint8_t  *Data_Rx ,uint16_t  *LenData_Rx, uint8_t RxCRCEn );
uint8_t BC45_ReadE2( uint8_t StartAddr, uint8_t LenData_Rd_E2, uint8_t *Data_E2, uint16_t *LenData_E2 ) ;
uint8_t BC45_WriteE2( uint8_t StartAddr, uint8_t *Data_E2, uint16_t LenData_Wr_E2 ) ;
uint8_t BC45_LoadE2Config( uint8_t Star_Addr ) ;
uint8_t BC45_LoadKey( uint8_t *Key, uint8_t LenKey ) ;
uint8_t BC45_LoadKeyE2( uint8_t Star_Addr ) ;
uint8_t BC45_Cal_CRC( uint8_t *Data_Cal, uint16_t LenData_Cal, uint8_t *CRC_Result, uint16_t *LenCRC_Result ) ;
uint8_t BC45_Authent( uint8_t *Data_Authent, uint8_t LenData_Authent ) ;

uint8_t Transparent_With_CRC( uint8_t *Data_Tx, uint16_t LenData_Tx, uint8_t TimeOut, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t Transparent_Without_CRC( uint8_t *Data_Tx, uint16_t LenData_Tx, uint8_t TimeOut, uint8_t *Data_Resp, uint16_t *LenData_Resp ) ;
uint8_t Transparent_Configure_CRC( uint8_t TxCRCEn, uint8_t RxCRCEn ,uint8_t *Data_Tx, uint16_t LenData_Tx, uint8_t TimeOut, uint8_t *Data_Resp, uint16_t *LenData_Resp );
uint8_t Transparent_With_RxMultiple( uint8_t *Data_Tx, uint16_t LenData_Tx, uint8_t TimeOut, uint8_t *Data_Resp, uint16_t *LenData_Resp );

uint8_t Transparent_NFC_Type1_With_CRC( uint8_t *Data_Tx, uint16_t LenData_Tx, uint8_t TimeOut, uint8_t *Data_Resp, uint16_t *LenData_Resp );
uint8_t Transparent_NFC_Type1_Without_CRC( uint8_t *Data_Tx, uint16_t LenData_Tx, uint8_t TimeOut, uint8_t *Data_Resp, uint16_t *LenData_Resp );
uint8_t Transparent_NFC_Type1_Config_CRC( uint8_t TxCRCEn, uint8_t RxCRCEn, uint8_t *Data_Tx, uint16_t LenData_Tx, uint8_t TimeOut, uint8_t *Data_Resp, uint16_t *LenData_Resp );

void BC45_ON_RF( uint8_t  Driver_Config_Type ) ;
void BC45_OFF_RF( uint8_t  Driver_Config_Type ) ;
void BC45_Config_Driver( uint8_t  Driver_Config_Type ) ;	
void BC45_InitChip( uint8_t  Driver_Config_Type )	; 

//---------- Config For ISO14443A -----------------//
void BC45_Config_14443A( void ) ;
void BC45_Speed_14443A ( uint8_t SPD_SEL ) ;

//---------- Config For ISO14443B -----------------//
void BC45_Config_14443B( void ) ;
void BC45_Speed_14443B ( uint8_t SPD_SEL ) ;

//---------- Config For ISO15693 -----------------//
void BC45_Config_15693( void ) ;
void BC45_Speed_15693 ( uint8_t SPD_SEL ) ;

uint8_t Initial_BC45_Board( void ) ;
uint8_t BC45_Diagnose( void ) ;
uint8_t BC45_Get_Chip_Version( void ) ;
#endif
