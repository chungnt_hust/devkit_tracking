/*****************************************************************************
* File Name          : BC45_Board_Define.h
* Author             : CrystalSu
* Version            : V1.0.0.0
* Date               : Feb 12,2020
* Description        : 
******************************************************************************/   

#ifndef BC45_BOARD_DEFINE_H
#define BC45_BOARD_DEFINE_H

#include "BC45_Chip_Function.h"

#define MAX_DEVICE_ID  				127 

//-------------------------------------------------------------------------//
//------------------------ Define BC45 Booard ---------------------------//
//-------------------------------------------------------------------------//

#define PACKAGE_BUFFER_SIZE			512 + 9 // (Header(1 byte) + Len_H(1) + Len_L(1) + Sequence_Num(1) + Dev_ID(1) + Cmd_Category(1) + Sub_Cmd(1) + Resp(1) + Data(512) + LRC(1))


// Define Type of Host - BC45 Board Interface 
#define USB_IF  			0x01 
#define UART_IF  			0x02 

// Define Type of Product
#define  CUSTOMER  			0x00 
#define  FW_DETAIL     		CUSTOMER

//-------------------------------------------------------------------------//
//-------------------------------------------------------------------------//



//-------------------------------------------------------------------------//
//----------------------- Define FBP Response -----------------------------//
//-------------------------------------------------------------------------//

#define RESP_CATEGORY_MASK			0xF0
#define RF_COMMU_ERR_CATEGORY		0xE0


// Board IF Err
#define TIMEOUT_RECEIVE				0x10 // No input data is receive within given time. time defined
#define LRC_ERR								0x11 // Verification of input Package Checksum is failed 
#define RX_BUFFER_FULL				0x12 // interface buffer is already full.

// Communication Protocol Err
#define UNKNOWN_CMD_TYPE			0x20 // Input command category is undefined
#define UNKNOWN_CMD						0x21 // Input command is undefined
#define PARAMETER_NOT_CORRECT	0x22 // Parameter is incomplete or invalid

// Board Function Err
#define WRITE_HARDWARE_PARAM_FAIL	      0x30 // Writing Hardware Parameter fail
#define CHECKSUM_HARDWARE_PARAM_FAIL		0x31 // Checksum Hardware Parameter Fail
#define HARDWARE_PARAM_NOT_CORRECT	    0x32 // 

// ISO14443B Err

#define RESERVED_RESP_CODE_ERR		0xC0 	// Response code mismatched

// ISO15693 Err
#define FLAG_ERR					0xD0 					// Bit Error flag in ISO15693 response is set

//-------------------------------------------------------------------------//
//-------------------------------------------------------------------------//







//-------------------------------------------------------------------------//
//-------------------------------------------------------------------------//
//																		   //	
//------------------------ All List Command -------------------------------//
//																		   //	
//-------------------------------------------------------------------------//
//-------------------------------------------------------------------------//

// Define Category
#define CMD_CATEGORY_MASK			0xF0

#define CONFIG_READER_CMD_CATEGORY	0x00

#define STANDARD_CMD_CATEGORY1		0x10
#define STANDARD_CMD_CATEGORY2		0x20

#define CUSTOM_CMD_CATEGORY1		0x30
#define CUSTOM_CMD_CATEGORY2		0x40
#define CUSTOM_CMD_CATEGORY3		0x50
#define CUSTOM_CMD_CATEGORY4		0x60
#define CUSTOM_CMD_CATEGORY5		0x70
#define CUSTOM_CMD_CATEGORY6		0x80
#define CUSTOM_CMD_CATEGORY7		0x90

#define COMBO_CMD_CATEGORY1			0xA0	
#define COMBO_CMD_CATEGORY2			0xB0

#define SPECIAL_CMD_CATEGORY1		0xC0
#define SPECIAL_CMD_CATEGORY2		0xD0

#define PROPRIETARY_CMD_CATEGORY1	0xE0
#define PROPRIETARY_CMD_CATEGORY2	0xF0


//-------------------------------------------------------------------------//
//------------------------ BC45 Board Command ---------------------------//
//-------------------------------------------------------------------------//

// BC45_Board Cmd
#define BC45_BOARD_CMD_TYPE		0x00

#define Get_Device_ID					0x00
#define Write_Device_ID				0x01
#define Get_Last_Response			0x02												
#define Test_Communication		0x03
#define Get_Version						0x04

#define Get_Driver_Config			0x40
#define Write_Driver_Config		0x41

// Proprietary Cmd
#define Get_Handle_RFData_Fail			0xE0												
#define Handle_RFData_Fail					0xE1												
#define Get_Last_Response_All		    0xE2												
#define Get_ASIC_Exe_Timeout_Val	  0xE3	
#define Write_ASIC_Exe_Timeout_Val	0xE4												
#define Get_Board_Description		    0xE5
#define Enable_Continuous_Run		    0xE6												
#define Disable_Continuous_Run		  0xE7												
#define Set_Delay_Time_ms						0xE8												
#define Get_Delay_Time_ms						0xE9	
#define On_Magnetic_Buzzer		      0xEA	


//-------------------------------------------------------------------------//
//-------------------------------------------------------------------------//




//-------------------------------------------------------------------------//
//---------------------- BC45 Board ASIC Command ------------------------//
//-------------------------------------------------------------------------//

// BC45_Board_ASIC Cmd
#define BC45_BOARD_ASIC_CMD_TYPE	0x01

#define Reset_BC45								0x20
#define Read_Reg_BC45							0x21
#define Write_Reg_BC45						0x22									
#define Read_Mutiple_Reg_BC45		  0x23												
#define Write_Mutiple_Reg_BC45	  0x24												
//#define Read_E2_BC45							0x25												
//#define Write_E2_BC45							0x26												
//#define Load_E2_Config_BC45		    0x27												
#define Cal_CRC										0x28												

#define ON_Field_BC45				0x30
#define OFF_Field_BC45			0x31

#define Set_TimeOut_Wait_Tag_Resp_BC45	0x42

#define BC45_Diagnose_Cmd								0xF0		
#define BC45_Init_Standard_Ana_Param		0xF1
#define BC45_Set_Standard_Ana_Param	    0xF2
#define BC45_Get_Standard_Ana_Param 		0xF3
#define BC45_Get_ASIC_Version 					0xF4

/// HTK Proprietary & Test CMD ///////
#define Write_Read_Reg_Check	            0xE0
#define Reset_BC45_Without_Init	      	0xE1

//-------------------------------------------------------------------------//
//-------------------------------------------------------------------------//

//-------------------------------------------------------------------------//
//-------------------------- Standard Define ------------------------------//
//-------------------------------------------------------------------------//
#define Standard_ISO14443A			0x0A
#define Standard_ISO14443B			0x0B
#define Standard_Reserved		    0x0C
#define Standard_ISO15693				0x0D





#endif
