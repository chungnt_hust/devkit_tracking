/*****************************************************************************
* File Name          : bc45_cli.c
* Author             : CrystalSu
* Version            : V1.0.0.0
* Date               : Feb 12, 2020
* Description        : This file can be modified for using with any project.
******************************************************************************/

#include <string.h>

#include "BC45_Cli.h"
#include "BC45_Function.h"
#include "BC45_Chip_Function.h"
#include "BC45_Board_ASIC_Category.h"
#include "BC45_Spi.h"

/*Constant register address of BC45 on page 0*/
const uint8_t reg_sec0[57] =
{
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x11,
	0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x19, 0x1A,
	0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 
	0x23, 0x24, 0x25, 0x26, 0x27, 0x29, 0x2A, 0x2B, 
	0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33, 
	0x34, 0x35, 0x36, 0x37, 0x39, 0x3A, 0x3C, 0x3E, 
	0x3F
};

/*Constant register address of BC45 on page 1*/
const uint8_t reg_sec1[19] =
{
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x08, 0x09,
	0x0B, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13,
	0x2E, 0x2F, 0x37
};

tISO_TagTypeDef tag_type;

tBC45_WAKEUP_TIMER_PROPERTY Timer_WKUP;
tBC45_CD_TUNING Tunning_Thereshold;

uint8_t Data_Resp[64];
uint16_t Len_Resp = 0;

//uint8_t ISO15693_Speed = 0x11;//Default is 0x00, 0x11 for 26.48 Kbps
nfc_config_t nfca0;
nfc_config_t nfcb0;
nfc_config_t nfca1;
nfc_config_t nfcb1;

/* Global functions ----------------------------------------------------------------------------------------*/
/*
 * @brief Configure BC45 tag type communication
 * @param tagtype: Tag type definition
 *                 - CONFIG_14443A
 *                 - CONFIG_14443B
 *                 - CONFIG_15693
 * @retval None
 * */
void BC45_Configuration(uint8_t tagtype)
{
	uint8_t Resp = 0;
	//add by crystal
	//BC45_CDMode_Exit();
	//BC45_Board_ASIC_Command(Reset_BC45, NULL, 0, &Data_Resp[0], &Len_Resp);
	
  /*Re-Init BC45 reader and turn on RF field*/
  if(Initial_BC45_Board() != _SUCCESS_)
	{
		DEBUG("[BC45] Initial Board Failed\r\n");
		while(1);
	}
	
  //BC45_Board_ASIC_Command(OFF_Field_BC45, NULL, 0, &Data_Resp[0], &Len_Resp);
	BC45_ON_RF(BC45_BOARD_PROPERTY.Driver_Config_Type);
  HAL_Delay(50);
	BC45_ON_RF(BC45_BOARD_PROPERTY.Driver_Config_Type);
  //BC45_Board_ASIC_Command(ON_Field_BC45, NULL, 0, &Data_Resp[0], &Len_Resp);

  /*Configure tag type communication*/
  switch(tagtype)
  {
    case CONFIG_15693:
      Resp = ISO15693_Config(ISO15693_Speed);
      if(Resp != _SUCCESS_)
      {
    	  DEBUG("[BC45] ISO15693 Configuration failed\r\n");
      }
      else
      {
    	  DEBUG("[BC45] ISO15693 Configuration success\r\n");
      }
      break;
    default:
      Resp = ERROR;
      DEBUG("[BC45] Configuration failed");
      break;
  }
}

/*
 * @brief Scan UID of an ISO15693 tag type
 * @retval Response from scanning
 *         - _SUCCESS_
 *         - NO_RESPONSE
 *         - ASIC_EXE_TIMEOUT
 *         - ERROR
 * */
static uint8_t iso15693_uid[12] = {0};
static char UID_Out[16];
static uint16_t _BC45_Tick = BC45_TICK_INV_REQ_EN;
uint8_t ScanUID_ISO15693TagType(char *UID)
{
	uint8_t Resp = 0;
	uint8_t response = 0;
	uint8_t mask_len;
	uint8_t mask_value;
	uint8_t num_uid;
	uint8_t num_data;

	UID[0] = 0;
  mask_len = 0x00;
  mask_value = NULL;
  /*Send inventory 1 slot command*/
  Resp = ISO15693_Inv_Req_1_Slot(ISO15693_Speed, 0x00, 0x00,
		  	  mask_len, &mask_value, Data_Resp,  &Len_Resp, &_BC45_Tick);

  /*Check response from the inventory 1 slot command*/
  if((Resp == _SUCCESS_) && (_BC45_Tick != BC45_TICK_TIMEOUT))
  {
		_BC45_Tick = BC45_TICK_INV_REQ_EN;
  	//DEBUG("[BC45] Success\r\n");

    /*Show number of tag (Should be 1)*/
    //byteToChar(&numTags, Data_Trans);
   	/*Clear num_data variable for counting on Data_Resp buffer*/
    num_data = 0;
    /*Store ISO15693 tag's UID*/
		for(num_uid = 0; num_uid < 12; num_uid++)
		{
		  //DEBUG("%02x", Data_Resp[num_data]);
			iso15693_uid[num_uid] = Data_Resp[num_data];
			num_data++;
		}
		BC45_ConvertUID2Char(&iso15693_uid[0], &UID_Out[0]);
		memcpy(UID, UID_Out, 16);
    response = _SUCCESS_;
  }
  else
  {
		if(_BC45_Tick == BC45_TICK_TIMEOUT) _BC45_Tick = BC45_TICK_INV_REQ_EN;
		switch(Resp)
		{
			case NO_RESPONSE:
//				DEBUG("No Response\r\n");
				response = NO_RESPONSE;
				break;
			case ASIC_EXE_TIMEOUT:
				//DEBUG("[BC45] Timeout\r\n");
				response = ASIC_EXE_TIMEOUT;
				break;
//			default:
//				DEBUG("Receive Error\r\n");
//				response = ERROR;
//				break;
		}
  }

  return response;
}

/*
 * @brief Turn on/off RF of BC45
 * @param  rf_field : RF Field definition
 * 					- RFON
 * 					- RFOFF
 * 					- RFRESET
 * @retval None
 * */
void BC45_RF_OnOff(uint8_t rf_field)
{
	switch(rf_field)
	{
//		case RFON:
//			BC45_Board_ASIC_Command(ON_Field_BC45, NULL, 0, &Data_Resp[0], &Len_Resp);
//			break;
//		case RFOFF:
//			BC45_Board_ASIC_Command(OFF_Field_BC45, NULL, 0, &Data_Resp[0], &Len_Resp);
//			break;
//		default:
//			//Initial_BC45_Board();
//			BC45_Board_ASIC_Command(OFF_Field_BC45, NULL, 0, &Data_Resp[0], &Len_Resp);
//			HAL_Delay(6);
//			BC45_Board_ASIC_Command(ON_Field_BC45, NULL, 0, &Data_Resp[0], &Len_Resp);
//			break;

	}
}

void BC45_InitISO15693(void)
{
	Spi_Configuration(BC45_INDEX, SPI_DATALENGTH_8, 0);
	
	/* Configuration of tag type communication */	
	BC45_Configuration(CONFIG_15693);
	tag_type = ISO15693_TagType;
	
	//Before enabling the card detection function of the BC45B4523, calibrate Card ditection (CD) 
  //to implement RF calibration without placing any tag above the reader.

  /* Configure CD threshold to default*/
  Tunning_Thereshold.CD_Threshold_I_L = 0xD6;
  Tunning_Thereshold.CD_Threshold_I_H = 0xE0;
  Tunning_Thereshold.CD_Threshold_Q_L = 0xF5;
  Tunning_Thereshold.CD_Threshold_Q_H = 0xFF;
  CD_ADC_Tuning(Tunning_Thereshold);
	
	/*
   * Configure Wake up timer period to default
   * which is 500 ms. (reload is 0x10, prescaler is 0x09)
   * */
  Timer_WKUP.Timer_COUNTER = 0x0A;
  Timer_WKUP.Timer_PRESCALER = 0x09;
  Timer_WKUP.Timer_RELOAD = 0x10;
  TimerWKUP_INIT(Timer_WKUP);
}

/* Convert 8byte of UID to char */
void BC45_ConvertUID2Char(uint8_t *inputByte, char *outputChar)
{
	uint8_t temp_dataH;
	uint8_t temp_dataL;
	uint8_t i, j = 0;
	
	for(i = 1; i < 9; i++)
	{
		/*Seperate 4 bits of data (first 4 bits and last 4 bits)*/
		temp_dataH = ((inputByte[i]) & 0xF0) >> 4;
		temp_dataL = ((inputByte[i]) & 0x0F);

		/*Check 4 bits high*/
		if(temp_dataH <= 0x09)
		{
				temp_dataH = temp_dataH + '0';
		}
		else
		{
				temp_dataH = (temp_dataH - 0x0A) + 'A';
		}

		/*Check 4 bits low*/
		if(temp_dataL <= 0x09)
		{
				temp_dataL = temp_dataL + '0';
		}
		else
		{
				temp_dataL = (temp_dataL - 0x0A) + 'A';
		}

		/*Assign to output buffer*/
		outputChar[j++] = temp_dataH;
		outputChar[j++] = temp_dataL;
	}
}
