/*
 * BC45_Spi.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/

#ifndef HTK_CLI_H_
#define HTK_CLI_H_

#include "All_User_Lib.h"

#define BC45_SPI_PORT 				QSPI_PORT
#define BC45_INDEX		 				QSPI_INDEX
#define BC45_SEL_ACTIVE 			QSPI_SEL_ACTIVE
#define BC45_SEL_INACTIVE 		QSPI_SEL_INACTIVE

#define BC45_IRQ_GPIO_Port 		NFC_INT_PORT
#define BC45_IRQ_Pin 					NFC_INT_PIN
#define BC45_IRQ_EXTI_IRQn 		NFC_INT_EXTI_IRQn

#define BC45_RSTPD_GPIO_Port 	NFC_RST_PORT
#define BC45_RSTPD_Pin 				NFC_RST_PIN

void BC45_SPI_writeSingleRegister(uint8_t address, uint8_t data);
void BC45_SPI_readSingleRegister(uint8_t address, uint8_t *data);
void BC45_SPI_writeMultiData(uint8_t address, uint8_t *data, uint16_t inputLen);
void BC45_SPI_readMultiData(uint8_t address, uint8_t *data, uint16_t outputLen);
void BC45_SPI_Init(HT_SPI_TypeDef *SPI, HT_GPIO_TypeDef *NCS_Port, uint16_t NCS_Pin);

#endif /* HTK_CLI_H_ */
