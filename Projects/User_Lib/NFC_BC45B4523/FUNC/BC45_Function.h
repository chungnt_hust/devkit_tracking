/*****************************************************************************
* File Name          : BC45_Function.h
* Author             : CrystalSu
* Version            : V1.0.0.0
* Date               : Feb 12, 2020
* Description        : Use for test BC45 with Card Detection Feature
******************************************************************************/  

#ifndef BC45_TEST_FUNCTION_H
#define BC45_TEST_FUNCTION_H

#include "ht32.h"
#include <stdio.h>

/* Register for BC45 Card Detection*/
#define SELECTPAGE					0x00
#define CMDPAGE							0x01
#define InterruptEN         0x06
#define InterruptIRQ        0x07
#define SYSTEM_CONTROL			0x09
#define ADC_RESULT_I        0x26
#define ADC_RESULT_Q        0x27
#define WAKEUP_TIME_CONTROL	0x2D
#define WAKEUP_TIME_RELOAD	0x2E
#define WAKEUP_TIME_COUNTER 0x2F
#define WAKEUP_CD_CONTROL		0x31
#define FD_THRES_I_HIGH     0x32
#define FD_THRES_Q_HIGH     0x33
#define CD_THRES_I_LOW      0x34
#define CD_THRES_I_HIGH     0x35
#define CD_THRES_Q_LOW      0x36
#define CD_THRES_Q_HIGH     0x37
/*********************************/

/* Mask for Interrupt bit mask (ADDR 0x06, 0x07)*/
#define SetIrq_Mask      		0x80
#define SetIEN_Mask      		0x80
#define CDIRQ_Mask          0x40
#define TimerIRQ_Mask       0x20
#define TxIRQ_Mask          0x10
#define RxIRQ_Mask          0x08
#define IdleIRQ_Mask        0x04
#define HiAlertIRQ_Mask     0x02
#define LoAlertIRQ_Mask     0x01
/*********************************/

/* Mask for System Control (ADDR 0x09) */
#define BC45_WKUP_CD_MODE    0x40
#define BC45_IDLE_CD_MODE  	 0x41
#define BC45_STANDBY_MODE    0x20
#define BC45_PWR_DOWN_MODE   0x10
#define BC45_CLEAR_ALL_MODE  0x8F
#define BC45_CHECK_ALL_MODE  0x60
/*********************************/

/* Mask for WKUP TIME Control (ADDR 0x2D)*/
#define WKUP_TSTART_NOW		    0x80
#define WKUP_TSTOP_NOW		    0x40
#define WKUP_TRUNNING		    	0x20
#define WKUP_TAUTO_RELOAD     0x10 
#define WKUP_TIMER_PRESCALER  0x0F
#define WKUP_TIMER_COUNTER    WKUP_TIMER_PRESCALER
/*********************************/

/* Mask for Wake up CD control (ADDR 0x31) */
#define WKUP_CD_GoActive    0x40
#define WKUP_FD_Ignore      0x20
#define WKUP_FD_En          0x10
#define CD_TX_Delay         0x04
#define CD_Avg              0x03
#define WKUP_CD_CLEAR_PROPERTY  0x8F
/*********************************/

/* BC45 command for writing to address 0x01 */
#define STARTUP_CMD				0x30
#define IDLE_CMD					0x00
#define TX_CMD						0x1A
#define RX_CMD						0x16
#define TRANSCEIVE_CMD		0x1E
#define CALCRC_CMD				0x12
#define LOADKEY_FIFO_CMD	0x19
#define AUTHENT_CMD				0x0C
#define RXFILTER_TUNE_CMD	0x10
#define LFOTUNE_CMD				0x20
#define ADC_CAL_CMD				0x21
#define CARD_DETECT_CMD		0x22
#define FIELD_DETECT_CMD	0x23
#define SIGNATURE_CMD			0x31
/**********************************/

/* Definition response status */
#define WKUP_TIMER_RUNNING      0x2A
#define WKUP_TIMER_STOP         0x2B
#define _SUCCESS_               0x01
#define FAILED                  0x00
/*********************************/

/* Structure of Wake up timer */
typedef struct struct_tBC45_WAKEUP_TIMER
{	
    uint8_t Timer_RELOAD;
    uint8_t Timer_PRESCALER;
    uint8_t Timer_COUNTER;

} tBC45_WAKEUP_TIMER_PROPERTY;

/* Structure of Card Detection property */
typedef struct struct_tBC45_CD_FD_CONFIG
{
    uint8_t CD_GOActive;
    uint8_t FD_Ignore;
    uint8_t FD_Enable;

} tBC45_CARDDETECT_PROPERTY;

/* Structure of Card Detection Threshold */
typedef struct struct_tBC45_CD_THRESHOLD
{
    uint8_t CD_Threshold_I_H;
    uint8_t CD_Threshold_I_L;
    uint8_t CD_Threshold_Q_H;
    uint8_t CD_Threshold_Q_L;

} tBC45_CD_TUNING;


/* Function for test BC45 Card Detection */
uint8_t TimerWKUP_GetStatus(void);
uint8_t TimerWKUP_SetPreScaler(uint8_t prescale);
uint8_t TimerWKUP_SetReload(uint8_t reload);
uint8_t TimerWKUP_SetCounter(uint8_t counter);
uint8_t BC45_Mode_Selection(uint8_t BC45_Running_Mode);
uint8_t Interrupt_GetIrq_SourceBC45(uint8_t ChkIrq_Source);
uint8_t BC45_CDMode_Exit(void);
void TimerWKUP_Start(void);
void TimerWKUP_Stop(void);
void TimerWKUP_INIT(tBC45_WAKEUP_TIMER_PROPERTY WAKEUP_TIMER_PROPERTY);
void CD_ADC_Tuning(tBC45_CD_TUNING ADC_Threshold);
void Interrupt_EnableBC45(uint8_t IrqEn_Source);
void ADC_Calibration(void);

void Timeout_Handler(void);
void Timeout_Start(uint16_t Count_times);
uint8_t TimeOut_Check(void);

#endif
