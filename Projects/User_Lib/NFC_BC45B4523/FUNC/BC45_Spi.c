/*
 * BC45_Spi.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/

#include "BC45_Spi.h"

uint8_t txBuffer[65];
uint8_t rxBuffer[65];

/**
  * @brief  SPI write single register of BC45.
  * @param  address: Register address of BC45 for write.
  * @param  data: Data to write.
  * @retval None
  */
void BC45_SPI_writeSingleRegister(uint8_t address, uint8_t data)
{
  uint8_t tmpAddr = 0;
  tmpAddr = (uint8_t)(address << 1) & 0x7E;
	
  SPI_SoftwareSELCmd(BC45_SPI_PORT, SPI_SEL_ACTIVE);
	Spi_SendReceive(BC45_INDEX, tmpAddr);
	Spi_SendReceive(BC45_INDEX, data);
	SPI_SoftwareSELCmd(BC45_SPI_PORT, SPI_SEL_INACTIVE);
}


/**
  * @brief  SPI read single register of BC45.
  * @param  address: Register address of BC45 for read.
  * @param  data: Pointer to data buffer for storing
  * 			  output data from the register.
  * @retval None
  */
void BC45_SPI_readSingleRegister(uint8_t address, uint8_t *data)
{
  uint8_t tmpAddr = 0; 
  tmpAddr = (((uint8_t)(address << 1) & 0x7E) | 0x80);

  SPI_SoftwareSELCmd(BC45_SPI_PORT, SPI_SEL_ACTIVE);
	Spi_SendReceive(BC45_INDEX, tmpAddr);
	*data = Spi_SendReceive(BC45_INDEX, 0);
	SPI_SoftwareSELCmd(BC45_SPI_PORT, SPI_SEL_INACTIVE);
}

/**
  * @brief  SPI write multiple data to single register of BC45.
  * @param  address: Register address of BC45 to write.
  * @param  data: Pointer to data input buffer for writing.
  * @param  len: Length of data input.
  * @retval None
  */
void BC45_SPI_writeMultiData(uint8_t address, uint8_t *data, uint16_t inputLen)
{
  uint8_t srcOffset = 0;
  uint8_t tmpAddr = 0;
  uint8_t len;
  uint8_t countData;

  /*Assign the address to write*/
  tmpAddr = (uint8_t)(address << 1) & 0x7E;

  SPI_SoftwareSELCmd(BC45_SPI_PORT, SPI_SEL_ACTIVE);

	Spi_SendReceive(BC45_INDEX, tmpAddr);
	
	while(inputLen > 0)
	{
		if(inputLen <= 64) len = inputLen;
		else len = 64;

		for(countData = 0; countData < len; countData++)
		{
			Spi_SendReceive(BC45_INDEX, data[countData + srcOffset]);
		}
		
		srcOffset += len;
		inputLen -= len;
	}

  SPI_SoftwareSELCmd(BC45_SPI_PORT, SPI_SEL_INACTIVE);	
}


/**
  * @brief  SPI read multiple data from single register of BC45.
  * @param  address: Register address of BC45 to read.
  * @param  data: Pointer to data output buffer for reading.
  * @param  len: Length of data output.
  * @retval None
  */
void BC45_SPI_readMultiData(uint8_t address, uint8_t *data, uint16_t outputLen)
{
  uint8_t srcOffset = 0;
  uint8_t tmpAddr = 0;
  uint8_t len;
  uint8_t countData;

  SPI_SoftwareSELCmd(BC45_SPI_PORT, SPI_SEL_INACTIVE);

	  /*Assign the address to write*/
  tmpAddr = ((address << 1) & 0x7E );

  SPI_SoftwareSELCmd(BC45_SPI_PORT, SPI_SEL_ACTIVE);

	Spi_SendReceive(BC45_INDEX, tmpAddr | 0x80);
	
	while(outputLen > 0)
	{
		if(outputLen <= 64) len = outputLen;
		else len = 64;

		for(countData = 0; countData < len - 1; countData++)
		{
			data[countData + srcOffset] = Spi_SendReceive(BC45_INDEX, tmpAddr);
		}
		
		if(outputLen <= 64) data[countData + srcOffset] = Spi_SendReceive(BC45_INDEX, 0);
		else data[countData + srcOffset] = Spi_SendReceive(BC45_INDEX, tmpAddr);
		
		srcOffset += len;
		outputLen -= len;
	}

  SPI_SoftwareSELCmd(BC45_SPI_PORT, SPI_SEL_INACTIVE);
}
