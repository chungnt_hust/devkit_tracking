/*
 *  bc45_cli.h
 *
 *  Created on: March 25, 2020
 *      Author: Crystal Su
 */

#ifndef BC45_CLI_H_
#define BC45_CLI_H_


#include "ht32.h"
#include "BC45_Function.h"

#if(ISO14443A == 1)
#include "ISO14443A_Category.h"
#endif
#if(ISO15693 == 1)
#include "ISO15693_Category.h"
#endif
#if(ISO14443B == 1)
#include "ISO14443B_Category.h"
#endif

#define CONFIG_14443A   0x3A
#define CONFIG_14443B   0x3B
#define CONFIG_15693    0x93
#define INV_16_SLOTS    0x16
#define INV_1_SLOT			0x01
#define ISO15693_Speed  0x11

/*RF field definition*/
#define RFON 			0x01
#define	RFOFF  		0x02
#define	RFRESET  	0x03

typedef enum{
	ISO14443A_TagType = 0x01u,
	ISO14443B_TagType = 0x02u,
	ISO15693_TagType  = 0x03u
}tISO_TagTypeDef;

typedef enum{
	SLEEPMODE = 0x01u,
	STOPMODE = 0x02u,
	NONLOWPWR = 0x03u
}tMCU_LowPwrTypeDef;

typedef enum{
	A_SETUP = 0x00u,
	A_REQA,
	A_WUPA,
	A_ANTICOLA,
	A_SELA,
	A_HLTA,
	A_GETUID,
	A_TRANS_CRC,
	A_TRANS_NOCRC,
	A_NOCOMMAND
}nfcA_Command_t;

typedef enum{
	B_SETUP = 0x00u,
	B_REQB,
	B_WUPB,
	B_ATTRI,
	B_HLTB,
	B_GETUID,
	B_TRANS_CRC,
	B_TRANS_NOCRC,
	B_NOCOMMAND
}nfcB_Command_t;


typedef enum{
	V_SETUP = 0x00u,
	V_INV1,
	V_INV16,
	V_QUIET,
	V_TRANS_CRC,
	V_TRANS_NOCRC,
	V_NOCOMMAND
}nfcV_Command_t;

typedef struct{
	int nIdx;
	uint8_t addr[8];
	uint8_t data[8];
}nfc_config_t;

#define T2T_READ		0x10
#define T2T_WRITE		0x20

extern uint8_t runningLoop;
extern uint8_t typeMask;
extern uint8_t waittingIRQ;
extern tISO_TagTypeDef tag_type;
extern nfc_config_t nfca0;
extern nfc_config_t nfca1;
extern nfc_config_t nfcb0;
extern nfc_config_t nfcb1;

/*External variable*/
extern tBC45_WAKEUP_TIMER_PROPERTY Timer_WKUP;
extern tBC45_CD_TUNING Tunning_Thereshold;

void BC45_Configuration(uint8_t tagtype);
uint8_t ScanUID_ISO14443ATagType(void);
uint8_t SelectUID_ISO14443ATagType(void);
uint8_t ScanUID_ISO15693TagType(char *UID);
uint8_t SelectUID_ISO15693TagType(void);
uint8_t ScanUID_ISO14443BTagType(void);
uint8_t SelectUID_ISO14443BTagType(void);
uint8_t ScanCertificateFlow_ISO14443ATagType(uint8_t *Running_state);
uint8_t ScanCertificateFlow_ISO14443BTagType(uint8_t *Running_state);
uint8_t ScanCertificateFlow_ISO15693TagType(uint8_t *Running_state);
void BC45_RF_OnOff(uint8_t rf_field);
void BC45_ConfigurationNoOfffield(uint8_t tagtype);
void UIDTypeA_BytesToChar(uint8_t *bytesUID, uint8_t *charUID, uint16_t  *outputlen);
void UID15693_BytesToChar(uint8_t *InputUID, uint8_t *OutputUID, uint8_t *outputlen, uint8_t Inv);
void UIDTypeB_BytesToChar(uint8_t *bytesUID, uint8_t *charUID, uint16_t *outputLen);
void Display_ADC_I_Q(void);
void byteToChar(uint8_t *inputByte, uint8_t *outputChar);

void BC45_InitISO15693(void);
void BC45_ConvertUID2Char(uint8_t *inputByte, char *outputChar);
#endif /* BC45_CLI_H_ */
