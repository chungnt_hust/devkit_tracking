/*
 * softI2C.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef SOFTI2C_H
#define SOFTI2C_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
/*--------------------- softI2C ------------------------------------------------*/

#define SCL_GPIOX             C
#define SCL_GPION             11
#define SDA_GPIOX             C
#define SDA_GPION             12

#define softI2C_SCL_BITCLOCK  STRCAT2(P, SCL_GPIOX)
#define softI2C_SCL_PORT  	  STRCAT2(HT_GPIO, SCL_GPIOX)
#define softI2C_SCL_PORTNUM   STRCAT2(GPIO_P, SCL_GPIOX)
#define	softI2C_SCL_PIN			  STRCAT2(GPIO_PIN_, SCL_GPION)

#define softI2C_SDA_BITCLOCK  STRCAT2(P, SDA_GPIOX)
#define softI2C_SDA_PORT  		STRCAT2(HT_GPIO, SDA_GPIOX)
#define softI2C_SDA_PORTNUM   STRCAT2(GPIO_P, SDA_GPIOX)
#define	softI2C_SDA_PIN			  STRCAT2(GPIO_PIN_, SDA_GPION)

#define	RTC_ACK								0
#define	RTC_NACK							1

#define softI2C_CLK(x)   			((x) ? (softI2C_SCL_PORT->SRR = softI2C_SCL_PIN) : (softI2C_SCL_PORT->RR = softI2C_SCL_PIN))
#define softI2C_DIO(x)   			((x) ? (softI2C_SDA_PORT->SRR = softI2C_SDA_PIN) : (softI2C_SDA_PORT->RR = softI2C_SDA_PIN))
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */

/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void softI2C_Init(uint8_t slaveAdd);
void softI2C_Start(void);
void softI2C_Stop(void);
void softI2C_Send_Byte(uint8_t data_byte);
uint8_t softI2C_Receive_Byte(uint8_t ack);
void softI2C_Write(uint8_t addr, uint8_t data);
uint8_t softI2C_Read(uint8_t addr);
#endif
#ifdef __cplusplus
}
#endif





























































