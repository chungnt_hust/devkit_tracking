/*
 * ht1382.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht1382.h"
#include "softI2C.h"


/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
static uint8_t hourMode = HT1382_HR_12;

/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
static uint8_t DEC_BCD(uint8_t DEC)
{
	uint8_t temp = DEC;
	return (((DEC/10)*16) + (temp%10));
}
/*---------------*/
static uint8_t BCD_DEC(uint8_t BCD)
{
	uint8_t temp = BCD;
	return ((BCD/16)*10 + temp%16);
}

/*********************************************************************************************************//**
  * @brief  HT1382_Init.
  * @retval None
  ***********************************************************************************************************/
void HT1382_Init(void)
{
	uint8_t hr; 
	
	hourMode = HT1382_HR_24;
	
	softI2C_Init(HT1382_ADDR);
	softI2C_Write(HT1382_WPR, 0);
	
	hr = softI2C_Read(HT1382_HOURR);
	if(hr & HT1382_HR_24) hr &= (~HT1382_HR_24);
	softI2C_Write(HT1382_HOURR, hr | hourMode);
}

/*********************************************************************************************************//**
  * @brief  HT1382_Read_Datetime.
  * @retval None
  ***********************************************************************************************************/
DateTime_TypeDef HT1382_Read_Datetime(void)
{
	DateTime_TypeDef rtc;
	uint8_t Sec, Min, Hour, Date, Day, Month, Year;
	softI2C_Start();
	softI2C_Send_Byte(HT1382_ADDR);
	softI2C_Send_Byte(HT1382_SECR);
	softI2C_Start();
	softI2C_Send_Byte(HT1382_ADDR | 1);
	Sec   = softI2C_Receive_Byte(RTC_ACK);
	Min   = softI2C_Receive_Byte(RTC_ACK);
	Hour  = softI2C_Receive_Byte(RTC_ACK);
	Hour &= (~hourMode);
	Date  = softI2C_Receive_Byte(RTC_ACK);
	Day   = softI2C_Receive_Byte(RTC_ACK);
	Month = softI2C_Receive_Byte(RTC_ACK);
	Year  = softI2C_Receive_Byte(RTC_NACK);
	softI2C_Stop();	
	rtc.Seconds = BCD_DEC(Sec);
	rtc.Minutes = BCD_DEC(Min);
	rtc.Hours 	= BCD_DEC(Hour);
	rtc.Day 		= BCD_DEC(Day);
	rtc.Month 	= BCD_DEC(Month);
	rtc.Year 		= BCD_DEC(Year);
	rtc.Date 		= BCD_DEC(Date);
	return rtc;
}

/*********************************************************************************************************//**
  * @brief  HT1382_Write_Datetime.
  * @retval None
  ***********************************************************************************************************/
void HT1382_Write_Datetime(DateTime_TypeDef rtc)
{
	softI2C_Start();
	softI2C_Send_Byte(HT1382_ADDR);
	softI2C_Send_Byte(HT1382_SECR);
	softI2C_Send_Byte(DEC_BCD(rtc.Seconds));		// sec
	softI2C_Send_Byte(DEC_BCD(rtc.Minutes));		// min
	softI2C_Send_Byte(DEC_BCD(rtc.Hours) | hourMode);   	// hour
	softI2C_Send_Byte(DEC_BCD(rtc.Date));			// day on week
	softI2C_Send_Byte(DEC_BCD(rtc.Day));	  		// day
	softI2C_Send_Byte(DEC_BCD(rtc.Month));	  	// month
	softI2C_Send_Byte(DEC_BCD(rtc.Year));	  	// year	
	softI2C_Stop();
}

/*********************************************************************************************************//**
  * @brief  HT1382_Read.
  * @retval None
  ***********************************************************************************************************/
uint8_t HT1382_Read(uint8_t add)
{
	uint8_t tem = softI2C_Read(add);
	return BCD_DEC(tem);
}

/*********************************************************************************************************//**
  * @brief  HT1382_Write.
  * @retval None
  ***********************************************************************************************************/
void HT1382_Write(uint8_t add, uint8_t data)
{
	uint8_t tem = DEC_BCD(data);
	if(add == HT1382_HOURR) tem |= hourMode;
	softI2C_Write(add, tem);
}
