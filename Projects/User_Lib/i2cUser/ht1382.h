/*
 * ht1382.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef HT1382_H
#define HT1382_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
/*--------------------- softI2C ------------------------------------------------*/
#define HT1382_ADDR 			    0xD0 		// I2C 7-bit slave address shifted for 1 bit to the left
#define HT1382_SECR  		      0x00 		// softI2C seconds address
#define HT1382_MINR  		      0x01 		// softI2C minutes address
#define HT1382_HOURR  		    0x02		// softI2C hours address
#define HT1382_WPR         		0x07 		// HT1382 write protect 

#define HT1382_HR_12					0
#define HT1382_HR_24					0x80
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */
typedef struct 
{
	uint8_t Seconds;
	uint8_t Minutes;
	uint8_t Hours;
	uint8_t Day;
	uint8_t Month;
	uint8_t Year;
	uint8_t Date;
} DateTime_TypeDef;

/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void HT1382_Init(void);
DateTime_TypeDef HT1382_Read_Datetime(void);
void HT1382_Write_Datetime(DateTime_TypeDef rtc);
uint8_t HT1382_Read(uint8_t add);
void HT1382_Write(uint8_t add, uint8_t data);
#endif
#ifdef __cplusplus
}
#endif





























































