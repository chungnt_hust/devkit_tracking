/*
 * i2cUser.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef I2CUSER_H
#define I2CUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */

/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */
/***************************  I2C0  ***************************************//**/
#define I2C0_USE_INTERRUPT 												  (1)
#define I2C0_MASTER_SCK_ID                          (GPIO_PC)
#define I2C0_MASTER_SCK_AFIO_PIN                    (AFIO_PIN_12)
#define I2C0_MASTER_SDA_ID                          (GPIO_PC)
#define I2C0_MASTER_SDA_AFIO_PIN                    (AFIO_PIN_13)

#define I2C0_MASTER_ADDRESS    											0x0A
#define SLAVE_ADDRESS                               0x68
#define I2C0_CLK_SPEED             									400000

/***************************  I2C1  ***************************************//**/
#define I2C1_USE_INTERRUPT 												  (0)
#define I2C1_MASTER_SCK_ID                          (GPIO_PC)
#define I2C1_MASTER_SCK_AFIO_PIN                    (AFIO_PIN_4)
#define I2C1_MASTER_SDA_ID                          (GPIO_PC)
#define I2C1_MASTER_SDA_AFIO_PIN                    (AFIO_PIN_5)

#define I2C1_MASTER_ADDRESS    											0x0A
#define SLAVE_ADDRESS                               0x68
#define I2C1_CLK_SPEED             									400000

/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void I2C0_Configuration(void);
void I2C0_Sendbyte(uint8_t Register, uint8_t inData);
uint8_t I2C0_Readbyte(uint8_t Register);

#endif
#ifdef __cplusplus
}
#endif
