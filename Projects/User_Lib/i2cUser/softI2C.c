/*
 * softI2C.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "softI2C.h"


/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
static uint8_t softI2C_addr = 0xFF;

/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
//-----------------------------------------------------------------
static void softI2C_NOP(void)
{
	uint8_t i;
	for(i = 0; i < 10; i++);
}

/*********************************************************************************************************//**
  * @brief  softI2C_Init.
  * @retval None
  ***********************************************************************************************************/
void softI2C_Init(uint8_t slaveAdd)
{
	CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};

	CKCUClock.Bit.softI2C_SCL_BITCLOCK           = 1;
	CKCUClock.Bit.softI2C_SDA_BITCLOCK           = 1;
	CKCUClock.Bit.AFIO         = 1;
	CKCU_PeripClockConfig(CKCUClock, ENABLE);

	AFIO_GPxConfig(softI2C_SCL_PORTNUM, softI2C_SCL_PIN, AFIO_FUN_GPIO);
	AFIO_GPxConfig(softI2C_SDA_PORTNUM, softI2C_SDA_PIN, AFIO_FUN_GPIO);
	AFIO_GPxConfig(softI2C_SCL_PORTNUM, softI2C_SCL_PIN, AFIO_FUN_GPIO);
	AFIO_GPxConfig(softI2C_SDA_PORTNUM, softI2C_SDA_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(softI2C_SCL_PORT, softI2C_SCL_PIN, GPIO_DIR_OUT);
	GPIO_DirectionConfig(softI2C_SDA_PORT, softI2C_SDA_PIN, GPIO_DIR_OUT);
	
	softI2C_DIO(1);
	softI2C_CLK(1);
	
	softI2C_addr = slaveAdd;
}

/*********************************************************************************************************//**
  * @brief  softI2C_Start.
  * @retval None
  ***********************************************************************************************************/
void softI2C_Start(void)
{
	softI2C_DIO(1);
	softI2C_NOP();  
	softI2C_CLK(1);
	softI2C_NOP();  
	softI2C_DIO(0);
	softI2C_NOP();  
	softI2C_CLK(0);
	softI2C_NOP();  	
}

/*********************************************************************************************************//**
  * @brief  softI2C_Stop.
  * @retval None
  ***********************************************************************************************************/
void softI2C_Stop(void)
{
	softI2C_DIO(0);
	softI2C_NOP();
	softI2C_CLK(1);  
	softI2C_NOP();  
	softI2C_DIO(1);
	softI2C_NOP();
	softI2C_CLK(0);
	softI2C_NOP();
}

/*********************************************************************************************************//**
  * @brief  softI2C_Send_Byte.
  * @retval None
  ***********************************************************************************************************/
void softI2C_Send_Byte(uint8_t data)
{
	uint8_t i;
	for(i = 0; i < 8; i++)
	{		
		if(data&0x80)
		{
			softI2C_DIO(1);
		}
		else 
		{
			softI2C_DIO(0);
		}
		softI2C_NOP();
		softI2C_CLK(1);
		softI2C_NOP();
		softI2C_CLK(0);
		data <<= 1;
	}
	softI2C_NOP();
	softI2C_DIO(0);
	softI2C_NOP();
	softI2C_CLK(1);
	softI2C_NOP();
	softI2C_CLK(0);
}

/*********************************************************************************************************//**
  * @brief  softI2C_Receive_Byte.
  * @retval None
  ***********************************************************************************************************/
uint8_t softI2C_Receive_Byte(uint8_t ack)
{
	uint8_t i, tem_byte = 0x00;

	AFIO_GPxConfig(softI2C_SDA_PORTNUM, softI2C_SDA_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(softI2C_SDA_PORT, softI2C_SDA_PIN , GPIO_DIR_IN);
	GPIO_PullResistorConfig(softI2C_SDA_PORT, softI2C_SDA_PIN, GPIO_PR_UP);
	GPIO_InputConfig(softI2C_SDA_PORT, softI2C_SDA_PIN, ENABLE);
	
	// Read SDA input
	for(i = 0; i < 8; i++)
	{	
		softI2C_CLK(1);
		softI2C_NOP();
		if(softI2C_SDA_PORT->DINR & softI2C_SDA_PIN)
		{
			tem_byte <<= 1;
			tem_byte++;
		}
		else
		{
			tem_byte <<= 1;
		}
		softI2C_CLK(0);
		softI2C_NOP();
	}
		
	AFIO_GPxConfig(softI2C_SDA_PORTNUM, softI2C_SDA_PIN, AFIO_FUN_GPIO);
	AFIO_GPxConfig(softI2C_SDA_PORTNUM, softI2C_SDA_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(softI2C_SDA_PORT, softI2C_SDA_PIN, GPIO_DIR_OUT);
		
	/* low for ack, high for nack */
	if(ack == RTC_ACK) softI2C_DIO(0);
	else softI2C_DIO(1);
	softI2C_CLK(1);
	softI2C_NOP();
	softI2C_CLK(0);
	softI2C_NOP();	
	softI2C_DIO(1);          /* Release the sda line */
	softI2C_NOP();
	return(tem_byte);
}

/*********************************************************************************************************//**
  * @brief  softI2C_Write.
  * @retval None
  ***********************************************************************************************************/
void softI2C_Write(uint8_t addr, uint8_t data)
{
	softI2C_Start();
	//------ Lenh gui dia chi ------
	softI2C_Send_Byte(softI2C_addr);
	softI2C_Send_Byte(addr);
	//------ Lenh gui du lieu ------
	softI2C_Send_Byte(data);
	softI2C_Stop();
}

/*********************************************************************************************************//**
  * @brief  softI2C_Read.
  * @retval None
  ***********************************************************************************************************/
uint8_t softI2C_Read(uint8_t addr)
{
	uint8_t data = 255;
	softI2C_Start();
	//------ Send Add + write ------
	softI2C_Send_Byte(softI2C_addr);
	softI2C_Send_Byte(addr);
	//------ Restart
	softI2C_Start();
	//------ Send Add + read ------
	softI2C_Send_Byte(softI2C_addr | 1);
	//------ Send Data  ------
	data = softI2C_Receive_Byte(RTC_NACK);
	softI2C_Stop();
	return data;
}
