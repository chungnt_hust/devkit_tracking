/*
 * uartInterfaces.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef UARTINTERFACES_H
#define UARTINTERFACES_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */

/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */
#define RS485_TX_ENABLE GPIO_WriteOutBits(DRE_485_PORT, DRE_485_PIN, SET);
#define RS485_RX_ENABLE GPIO_WriteOutBits(DRE_485_PORT, DRE_485_PIN, RESET);



/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */

#endif
#ifdef __cplusplus
}
#endif
