/*
 * uartConfigPort.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef UARTCONFIGPORT_H
#define UARTCONFIGPORT_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/******************************************************************************
* Macros 
******************************************************************************/

/* #define and enum statements go here */

typedef enum
{
	CMD_NOT_SUPPORT = 0,
	CMD_ERR,
	CMD_OK,
} ERR_E;
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void uartCfgPort_Process(char *data, uint16_t len);
#endif
#ifdef __cplusplus
}
#endif
