/*
 * uartUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "uartUser.h"

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */

/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
static buffer_t bufferRxCfg;
static buffer_t bufferRs232;
static uint8_t bufferGNSS[1024], indxGNSS = 0, timeout = 0;
static HT_USART_TypeDef *uartPort[MAX_UART_INDEX] = {CELL_PORT, CONFIG_PORT, RS232_PORT, GNSS_PORT};
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static void clkBitCELL(CKCU_PeripClockConfig_TypeDef *CKCUClock);
static void clkBitCONFIG(CKCU_PeripClockConfig_TypeDef *CKCUClock);
static void clkBitRS232(CKCU_PeripClockConfig_TypeDef *CKCUClock);
static void clkBitGNSS(CKCU_PeripClockConfig_TypeDef *CKCUClock);
static void RX_ProcessNewData(uint8_t uartId);

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
const Uart_Use_Typedef UART_Use[MAX_UART_INDEX] = 
{
	{clkBitCELL, 		CELL_RX_GPIO_PORT, 		CELL_RX_GPIO_PIN, 	CELL_TX_GPIO_ID, 		CELL_TX_AFIO_PIN, 	CELL_RX_GPIO_ID, 		CELL_RX_AFIO_PIN, 	CELL_PORT, 		CELL_IRQn, 		CELL_CKCU_PCLK},
  {clkBitCONFIG, 	CONFIG_RX_GPIO_PORT, 	CONFIG_RX_GPIO_PIN, CONFIG_TX_GPIO_ID, 	CONFIG_TX_AFIO_PIN, CONFIG_RX_GPIO_ID, 	CONFIG_RX_AFIO_PIN, CONFIG_PORT, 	CONFIG_IRQn, 	CONFIG_CKCU_PCLK},
	{clkBitRS232, 	RS232_RX_GPIO_PORT, 	RS232_RX_GPIO_PIN, 	RS232_TX_GPIO_ID, 	RS232_TX_AFIO_PIN, 	RS232_RX_GPIO_ID, 	RS232_RX_AFIO_PIN, 	RS232_PORT, 	RS232_IRQn, 	RS232_CKCU_PCLK},	
	{clkBitGNSS, 		GNSS_RX_GPIO_PORT, 		GNSS_RX_GPIO_PIN, 	GNSS_TX_GPIO_ID, 		GNSS_TX_AFIO_PIN, 	GNSS_RX_GPIO_ID, 		GNSS_RX_AFIO_PIN, 	GNSS_PORT, 		GNSS_IRQn, 		GNSS_CKCU_PCLK},
};
/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void UART_Cfg(uartIndex_t uartIndex, u32 baudRate, u16 parity, u16 dataLength, u16 stopBit)
{
	/* Enable peripheral clock of AFIO, UxART                                                               */
	CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
	CKCUClock.Bit.AFIO                   		= 1;
	UART_Use[uartIndex].clkFunc(&CKCUClock);
	CKCU_PeripClockConfig(CKCUClock, ENABLE);
	

  /* Turn on UxART Rx internal pull up resistor to prevent unknow state                                     */                             
  GPIO_PullResistorConfig(UART_Use[uartIndex].HT_GPIOx, UART_Use[uartIndex].GPIO_PIN, GPIO_PR_UP);
	
  /* Config AFIO mode as UxART function.                                                                    */
  AFIO_GPxConfig(UART_Use[uartIndex].AFIO_GPIOP_TX, UART_Use[uartIndex].AFIO_PIN_TX, AFIO_FUN_USART_UART); // TxS1
  AFIO_GPxConfig(UART_Use[uartIndex].AFIO_GPIOP_RX, UART_Use[uartIndex].AFIO_PIN_RX, AFIO_FUN_USART_UART); // RxS1	
  
	/* NOTE!! 16*baudRate < CK_UART < 65535*baudRate */
	// Auto change Prescaler
	u8 i;
	for(i = 0; i < 4; i++)
	{
		u32 pclk = CKCU_GetPeripFrequency(UART_Use[uartIndex].Perip);
		u32 min = pclk / 65535;
		u32 max = pclk / 16;
		u32 temp;
		temp = baudRate;
		if(min < temp && temp < max) 
			break;
		else
		{
			if(i == 3) while(1){}
			else CKCU_SetPeripPrescaler((CKCU_PeripPrescaler_TypeDef)UART_Use[uartIndex].Perip, (CKCU_APBCLKPRE_TypeDef)(i+1));
		}
	}

	{
    USART_InitTypeDef USART_InitStructure = {0};
    USART_InitStructure.USART_BaudRate = baudRate;
    USART_InitStructure.USART_WordLength = dataLength;
    USART_InitStructure.USART_StopBits = stopBit;
    USART_InitStructure.USART_Parity = parity;
    USART_InitStructure.USART_Mode = USART_MODE_NORMAL;
	  USART_Init(UART_Use[uartIndex].USARTx, &USART_InitStructure);
  }

  /* Enable UxART interrupt of NVIC                                                                         */
	NVIC_EnableIRQ(UART_Use[uartIndex].IRQn);
	
  /* Enable UxART Rx interrupt                                                                              */
  USART_IntConfig(UART_Use[uartIndex].USARTx, USART_INT_RXDR, ENABLE);
	
  /* Enable UxART Tx and Rx function                                                                        */
  USART_TxCmd(UART_Use[uartIndex].USARTx, ENABLE);
  USART_RxCmd(UART_Use[uartIndex].USARTx, ENABLE);
}

void UART_SendByte(HT_USART_TypeDef* USARTx, uint8_t xByte)
{
    USART_SendData(USARTx, xByte);
		while(USART_GetFlagStatus(USARTx, USART_FLAG_TXC) == RESET);
}

void UART_SendString(HT_USART_TypeDef* USARTx, uint8_t *str)
{
    while(*str != 0)
    {
        UART_SendByte(USARTx, *str);
        str++; 
    }
}

/*************************************************************************************************************
  * @brief  For UxART RX
  * @retval None
  ***********************************************************************************************************/
void UART_Process(void)
{
		if(bufferRxCfg.State > 0)
		{
			bufferRxCfg.State--;
			if(bufferRxCfg.State == 0)
			{
				RX_ProcessNewData(CONFIG_INDEX);
			}
		}
		
		if(bufferRs232.State > 0)
		{
			bufferRs232.State--;
			if(bufferRs232.State == 0)
			{
				RX_ProcessNewData(RS232_INDEX);
			}
		}
		
		if(timeout > 0)
		{
			timeout--;
			if(timeout == 0)
			{
				RX_ProcessNewData(GNSS_INDEX);
			}
		}
}

/*************************************************************************************************************
  * @brief  IRQHandler Cellular data
  * @retval None
  ***********************************************************************************************************/
void USART0_IRQHandler(void)
{
	if(USART_GetFlagStatus(uartPort[CELL_INDEX], USART_FLAG_RXDR))
	{
			AT_AddC(USART_ReceiveData(uartPort[CELL_INDEX]));
	}
}

/*************************************************************************************************************
  * @brief  IRQHandler RS232 
  * @retval None
  ***********************************************************************************************************/
void USART1_IRQHandler(void)
{
	if(USART_GetFlagStatus(uartPort[RS232_INDEX], USART_FLAG_RXDR))
	{
			bufferRs232.Data[bufferRs232.Index] = USART_ReceiveData(uartPort[RS232_INDEX]);
			bufferRs232.Index++;
			bufferRs232.State = 1;
	}
}

/*************************************************************************************************************
  * @brief  IRQHandler debug/config 
  * @retval None
  ***********************************************************************************************************/
void UART0_UART2_IRQHandler(void)
{
	if(USART_GetFlagStatus(uartPort[CONFIG_INDEX], USART_FLAG_RXDR))
	{
			bufferRxCfg.Data[bufferRxCfg.Index] = USART_ReceiveData(uartPort[CONFIG_INDEX]);
			bufferRxCfg.Index++;
			bufferRxCfg.State = 1;
	}
}

/*************************************************************************************************************
  * @brief  IRQHandler GNSS
  * @retval None
  ***********************************************************************************************************/
void UART1_UART3_IRQHandler(void)
{
	if(USART_GetFlagStatus(uartPort[GNSS_INDEX], USART_FLAG_RXDR))
	{
			bufferGNSS[indxGNSS] = USART_ReceiveData(uartPort[GNSS_INDEX]);
			indxGNSS++;
			timeout = 1;
	}
}

/* Private functions ---------------------------------------------------------------------------------------*/
static void clkBitCELL(CKCU_PeripClockConfig_TypeDef *CKCUClock)
{                                                             
	(*CKCUClock).Bit.CELL_RX_GPIO_CLK            = 1;
	(*CKCUClock).Bit.CELL_IPN		                 = 1;
}
	 
static void clkBitCONFIG(CKCU_PeripClockConfig_TypeDef *CKCUClock)
{ 	
	(*CKCUClock).Bit.CONFIG_RX_GPIO_CLK          = 1;
	(*CKCUClock).Bit.CONFIG_IPN		               = 1;
}

static void clkBitGNSS(CKCU_PeripClockConfig_TypeDef *CKCUClock)
{ 
	(*CKCUClock).Bit.GNSS_RX_GPIO_CLK            = 1;
	(*CKCUClock).Bit.GNSS_IPN		                 = 1;
}

static void clkBitRS232(CKCU_PeripClockConfig_TypeDef *CKCUClock)
{ 
	(*CKCUClock).Bit.RS232_RX_GPIO_CLK           = 1;
	(*CKCUClock).Bit.RS232_IPN		               = 1;
}

static void RX_ProcessNewData(uint8_t uartId)
{
	switch(uartId)
	{
		case CONFIG_INDEX:
			bufferRxCfg.Data[bufferRxCfg.Index] = 0;
			// process uart config port 
			uartCfgPort_Process((char*)bufferRxCfg.Data, bufferRxCfg.Index);
			bufferRxCfg.Index = 0;
			break;
				
		case RS232_INDEX:
			bufferRs232.Data[bufferRs232.Index] = 0;
			// process uart interfaces port 
			DEBUG("RX: %s", bufferRs232.Data);
			if(TCP_LogParams((char*)bufferRs232.Data)) 
			{
				bufferRs232.Index = 0;
				DEBUG("\r\nOK\r\n");
				return;
			}
			if(TCP_SetParams((char*)bufferRs232.Data)) 
			{
				bufferRs232.Index = 0;
				DEBUG("\r\nOK\r\n");
				return;
			}
			if(Cellular_CheckReady2Use() == 1)
				UART_SendString(CELL_PORT, bufferRs232.Data);
			bufferRs232.Index = 0;
			break;
			
		case GNSS_INDEX:
			bufferGNSS[indxGNSS] = 0;
			DEBUG("[GNSS]: %s\r\n", bufferGNSS);
			indxGNSS = 0;
			break;
	}
}
