/*
 * uartConfigPort.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "uartConfigPort.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define COMMAND_MAX_LEN (10)

#define MSG_FIELD_SEPARATOR (':')
/* Message field position */
#define MSG_FIELD_TYPE (0)
#define MSG_FIELD_DATA (1)
#define MSG_MAX_FIELDS (2)

typedef ERR_E(* PACKET_FUNC_PTR)(char* input, char* out);
/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static ERR_E Config_HandleSettingParams(char* input, char* out);
static ERR_E Config_HandleCtrlCellular(char* input, char* out);
/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
typedef struct parse_cmd_process_tag
{
    uint8_t						msg_id;
    const char				packet_str[COMMAND_MAX_LEN];
    PACKET_FUNC_PTR		packet_func_ptr;
} PARSE_PACKET_PROCESS_T;

static const PARSE_PACKET_PROCESS_T packet_process_table[] =
{
	{1, "SET_PARAMS", Config_HandleSettingParams},
	{2, "CTRL_CELL", Config_HandleCtrlCellular},
};

static uint8_t packet_process_table_size = sizeof(packet_process_table)/sizeof(packet_process_table[0]);
/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void uartCfgPort_Process(char *data, uint16_t len)
{
	char	*msg_field[MSG_MAX_FIELDS];
	ERR_E ret = CMD_NOT_SUPPORT;
	// pointer 
	char *msg_token;
	char data_reply[20] = {0};
	// generic indexers
	uint8_t j;
	
	memset(msg_field, 0x00, sizeof(msg_field));

	msg_token		 							= data;
	msg_field[MSG_FIELD_TYPE] = msg_token;
	
	while(*msg_token != '\0')
	{
		if(*msg_token == MSG_FIELD_SEPARATOR)
		{
				// terminate string after field separator or end-of-message characters
				*msg_token = '\0';

				// save position of the next token
				msg_field[MSG_FIELD_DATA] = msg_token + 1;
				break;
		}
		msg_token++;
	}
	
	for(j = 0; j < packet_process_table_size; j++)
	{
		if(strcmp((char*)packet_process_table[j].packet_str, (char*)msg_field[MSG_FIELD_TYPE]) == 0)
		{
				ret = packet_process_table[j].packet_func_ptr((char*)msg_field[MSG_FIELD_DATA], (char*)data_reply);
				break;
		}
	}
	
	if(j == packet_process_table_size)
	{
		DEBUG("Command not support\r\n");
		return;
	}
				
	DEBUG("+%s, ret: %d\r\n", (char*)packet_process_table[j].packet_str, ret);
}

/* Private functions ---------------------------------------------------------------------------------------*/
static ERR_E Config_HandleSettingParams(char* input, char* out)
{
	
	return CMD_OK; 
}

static ERR_E Config_HandleCtrlCellular(char* input, char* out)
{	
	UART_SendString(CELL_PORT, (uint8_t*)input);
	return CMD_OK;
}
