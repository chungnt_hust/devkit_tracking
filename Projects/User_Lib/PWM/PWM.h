/*
 * PWM.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef PWM_H
#define PWM_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define PWM0_IPN                         			PWM0

#define PWM0_CH0_CHN                         	0
#define PWM0_CH0_GPIOX                       	C
#define PWM0_CH0_GPION                       	1

#define PWM0_CH1_CHN                         	1
#define PWM0_CH1_GPIOX                       	B
#define PWM0_CH1_GPION                       	0

#define PWM0_CH2_CHN                         	2
#define PWM0_CH2_GPIOX                       	B
#define PWM0_CH2_GPION                       	15

#define PWM0_CH3_CHN                         	3
#define PWM0_CH3_GPIOX                       	C
#define PWM0_CH3_GPION                       	0

#define PWM0_PORT                         			STRCAT2(HT_,       PWM0_IPN)
#define PWM0_AFIO_FUN                     			STRCAT2(AFIO_FUN_, PWM0_IPN)

/* =================================================================================== */
/* ================================== PWM0__CH0 ========================================= */
/* =================================================================================== */
#define PWM0_CH0_GPIO_ID                         STRCAT2(GPIO_P,         PWM0_CH0_GPIOX)
#define PWM0_CH0_AFIO_PIN                        STRCAT2(AFIO_PIN_,      PWM0_CH0_GPION)
#define PWM0_CH0_CH                              STRCAT2(TM_CH_,         PWM0_CH0_CHN)
/********************************** PWM0__CH0 DMA *************************************//**/
#define PWM0_CH0_CH_DMA													PDMA_CH2 
#define PWM0_CH0_CCR			                        CH0CCR

/* =================================================================================== */
/* ================================== PWM0__CH1 ========================================= */
/* =================================================================================== */
#define PWM0_CH1_GPIO_ID                         STRCAT2(GPIO_P,         PWM0_CH1_GPIOX)
#define PWM0_CH1_AFIO_PIN                        STRCAT2(AFIO_PIN_,      PWM0_CH1_GPION)
#define PWM0_CH1_CH                              STRCAT2(TM_CH_,         PWM0_CH1_CHN)
/********************************** PWM0__CH1 DMA *************************************//**/
#define PWM0_CH1_CH_DMA													PDMA_CH0 
#define PWM0_CH1_CCR			                        CH1CCR

/* =================================================================================== */
/* ================================== PWM0__CH2 ========================================= */
/* =================================================================================== */
#define PWM0_CH2_GPIO_ID                         STRCAT2(GPIO_P,         PWM0_CH2_GPIOX)
#define PWM0_CH2_AFIO_PIN                        STRCAT2(AFIO_PIN_,      PWM0_CH2_GPION)
#define PWM0_CH2_CH                              STRCAT2(TM_CH_,         PWM0_CH2_CHN)
/********************************** PWM0__CH2 DMA *************************************//**/
#define PWM0_CH2_CH_DMA													PDMA_CH1 
#define PWM0_CH2_CCR			                        CH2CCR

/* =================================================================================== */
/* ================================== PWM0__CH3 ========================================= */
/* =================================================================================== */
#define PWM0_CH3_GPIO_ID                         STRCAT2(GPIO_P,         PWM0_CH3_GPIOX)
#define PWM0_CH3_AFIO_PIN                        STRCAT2(AFIO_PIN_,      PWM0_CH3_GPION)
#define PWM0_CH3_CH                              STRCAT2(TM_CH_,         PWM0_CH3_CHN)
/********************************** PWM0__CH3 DMA *************************************//**/
#define PWM0_CH3_CH_DMA													PDMA_CH0 
#define PWM0_CH3_CCR			                        CH3CCR

/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */
typedef struct
{
	uint16_t timeOn;
	uint16_t period;
	uint8_t beeTime;
} Buzzer_Typedef_t;




/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void PWM_Init(void);
/*********************************************************************************************************//**
  * @brief  Set PWM Frequency
  * @param  uReload: Reload value of timer)
  * @retval None
  ***********************************************************************************************************/
void PWM_SetFreq(u32 uReload);
/*********************************************************************************************************//**
  * @brief  Update PWM Duty
  * @param  TM_CH_n: Specify the TM channel.
  * @param  uCompare: PWM duty (Compare value of timer)
  * @retval None
  ***********************************************************************************************************/
void PWM_UpdateDuty(TM_CH_Enum TM_CH_n, u32 uCompare);
/*********************************************************************************************************//**
  * @brief Enable or Disable PWM.
  * @param NewState: This parameter can be ENABLE or DISABLE.
  * @retval None
  ***********************************************************************************************************/
void PWM_Cmd(ControlStatus NewState);

/*********************************************************************************************************//**
  * @brief Added function for Buzzer
  * @param 
  * @retval None
  ***********************************************************************************************************/
void Buzzer_Init(void);
void Buzzer_SetBeep(uint16_t timeOn, uint16_t period, uint8_t beeTime);
#endif
#ifdef __cplusplus
}
#endif
