/*********************************************************************************************************//**
 * PWM.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *************************************************************************************************************
 @16-bit counter registers
 @RepetitionCounter only use for MCTM, if RepetitionCounter != 0,
  update event 1 is only generated if f the REPR counter has reached zero
  The REPR value is decreased when the following conditions occur:
  - At each counter overflow in the up-counting mode
	- At each counter underflow in the down-counting mode
	- At each counter overflow and underflow in the center-aligned counting mode
 @PWM mode 1
	- During up-counting, channel 1 has an active level when CNTR < CH1CR
	or otherwise has an inactive level.
	- During down-counting, channel 1 has an inactive level when CNTR > CH1CR 
	or otherwise has an active level
	
 @PWM mode 2
	- During up-counting, channel 1 has an inactive level when CNTR <
	CH1CR or otherwise has an active level.
	- During down-counting, channel 1 has an active level when CNTR > 
	CH1CR or otherwise has an inactive level.
	
	@ControlN, PolarityN, IdleStateN are Reserved Bits
	
	@Symmetric vs Asymmetric PWM
							   |<-period->|
									    ___        ___        ___        ___        
	Symmetric		_______|   |______|   |______|   |______|   |______
									___        ___        ___        ___       
	Asymmetric  ___|   |______|   |______|   |______|   |______
 ************************************************************************************************************/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "PWM.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
Buzzer_Typedef_t Buzzer_type;


/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
/* Default */
#define USE_PWM0_DMA         0
#define MAX_CK_AHB 					 60000000 //(ClockFreq.HCLK_Freq) // Hz
#define PWM0_PRESCALER       60000    // -> 60000000/60000 = 1000 -> 1tick = 1ms
#define PWM0_FRE    				 100    	// Hz
#define PWM0_RELOAD_COUNTER  (MAX_CK_AHB/PWM0_PRESCALER/PWM0_FRE) /* Note: 16bit counter */

#if (USE_PWM0_DMA == 1)
u32 CCR_Buffer = 0;
#endif

/* =================================================================================== */
#define BUZZER_CH_ID 				PWM0_CH0_GPIO_ID
#define BUZZER_CH_AFIO      PWM0_CH0_AFIO_PIN
#define BUZZER_CH						PWM0_CH0_CH
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
  * @brief  Init GPTM PWM function.
  * @retval None
  ***********************************************************************************************************/
void PWM_Init(void)
{
	{ /* Enable peripheral clock                                                                              */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
    CKCUClock.Bit.PWM0_IPN	    	            = 1;
		#if (USE_PWM0_DMA == 1)
		CKCUClock.Bit.PDMA 							        = 1;
		#endif 
    CKCUClock.Bit.AFIO             	        = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }
	
#if (USE_PWM0_DMA == 1)
  {
    /* !!! NOTICE !!!
       Notice that the local variable (structure) did not have an initial value.
       Please confirm that there are no missing members in the parameter settings below in this function.
    */
    PDMACH_InitTypeDef PDMACH_InitStructure;

    /* PDMA configuration                                                                                   */
    PDMACH_InitStructure.PDMACH_SrcAddr = (u32)&CCR_Buffer;
    PDMACH_InitStructure.PDMACH_DstAddr = (u32)&(PWM0_PORT->PWM0_CH0_CCR);
    PDMACH_InitStructure.PDMACH_BlkCnt = 1;
    PDMACH_InitStructure.PDMACH_BlkLen = 1;
    PDMACH_InitStructure.PDMACH_DataSize = WIDTH_32BIT;
    PDMACH_InitStructure.PDMACH_Priority = VH_PRIO;
    PDMACH_InitStructure.PDMACH_AdrMod = SRC_ADR_FIX | DST_ADR_FIX | AUTO_RELOAD;
    PDMA_Config(PWM0_CH2_CH_DMA, &PDMACH_InitStructure);
  }

  PDMA_EnaCmd(PWM0_CH2_CH_DMA, ENABLE);
#endif

  /* Configure AFIO mode as TM function                                                                     */
  AFIO_GPxConfig(BUZZER_CH_ID, BUZZER_CH_AFIO, PWM0_AFIO_FUN);


  { /* Time base configuration                                                                              */

    /* !!! NOTICE !!!
       Notice that the local variable (structure) did not have an initial value.
       Please confirm that there are no missing members in the parameter settings below this function.
    */
    TM_TimeBaseInitTypeDef TimeBaseInit;

    TimeBaseInit.Prescaler = PWM0_PRESCALER - 1;            	// Timer clock = CK_AHB / 1
    TimeBaseInit.CounterReload = (PWM0_RELOAD_COUNTER) - 1;   // CK_AHB/fre
    TimeBaseInit.RepetitionCounter = 0;
    TimeBaseInit.CounterMode = TM_CNT_MODE_UP;
    TimeBaseInit.PSCReloadTime = TM_PSC_RLD_IMMEDIATE;
    TM_TimeBaseInit(PWM0_PORT, &TimeBaseInit);

    /* Clear Update Event Interrupt flag since the "TM_TimeBaseInit()" writes the UEV1G bit                 */
	  #if 0
		TM_ClearFlag(PWM0_PORT, TM_FLAG_UEV);
		#endif
  }

  { /* Channel n output configuration                                                                       */

    /* !!! NOTICE !!!
       Notice that the local variable (structure) did not have an initial value.
       Please confirm that there are no missing members in the parameter settings below this function.
    */
    TM_OutputInitTypeDef OutInit;

    OutInit.OutputMode 				= TM_OM_PWM1;
    OutInit.Control 					= TM_CHCTL_ENABLE;
    OutInit.ControlN 					= TM_CHCTL_DISABLE;
    OutInit.Polarity 					= TM_CHP_NONINVERTED;
    OutInit.PolarityN 				= TM_CHP_NONINVERTED;
    OutInit.IdleState 				= MCTM_OIS_LOW;
    OutInit.IdleStateN 				= MCTM_OIS_LOW;
    OutInit.Compare 					= 0;
    OutInit.AsymmetricCompare = 0;

    OutInit.Channel 					= BUZZER_CH;
    TM_OutputInit(PWM0_PORT, &OutInit); // CH0 Start Output as default value
  }

	#if 1 // enable or disable interrupt
  /* Enable Update Event interrupt                                                                          */
  NVIC_EnableIRQ(PWM0_IRQn);
	TM_IntConfig(PWM0_PORT, TM_INT_UEV, ENABLE);
	#endif
	
	#if (USE_PWM0_DMA == 1)
	/* MCTM/GPTM Update Event PDMA request enable                                                             */
  TM_PDMAConfig(PWM0_PORT, TM_PDMA_UEV, ENABLE);
	#endif
	
  #if 1 // Default enable or disable
  TM_Cmd(PWM0_PORT, DISABLE);
  #endif
}
 
/*********************************************************************************************************//**
  * @brief  Set PWM Frequency
  * @param  uReload: Reload value of timer)
  * @retval None
  ***********************************************************************************************************/
void PWM_SetFreq(u32 uReload)
{
  TM_SetCounterReload(PWM0_PORT, uReload);
}

/*********************************************************************************************************//**
  * @brief  Update PWM Duty
  * @param  TM_CH_n: Specify the TM channel.
  * @param  uCompare: PWM duty (Compare value of timer)
  * @retval None
  ***********************************************************************************************************/
void PWM_UpdateDuty(TM_CH_Enum TM_CH_n, u32 uCompare)
{
	#if (USE_PWM0_DMA == 1)
	CCR_Buffer = uCompare;
	#else
  TM_SetCaptureCompare(PWM0_PORT, TM_CH_n, uCompare);
	#endif
}

/*********************************************************************************************************//**
  * @brief Enable or Disable PWM.
  * @param NewState: This parameter can be ENABLE or DISABLE.
  * @retval None
	* @note: if NewState is equal DISABLE, user must set duty to zero first
  ***********************************************************************************************************/
void PWM_Cmd(ControlStatus NewState)
{
	if(NewState == DISABLE) PWM_UpdateDuty(BUZZER_CH, 0);
	TM_Cmd(PWM0_PORT, NewState);
}

/*********************************************************************************************************//**
 * @brief   This function handles PWM0 Handler.
 * @retval  None
 ************************************************************************************************************/
void PWM0_IRQHandler(void)
{
	TM_ClearFlag(PWM0_PORT, TM_INT_UEV);
	if(Buzzer_type.beeTime > 0)
	{
		if(--Buzzer_type.beeTime == 0)
			PWM_Cmd(DISABLE);
	}
}

/*********************************************************************************************************//**
  * @brief Added function for Buzzer
  * @param 
  * @retval None
  ***********************************************************************************************************/
void Buzzer_Init(void)
{
	Buzzer_type.beeTime = 0;
	PWM_Init();
}

void Buzzer_SetBeep(uint16_t timeOn, uint16_t period, uint8_t beeTime)
{
	Buzzer_type.timeOn = timeOn;
	Buzzer_type.period = period;
	Buzzer_type.beeTime = beeTime;
	
	PWM_Cmd(DISABLE);
	if(timeOn == 0) return; 
	PWM_SetFreq(Buzzer_type.period);
	PWM_UpdateDuty(BUZZER_CH, Buzzer_type.timeOn);
	PWM_Cmd(ENABLE);
}
