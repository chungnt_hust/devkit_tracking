/*
 * EN25Q.c
 *
 *  Created on: Jan 8, 2021
 *      Author: chungnt@epi-tech.com.vn
 */
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "EN25Qxxx.h"

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
#define FLASH_INDEX		 			SPI1_INDEX
#define FLASH_SEL_ACTIVE 		SPI1_SEL_ACTIVE
#define FLASH_SEL_INACTIVE 	SPI1_SEL_INACTIVE
/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
#define writeEN   		0x06 	// Write enable instruction  
#define writeDIS  		0x04 	// Write disable instruction  
#define readDAT   		0x03 	// Read Data instruction 
#define writePage 		0x02 	// Program the selected page instruction 
#define sector4k  		0x20 	// Sector Erase instruction 4kB
#define block32k  		0x52 	// Haft lock Erase instruction 32k-byte
#define block64k  		0xD8 	// Block Erase instruction 64k-byte
#define chipEra   		0x60 	// Chip Erase instruction 
#define RDSR      		0x05 	// Read Status Register instruction  
#define WRSR      		0x01 	// Write Status Register instruction
#define RMDID			  	0x90 	// Manufacturer/Device ID
#define RDID					0x9F	// Read Manufacturer Identification
#define BUSY_FLAG 		0x01 	// Write operation in progress 

/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static uint8_t WaitForWriteEnd(void);
static void EN25Q_writeEnable(void);
static void EN25Q_writeDisable(void);
static void EN25Q_writePage(uint32_t add, uint8_t *tx_DATA, uint32_t len);
static uint8_t EN25Q_CheckFirstRun(void);
/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void EN25Q_Init(void)
{
	Spi_Configuration(FLASH_INDEX, SPI_DATALENGTH_8, 0);
	
	EN25Q_readMfrID();
	if(EN25Q_CheckFirstRun() == 1) DEBUG("[EN25Q] First run\r\n");
	else DEBUG("[EN25Q] Not first run\r\n");
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void EN25Q_readData(uint32_t add, void *rx_DATA, uint32_t len)
{
	uint8_t *p;
	uint8_t arr[4];
	arr[0] = readDAT;
	arr[1] = (add&0x00FF0000)>>16;
	arr[2] = (add&0x0000FF00)>>8;
	arr[3] = add&0x000000FF;
	
	FLASH_SEL_ACTIVE;
	Spi_SendMulti(FLASH_INDEX, arr, 4);
	
	p = (uint8_t*)rx_DATA;
	Spi_ReceiveMulti(FLASH_INDEX, p, len);
	
	FLASH_SEL_INACTIVE;
	//HAL_Delay(100);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void EN25Q_writeData(uint32_t add, void *tx_DATA, uint32_t len)
{
	uint32_t offsetAdd;
	uint32_t dataSize;
	uint32_t dataDone = 0;
	uint8_t *p;
	
	while(len)
	{
		offsetAdd = add%256;
		if(offsetAdd + len > 256) dataSize = 256 - offsetAdd;
		else dataSize = len;
			
		p = (uint8_t*)tx_DATA;
		EN25Q_writePage(add, (uint8_t*)&p[dataDone], dataSize);
		add += dataSize;
		dataDone += dataSize;
		len -= dataSize;		
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void EN25Q_erase4k(uint32_t add)
{
	uint8_t arr[4];
	arr[0] = sector4k;
	arr[1] = (add&0x00FF0000)>>16;
	arr[2] = (add&0x0000FF00)>>8;
	arr[3] = add&0x000000FF;
		
	EN25Q_writeEnable();
	
	FLASH_SEL_ACTIVE;	
	Spi_SendMulti(FLASH_INDEX, arr, 4);
	FLASH_SEL_INACTIVE;
	WaitForWriteEnd();
	
	EN25Q_writeDisable();
	HAL_Delay(100);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void EN25Q_erase32k(uint32_t add)
{
	uint8_t arr[4];
	arr[0] = block32k;
	arr[1] = (add&0x00FF0000)>>16;
	arr[2] = (add&0x0000FF00)>>8;
	arr[3] = add&0x000000FF;
	
	EN25Q_writeEnable();
	
	FLASH_SEL_ACTIVE;	
	Spi_SendMulti(FLASH_INDEX, arr, 4);
	FLASH_SEL_INACTIVE;
	WaitForWriteEnd();
	
	EN25Q_writeDisable();
	HAL_Delay(500);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void EN25Q_erase64k(uint32_t add)
{
	uint8_t arr[4];
	arr[0] = block64k;
	arr[1] = (add&0x00FF0000)>>16;
	arr[2] = (add&0x0000FF00)>>8;
	arr[3] = add&0x000000FF;
	
	FLASH_SEL_ACTIVE;
	
	EN25Q_writeEnable();
	
	FLASH_SEL_ACTIVE;	
	Spi_SendMulti(FLASH_INDEX, arr, 4);
	FLASH_SEL_INACTIVE;
	WaitForWriteEnd();
	
	EN25Q_writeDisable();
	HAL_Delay(1000);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void EN25Q_eraseChip(void)
{
	EN25Q_writeEnable();
	
	FLASH_SEL_ACTIVE;
	Spi_SendReceive(FLASH_INDEX, chipEra);
	FLASH_SEL_INACTIVE;
	WaitForWriteEnd();
	
	EN25Q_writeDisable();
	HAL_Delay(2000);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void EN25Q_readMfrID(void)
{
	uint8_t Mfr, ID;
	
	FLASH_SEL_ACTIVE;
	Spi_SendReceive(FLASH_INDEX, RMDID);
	
	Spi_SendReceive(FLASH_INDEX, 0);
	Spi_SendReceive(FLASH_INDEX, 0);
	Spi_SendReceive(FLASH_INDEX, 0);
	
	Mfr = Spi_SendReceive(FLASH_INDEX, 0);
	ID = Spi_SendReceive(FLASH_INDEX, 0);
	FLASH_SEL_INACTIVE;
	
	DEBUG("[EN25Q] MfrID = %02x, Device ID = %02x (Refer to Table 6)\r\n", Mfr, ID);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void EN25Q_readMID(void)
{
	uint8_t ID;
	
	FLASH_SEL_ACTIVE;
	Spi_SendReceive(FLASH_INDEX, RDID);
	ID = Spi_SendReceive(FLASH_INDEX, 0);
	FLASH_SEL_INACTIVE;
	
	DEBUG("[EN25Q] MfrID = %02x\r\n", ID);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
typedef struct
{
	uint8_t x1;
	uint32_t x2;
} dataType_t;
static dataType_t tx, rx;

static void init(void)
{
	tx.x1 = 156;
	tx.x2 = 11;
	rx.x1 = 1;
	rx.x2 = 1;
}

void EN25Q_selftest(void)
{
	init();

	EN25Q_writeData(0x01, &tx, sizeof(tx));

	EN25Q_readData(0x01, &rx, sizeof(rx));
	DEBUG("[EN25Q] rx. = %d, %d\r\n", rx.x1, rx.x2);

}

/* ====================== private functions =========================== */
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static uint8_t WaitForWriteEnd(void)
{
  u8 FLASH_Status = 0;

  /* Select the SPI FLASH                                                                                   */
  FLASH_SEL_ACTIVE;

  /* Send "Read Status Register" instruction                                                                */
  Spi_SendReceive(FLASH_INDEX, RDSR);

  /* Loop as long as the busy flag is set                                                                   */
  do
  {
    /* Send a dummy byte to generate the clock to read the value of the status register                     */
    FLASH_Status = Spi_SendReceive(FLASH_INDEX, 0);

  } while((FLASH_Status & BUSY_FLAG) == SET);

  /* Deselect the SPI FLASH                                                                                 */
  FLASH_SEL_INACTIVE;

  return FLASH_Status;
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void EN25Q_writeEnable(void)
{
	FLASH_SEL_ACTIVE;
	Spi_SendReceive(FLASH_INDEX, writeEN);
	FLASH_SEL_INACTIVE; 
	
	WaitForWriteEnd();
	// Chip Select (CS#) must be driven High after the last bit of the instruction sequence has been shifted in
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void EN25Q_writeDisable(void)
{
	FLASH_SEL_ACTIVE;
	Spi_SendReceive(FLASH_INDEX, writeDIS);
	FLASH_SEL_INACTIVE;
	
	WaitForWriteEnd();
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
/* Neu ghi nhieu byte can chu y den vi tri cua byte dang ghi o vi tri nao cua 1 page
 * neu > 255 thi no se quay lai dau page va ghi de du lieu len => Nen ghi tu dia chi 
 * co byte cuoi la 00h
 * Neu ghi 1 byte thi thoai mai */ 
static void EN25Q_writePage(uint32_t add, uint8_t *tx_DATA, uint32_t len)
{
	uint8_t arr[4];
	arr[0] = writePage;
	arr[1] = (add&0x00FF0000)>>16;
	arr[2] = (add&0x0000FF00)>>8;
	arr[3] = add&0x000000FF;
	
	EN25Q_writeEnable();
	
	FLASH_SEL_ACTIVE;
	Spi_SendMulti(FLASH_INDEX, arr, 4);	
	Spi_SendMulti(FLASH_INDEX, tx_DATA, len);
	FLASH_SEL_INACTIVE;
	WaitForWriteEnd();
	
	EN25Q_writeDisable();
	
	//HAL_Delay(100);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static uint8_t EN25Q_CheckFirstRun(void)
{
	uint8_t bkDat;
	EN25Q_readData(FLASH_FIRST_RUN, (uint8_t*)&bkDat, 1);
	if(bkDat != FLASH_BACKUP_DATA)
	{
		DEBUG("[EN25Q] Init for first run\r\n");
		EN25Q_eraseChip();
		bkDat = FLASH_BACKUP_DATA;
		EN25Q_writeData(FLASH_FIRST_RUN, (uint8_t*)&bkDat, 1);
		
		EN25Q_selftest();
		return 1;
	}
	return 0;
}
