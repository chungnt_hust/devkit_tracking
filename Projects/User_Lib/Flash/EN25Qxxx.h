/*
 * EN25Q.h
 *
 *  Created on: Jan 8, 2021
 *      Author: chungnguyen
 *
*/
#ifndef  __EN25Q_H__
#define  __EN25Q_H__
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */

/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */

/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void EN25Q_Init(void);
void EN25Q_readData(uint32_t add, void *rx_DATA, uint32_t len);
void EN25Q_writeData(uint32_t add, void *tx_DATA, uint32_t len);
void EN25Q_erase4k(uint32_t add);
void EN25Q_erase32k(uint32_t add);
void EN25Q_erase64k(uint32_t add);
void EN25Q_eraseChip(void);
void EN25Q_readMfrID(void);
void EN25Q_readMID(void);

void EN25Q_selftest(void);
#endif
