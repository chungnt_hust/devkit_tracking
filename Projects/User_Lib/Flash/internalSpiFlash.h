/*
 * internalSpiFlash.h
 *
 *  Created on: Sep 5, 2019
 *      Author: chungnguyen
 *
*/
#ifndef  __INTERNALSPIFLASH_H__
#define  __INTERNALSPIFLASH_H__
#include "ht32.h"

void InternalFSPI_readData(uint32_t add, void *rx_DATA, uint32_t len);
void InternalFSPI_writeData(uint32_t add, void *tx_DATA, uint32_t len);
void InternalFSPI_erase4k(uint32_t add);
void InternalFSPI_erase32k(uint32_t add);
void InternalFSPI_eraseChip(void);
u32 InternalFSPI_readJEDECID(void);

void InternalFSPI_selftest(void);
#endif
