/*
 * InternalFSPI.c
 *
 *  Created on: Jan 8, 2021
 *      Author: chungnt@epi-tech.com.vn
 */
/**** include: internalSpiFlash.h ****/
#include "internalSpiFlash.h"///
#include "spiUser.h"
#include "gpioUser.h"
#include "systemClockCfg.h"
#include "uartUser.h"

static uint8_t writeEN     = 0x06; // Write enable instruction  
static uint8_t writeDIS    = 0x04; // Write disable instruction  
static uint8_t readDAT     = 0x03; // Read Data instruction 
static uint8_t writePage   = 0x02; // Program the selected page instruction 
static uint8_t sector4k    = 0x20; // Sector Erase instruction 4kB
static uint8_t block32k    = 0x52; // Block Erase instruction 42k-byte
static uint8_t chipEra     = 0x60; // Chip Erase instruction 
static uint8_t wSR1        = 0x01; // Write Status Register instruction
static uint8_t rSR1        = 0x05; // Read Status Register instruction  
static uint8_t rdid        = 0x9F; // Read JEDEC ID instruction                                
static uint8_t rems        = 0x90; // Read electronic manufacturer & device ID instruction     

static uint8_t BUSY_FLAG   = 0x01; // Write operation in progress                              

static u8 WaitForWriteEnd(void)
{
  u8 FLASH_Status = 0;

  /* Select the SPI FLASH                                                                                   */
  SPI1_SEL_ACTIVE;

  /* Send "Read Status Register" instruction                                                                */
  spiUser_SendMulti(SPI1_INDEX, &rSR1, 1);

  /* Loop as long as the busy flag is set                                                                   */
  do
  {
    /* Send a dummy byte to generate the clock to read the value of the status register                     */
    FLASH_Status = spiUser_SendReceive(SPI1_INDEX, 0);

  } while((FLASH_Status & BUSY_FLAG) == SET);

  /* Deselect the SPI FLASH                                                                                 */
  SPI1_SEL_INACTIVE;

  return FLASH_Status;
}

// Ham gui lenh cho phep ghi 
static void InternalFSPI_writeEnable()
{
	SPI1_SEL_ACTIVE;
	spiUser_SendMulti(SPI1_INDEX, &writeEN, 1);
	SPI1_SEL_INACTIVE; 
	// Chip Select (CS#) must be driven High after the last bit of the instruction sequence has been shifted in
}

// Ham gui lenh ko cho phep ghi
static void InternalFSPI_writeDisable()
{
	SPI1_SEL_ACTIVE;
	spiUser_SendMulti(SPI1_INDEX, &writeDIS, 1);
	SPI1_SEL_INACTIVE;
}

// Ham doc du lieu
void InternalFSPI_readData(uint32_t add, void *rx_DATA, uint32_t len)
{
	uint8_t *p;
	uint8_t arr[3];
	arr[0] = (add&0x00FF0000)>>16;
	arr[1] = (add&0x0000FF00)>>8;
	arr[2] = add&0x000000FF;
	
	SPI1_SEL_ACTIVE;
	spiUser_SendMulti(SPI1_INDEX, &readDAT, 1);
	SPI1_SEL_INACTIVE;
	SPI1_SEL_ACTIVE;
	spiUser_SendMulti(SPI1_INDEX, arr, 3);
	
	p = (uint8_t*)rx_DATA;
	spiUser_ReceiveMulti(SPI1_INDEX, p, len);
	
	SPI1_SEL_INACTIVE;
	//HAL_Delay(100);
	SysClockDelay_ms(100);
}

/* Neu ghi nhieu byte can chu y den vi tri cua byte dang ghi o vi tri nao cua 1 page
 * neu > 255 thi no se quay lai dau page va ghi de du lieu len => Nen ghi tu dia chi 
 * co byte cuoi la 00h
 * Neu ghi 1 byte thi thoai mai :D */ 
static void InternalFSPI_writePage(uint32_t add, uint8_t *tx_DATA, uint32_t len)
{
	uint8_t arr[3];
	arr[0] = (add&0x00FF0000)>>16;
	arr[1] = (add&0x0000FF00)>>8;
	arr[2] = add&0x000000FF;
	
	InternalFSPI_writeEnable();
	SPI1_SEL_ACTIVE;
	spiUser_SendMulti(SPI1_INDEX, &writePage, 1);
	SPI1_SEL_INACTIVE;
	SPI1_SEL_ACTIVE;
	spiUser_SendMulti(SPI1_INDEX, arr, 3);
	
	spiUser_SendMulti(SPI1_INDEX, tx_DATA, len);
	
	SPI1_SEL_INACTIVE;
	InternalFSPI_writeDisable();
	
	//SysClockDelay_ms(100);
	WaitForWriteEnd();
}

void InternalFSPI_writeData(uint32_t add, void *tx_DATA, uint32_t len)
{
	uint32_t offsetAdd;
	uint32_t luongDuLieuPhaiGhi;
	uint32_t luongDuLieuDaGhi = 0;
	uint8_t *p;
	
	while(len)
	{
		offsetAdd = add%256;
		if(offsetAdd + len > 256) luongDuLieuPhaiGhi = 256 - offsetAdd;
		else luongDuLieuPhaiGhi = len;
			
		p = (uint8_t*)tx_DATA;
		InternalFSPI_writePage(add, (uint8_t*)&p[luongDuLieuDaGhi], luongDuLieuPhaiGhi);
		add += luongDuLieuPhaiGhi;
		luongDuLieuDaGhi += luongDuLieuPhaiGhi;
		len -= luongDuLieuPhaiGhi;		
	}
}

void InternalFSPI_erase4k(uint32_t add)
{
	uint8_t arr[3];
	arr[0] = (add&0x00FF0000)>>16;
	arr[1] = (add&0x0000FF00)>>8;
	arr[2] = add&0x000000FF;
	
	InternalFSPI_writeEnable();
	SPI1_SEL_ACTIVE;
	spiUser_SendMulti(SPI1_INDEX, &sector4k, 1);
	SPI1_SEL_INACTIVE;
	SPI1_SEL_ACTIVE;
	spiUser_SendMulti(SPI1_INDEX, arr, 3);
	SPI1_SEL_INACTIVE;
	InternalFSPI_writeDisable();
	
	//SysClockDelay_ms(100);
	WaitForWriteEnd();
}

void InternalFSPI_erase32k(uint32_t add)
{
	uint8_t arr[3];
	arr[0] = (add&0x00FF0000)>>16;
	arr[1] = (add&0x0000FF00)>>8;
	arr[2] = add&0x000000FF;
	
	InternalFSPI_writeEnable();
	SPI1_SEL_ACTIVE;
	spiUser_SendMulti(SPI1_INDEX, &block32k, 1);
	spiUser_SendMulti(SPI1_INDEX, arr, 3);
	SPI1_SEL_INACTIVE;
	InternalFSPI_writeDisable();

  //SysClockDelay_ms(1000);
	WaitForWriteEnd();
}

void InternalFSPI_eraseChip(void)
{
	InternalFSPI_writeEnable();
	SPI1_SEL_ACTIVE;
	spiUser_SendMulti(SPI1_INDEX, &chipEra, 1);
	SPI1_SEL_INACTIVE;
	InternalFSPI_writeDisable();
 
	//SysClockDelay_ms(2000);
	WaitForWriteEnd();
}

u32 InternalFSPI_readJEDECID(void)
{
	uint8_t arr[3];

	InternalFSPI_writeEnable();
	SPI1_SEL_ACTIVE;
	spiUser_SendReceive(SPI1_INDEX, rdid);
	spiUser_ReceiveMulti(SPI1_INDEX, arr, 3);
	SPI1_SEL_INACTIVE;
	InternalFSPI_writeDisable();
	
	return ((arr[0] << 16) | (arr[1] << 8) | arr[2]);
}
//typedef struct
//{
//	uint8_t x1;
//	uint32_t x2;
//} dataType_t;
//static dataType_t tx, rx;

//static void init(void)
//{
//	tx.x1 = 123;
//	tx.x2 = 11;
//	rx.x1 = 1;
//	rx.x2 = 1;
//}

void InternalFSPI_selftest(void)
{
	uint8_t datTX = 0x55, datRX = 0;
	InternalFSPI_eraseChip();
	InternalFSPI_writeData(0x00, &datTX, sizeof(datTX));
	
	InternalFSPI_readData(0x00, &datRX, sizeof(datRX));
	DEBUG("datRX = %02x\r\n", datRX);
	
}
/****************/
