/*
 * MemoryMap.h
 *
 *  Created on: Jan 8, 2021
 *      Author: chungnguyen
 *
*/
#ifndef  __MEMORYMAP_H__
#define  __MEMORYMAP_H__
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
/* For Init Flash */
#define FLASH_FIRST_RUN 	0x000000 // Block64k 0, Block32k 0-1, Sector 0-15
#define FLASH_BACKUP_DATA 0xAA

/* TCP Area */
#define FLASH_TCP_AREA    0x010000 // Block64k 1, Block32k 2-3, Sector 16

/* FOTA Area */
#define FLASH_APP1_BASE		0x020000 // Block64k 2, 3, 4, 5 ~ 256K
#define FLASH_APP1_1			(FLASH_APP1_BASE+0x000000)
#define FLASH_APP1_2			(FLASH_APP1_BASE+0x010000)
#define FLASH_APP1_3			(FLASH_APP1_BASE+0x020000)
#define FLASH_APP1_4			(FLASH_APP1_BASE+0x030000)

#define FLASH_APP2_BASE		0x060000 // Block64k 6, 7, 8, 9 ~ 256K
#define FLASH_APP2_1			(FLASH_APP2_BASE+0x000000)
#define FLASH_APP2_2			(FLASH_APP2_BASE+0x010000)
#define FLASH_APP2_3			(FLASH_APP2_BASE+0x020000)
#define FLASH_APP2_4			(FLASH_APP2_BASE+0x030000)
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */

/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
#endif
