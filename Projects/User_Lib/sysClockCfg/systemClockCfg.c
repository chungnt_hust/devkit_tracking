/*
 * systemClock.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn

    Configure the system clock in startup file (startup_ht32fxxxx.s)
    by calling SystemInit function. Please refer to system_ht32fxxxx.c.

    If no system clock is configured, HSI 8MHz is the system clock by default.

    Configure system clock frequency and HCLK prescaler.
    External (4-16MHz) 8 MHz crystal is used to drive the system clock.

    Use FLASH_SetWaitState (FMCEN) if enabled during Sleep mode
    Set and reset by software. Users can set FMCEN as 0 to reduce power consumption
    if the Flash Memory is unused during Sleep mode
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "systemClockCfg.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
sysTickCB funcSysTickCB;
volatile u32 timeDelay = 0;

/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */

/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void SysClockCfg_RegCallback(sysTickCB func)
{
	CKCU_ClocksTypeDef ClockFreq;
	CKCU_GetClocksFrequency(&ClockFreq);
  /* SYSTICK configuration */
  SYSTICK_ClockSourceConfig(SYSTICK_SRC_STCLK);      			// Default : CK_AHB/8
  SYSTICK_SetReloadValue(ClockFreq.HCLK_Freq / 8 / 1000); // (CK_AHB/8/1000) = 1ms on chip
  SYSTICK_IntConfig(ENABLE);                          		// Enable SYSTICK Interrupt
  SYSTICK_CounterCmd(SYSTICK_COUNTER_CLEAR);
  SYSTICK_CounterCmd(SYSTICK_COUNTER_ENABLE);
  funcSysTickCB = func;
}

/* ==================================================================== */
void SysTick_Handler(void)
{
  if(funcSysTickCB != NULL) funcSysTickCB();
	if(timeDelay > 0) timeDelay--;
}
/* ==================================================================== */
void SysClockGetCK_AHB(void)
{
	CKCU_ClocksTypeDef ClockFreq;
	CKCU_GetClocksFrequency(&ClockFreq);
	DEBUG("CK_AHB: %2d[MHz]\r\n", ClockFreq.HCLK_Freq/1000000);
}

void HAL_Delay(u32 time)
{
  timeDelay = time;
  while(timeDelay > 0);
}

void WDT_Configuration(u16 mSec)
{
  CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
  CKCUClock.Bit.WDT = 1;
  CKCU_PeripClockConfig(CKCUClock, ENABLE);

  /* Enable WDT APB clock                                                                                   */
  /* Reset WDT                                                                                              */
  WDT_DeInit();
  /* Set Prescaler Value, wdt frequency LSI_RC/Prescaler = 32kHz/32 = 1kHz (1ms)                                                                  */
  WDT_SetPrescaler(WDT_PRESCALER_32);
  /* Set Prescaler Value,                                                          */
  WDT_SetReloadValue(mSec);
  /* Set Delta Value,                                                         */
  WDT_SetDeltaValue(mSec);
	/* Enable watchdog reset when underflow or error */
	WDT_ResetCmd(ENABLE);
  WDT_Restart();                    // Reload Counter as WDTV Value
  WDT_Cmd(ENABLE);                  // Enable WDT
//  WDT_ProtectCmd(ENABLE);           // Enable WDT Protection
}

char* SystemClockCfg_GetResetReason(void)
{
	if(RSTCU_GetResetFlagStatus(RSTCU_FLAG_SYSRST) == SET) 
	{
		RSTCU_ClearResetFlag(RSTCU_FLAG_SYSRST);
		return SYSRST;
	}
	else if(RSTCU_GetResetFlagStatus(RSTCU_FLAG_EXTRST) == SET) 
	{
		RSTCU_ClearResetFlag(RSTCU_FLAG_EXTRST);
		return EXTRST;
	}
	else if(RSTCU_GetResetFlagStatus(RSTCU_FLAG_WDTRST) == SET) 
	{
	   RSTCU_ClearResetFlag(RSTCU_FLAG_WDTRST);
		 return WDTRST;
	}
	else if(RSTCU_GetResetFlagStatus(RSTCU_FLAG_PORST) == SET) 
	{
		RSTCU_ClearResetFlag(RSTCU_FLAG_PORST);
		return PORST;
	}
	return "DEFAULT";
}
