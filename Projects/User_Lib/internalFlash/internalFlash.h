/*
 * internalFlash.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef INTERNALFLASH_H
#define INTERNALFLASH_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
typedef union
{
	uint8_t _byte[4];
	uint32_t _data;
} dataFlash_t;
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void InternalFlash_write(uint8_t PageNum, void *tx_DATA, uint32_t len);
void InternalFlash_read(uint8_t PageNum, void *rx_DATA, uint32_t len);
uint32_t InternalFlash_readMIDI(void);
#endif
#ifdef __cplusplus
}
#endif
