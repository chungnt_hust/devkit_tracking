/*
 * internalFlash.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "internalFlash.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */


/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/*********************************************************************************************************//**
  * @brief Program FLASH Byte page.
  * @retval None
 ************************************************************************************************************/
void InternalFlash_write(uint8_t PageNum, void *tx_DATA, uint32_t len)
{
  uint32_t StartAddress = 1024 * PageNum;
	uint8_t offset = (len / 1024) + PageNum + 1;
	uint32_t EndAddress = 1024 * offset;
	uint32_t Addr;
	dataFlash_t dataFlash;
	uint8_t index;
	
  FLASH_State FLASHState;
	
	uint8_t *p;
	p = (uint8_t*)tx_DATA;
	
	for(Addr = StartAddress; Addr < EndAddress; Addr += FLASH_PAGE_SIZE)
  {
    FLASHState = FLASH_ErasePage(Addr);
    if(FLASHState != FLASH_COMPLETE)
    {
      /* error */
    }
  }
	
  for(Addr = StartAddress; Addr < StartAddress + len; Addr += 4)
  {
		for(index = 0; index < 4; index++) dataFlash._byte[index] = *p++;
    FLASHState = FLASH_ProgramWordData(Addr, dataFlash._data);
    if(FLASHState != FLASH_COMPLETE)
    {
      /* error */
    }
  }
}

/*********************************************************************************************************//**
  * @brief Read FLASH Byte page.
  * @retval None
 ************************************************************************************************************/
void InternalFlash_read(uint8_t PageNum, void *rx_DATA, uint32_t len)
{
	uint32_t StartAddress = 1024 * PageNum;
	uint32_t Addr;
	dataFlash_t dataFlash;
	uint8_t index;
	
	uint8_t *p;
	p = (uint8_t*)rx_DATA;
	
  for(Addr = StartAddress; Addr < StartAddress + len; Addr += 4)
  {
    dataFlash._data = rw(Addr);
		for(index = 0; index < 4; index++) *p++ = dataFlash._byte[index];
  }
} 

/*********************************************************************************************************//**
  * @brief Read Flash Manufacturer and Device ID Register � MDID.
  * @retval None
 ************************************************************************************************************/
uint32_t InternalFlash_readMIDI(void)
{
	return HT_FLASH->MDID;
}
