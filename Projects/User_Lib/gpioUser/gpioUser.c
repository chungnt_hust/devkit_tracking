/*
 * gpioUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 * All GPIO pins can be selected as EXTI trigger source
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "gpioUser.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */

void (*irqHldFunction)(u16 Data);

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
volatile uint8_t gBtnState = 0;


/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
  * @brief  Configure the GPIO ports in/out
  * @retval None
  ***********************************************************************************************************/
void Gpio_Config(void)
{
	/* Configure EXTI Channel n  */
	//EXTI_InitTypeDef EXTI_InitStruct;
	/* Configure the GPIO ports. */
	CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
	
	CKCUClock.Bit.DEBUG_LED_BIT_CLK 		= 1;
	CKCUClock.Bit.CELL_LED_BIT_CLK 			= 1;
	CKCUClock.Bit.GNSS_LED_BIT_CLK 			= 1;
	CKCUClock.Bit.ALERT_BIT_CLK 				= 1;
	CKCUClock.Bit.CELL_VBAT_ON_BIT_CLK	= 1;
	CKCUClock.Bit.CELL_PWR_ON_BIT_CLK		= 1;
	CKCUClock.Bit.CELL_DTR_BIT_CLK      = 1;
	CKCUClock.Bit.CELL_RI_BIT_CLK 			= 1;
	CKCUClock.Bit.CELL_STT_BIT_CLK 			= 1;
	CKCUClock.Bit.GNSS_PWR_BIT_CLK 			= 1;
	CKCUClock.Bit.GNSS_PPS_BIT_CLK 			= 1;
	CKCUClock.Bit.ACC_INT_BIT_CLK 			= 1;
	CKCUClock.Bit.NFC_INT_BIT_CLK 			= 1;
	CKCUClock.Bit.NFC_RST_BIT_CLK 			= 1;
	CKCUClock.Bit.LOG_TX_BIT_CLK 				= 1;
	
	//CKCUClock.Bit.EXTI         					= 1;
	CKCUClock.Bit.AFIO         					= 1;
	CKCU_PeripClockConfig(CKCUClock, ENABLE);
	/***************************  DEBUG_LED  ***************************************//**/
	AFIO_GPxConfig(DEBUG_LED_PORTNUM, DEBUG_LED_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(DEBUG_LED_PORT, DEBUG_LED_PIN, GPIO_DIR_OUT);
	GPIO_PullResistorConfig(DEBUG_LED_PORT, DEBUG_LED_PIN, GPIO_PR_UP);
	GPIO_WriteOutBits(DEBUG_LED_PORT, DEBUG_LED_PIN, LED_OFF);
		/***************************  CELL_LED  ***************************************//**/
	AFIO_GPxConfig(CELL_LED_PORTNUM, CELL_LED_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(CELL_LED_PORT, CELL_LED_PIN, GPIO_DIR_OUT);
	GPIO_PullResistorConfig(CELL_LED_PORT, CELL_LED_PIN, GPIO_PR_UP);
	GPIO_WriteOutBits(CELL_LED_PORT, CELL_LED_PIN, LED_OFF);
		/***************************  GNSS_LED  ***************************************//**/
	AFIO_GPxConfig(GNSS_LED_PORTNUM, GNSS_LED_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(GNSS_LED_PORT, GNSS_LED_PIN, GPIO_DIR_OUT);
	GPIO_PullResistorConfig(GNSS_LED_PORT, GNSS_LED_PIN, GPIO_PR_UP);
	GPIO_WriteOutBits(GNSS_LED_PORT, GNSS_LED_PIN, LED_OFF);
	/***************************  ALERT  ***************************************//**/
	AFIO_GPxConfig(ALERT_PORTNUM, ALERT_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(ALERT_PORT, ALERT_PIN, GPIO_DIR_OUT);
	GPIO_PullResistorConfig(ALERT_PORT, ALERT_PIN, GPIO_PR_UP);
	GPIO_WriteOutBits(ALERT_PORT, ALERT_PIN, RESET);
	/***************************  CELL_VBAT_ON  ***************************************//**/
	AFIO_GPxConfig(CELL_VBAT_ON_PORTNUM, CELL_VBAT_ON_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(CELL_VBAT_ON_PORT, CELL_VBAT_ON_PIN, GPIO_DIR_OUT);
	GPIO_PullResistorConfig(CELL_VBAT_ON_PORT, CELL_VBAT_ON_PIN, GPIO_PR_UP);
	GPIO_WriteOutBits(CELL_VBAT_ON_PORT, CELL_VBAT_ON_PIN, CELL_VBAT_OFF);
	/***************************  CELL_PWR_ON ***************************************//**/
	AFIO_GPxConfig(CELL_PWR_ON_PORTNUM, CELL_PWR_ON_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(CELL_PWR_ON_PORT, CELL_PWR_ON_PIN, GPIO_DIR_OUT);
	GPIO_PullResistorConfig(CELL_PWR_ON_PORT, CELL_PWR_ON_PIN, GPIO_PR_UP);
	GPIO_WriteOutBits(CELL_PWR_ON_PORT, CELL_PWR_ON_PIN, CELL_PWR_OFF);
	/***************************  CELL_DTR ***************************************//**/
	AFIO_GPxConfig(CELL_DTR_PORTNUM, CELL_DTR_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(CELL_DTR_PORT, CELL_DTR_PIN, GPIO_DIR_OUT);
	GPIO_PullResistorConfig(CELL_DTR_PORT, CELL_DTR_PIN, GPIO_PR_UP);
	GPIO_WriteOutBits(CELL_DTR_PORT, CELL_DTR_PIN, RESET);
	/***************************  CELL_RI ***************************************//**/
  AFIO_GPxConfig(CELL_RI_PORTNUM, CELL_RI_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(CELL_RI_PORT, CELL_RI_PIN, GPIO_DIR_IN);
  GPIO_PullResistorConfig(CELL_RI_PORT, CELL_RI_PIN, GPIO_PR_UP);
	GPIO_InputConfig(CELL_RI_PORT, CELL_RI_PIN, ENABLE);
#if 0
	/* Select Port as EXTI Trigger Source */
  AFIO_EXTISourceConfig(CELL_RI_AFIO_EXTI_CH, CELL_RI_EXTIO_SOURCE);	
	EXTI_InitStruct.EXTI_Channel = CELL_RI_EXTI_CHANNEL;
	EXTI_InitStruct.EXTI_Debounce = EXTI_DEBOUNCE_DISABLE;
	EXTI_InitStruct.EXTI_DebounceCnt = 0;
	EXTI_InitStruct.EXTI_IntType = EXTI_POSITIVE_EDGE;
	EXTI_Init(&EXTI_InitStruct);
	/* Enable EXTI & NVIC line Interrupt */
  EXTI_IntConfig(CELL_RI_EXTI_CHANNEL, ENABLE);
  NVIC_EnableIRQ(CELL_RI_EXTI_IRQn);
#endif
	/***************************  CELL_STT ***************************************//**/
  AFIO_GPxConfig(CELL_STT_PORTNUM, CELL_STT_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(CELL_STT_PORT, CELL_STT_PIN, GPIO_DIR_IN);
  GPIO_PullResistorConfig(CELL_STT_PORT, CELL_STT_PIN, GPIO_PR_DOWN);
	GPIO_InputConfig(CELL_STT_PORT, CELL_STT_PIN, ENABLE);
	/***************************  GNSS_PWR ***************************************//**/
	AFIO_GPxConfig(GNSS_PWR_PORTNUM, GNSS_PWR_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(GNSS_PWR_PORT, GNSS_PWR_PIN, GPIO_DIR_OUT);
	GPIO_PullResistorConfig(GNSS_PWR_PORT, GNSS_PWR_PIN, GPIO_PR_UP);
	GPIO_WriteOutBits(GNSS_PWR_PORT, GNSS_PWR_PIN, GNSS_PWR_OFF);	
	/***************************  GNSS_PPS ***************************************//**/
	AFIO_GPxConfig(GNSS_PPS_PORTNUM, GNSS_PPS_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(GNSS_PPS_PORT, GNSS_PPS_PIN, GPIO_DIR_IN);
  GPIO_PullResistorConfig(GNSS_PPS_PORT, GNSS_PPS_PIN, GPIO_PR_UP);
	GPIO_InputConfig(GNSS_PPS_PORT, GNSS_PPS_PIN, ENABLE);
#if 0
	/* Select Port as EXTI Trigger Source */
  AFIO_EXTISourceConfig(GNSS_PPS_AFIO_EXTI_CH, GNSS_PPS_EXTIO_SOURCE);	
	EXTI_InitStruct.EXTI_Channel = GNSS_PPS_EXTI_CHANNEL;
	EXTI_InitStruct.EXTI_Debounce = EXTI_DEBOUNCE_DISABLE;
	EXTI_InitStruct.EXTI_DebounceCnt = 0;
	EXTI_InitStruct.EXTI_IntType = EXTI_POSITIVE_EDGE;
	EXTI_Init(&EXTI_InitStruct);
	/* Enable EXTI & NVIC line Interrupt */
  EXTI_IntConfig(GNSS_PPS_EXTI_CHANNEL, ENABLE);
  NVIC_EnableIRQ(GNSS_PPS_EXTI_IRQn);
#endif
	/***************************  ACC_INT ***************************************//**/
	AFIO_GPxConfig(ACC_INT_PORTNUM, ACC_INT_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(ACC_INT_PORT, ACC_INT_PIN, GPIO_DIR_IN);
  GPIO_PullResistorConfig(ACC_INT_PORT, ACC_INT_PIN, GPIO_PR_UP);
	GPIO_InputConfig(ACC_INT_PORT, ACC_INT_PIN, ENABLE);
#if 0
	/* Select Port as EXTI Trigger Source */
  AFIO_EXTISourceConfig(ACC_INT_AFIO_EXTI_CH, ACC_INT_EXTIO_SOURCE);	
	EXTI_InitStruct.EXTI_Channel = ACC_INT_EXTI_CHANNEL;
	EXTI_InitStruct.EXTI_Debounce = EXTI_DEBOUNCE_DISABLE;
	EXTI_InitStruct.EXTI_DebounceCnt = 0;
	EXTI_InitStruct.EXTI_IntType = EXTI_POSITIVE_EDGE;
	EXTI_Init(&EXTI_InitStruct);
	/* Enable EXTI & NVIC line Interrupt */
  EXTI_IntConfig(ACC_INT_EXTI_CHANNEL, ENABLE);
  NVIC_EnableIRQ(ACC_INT_EXTI_IRQn);
#endif
	/***************************  NFC_INT ***************************************//**/
	AFIO_GPxConfig(NFC_INT_PORTNUM, NFC_INT_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(NFC_INT_PORT, NFC_INT_PIN, GPIO_DIR_IN);
  GPIO_PullResistorConfig(NFC_INT_PORT, NFC_INT_PIN, GPIO_PR_DOWN);
	GPIO_InputConfig(NFC_INT_PORT, NFC_INT_PIN, ENABLE);
#if 0
	/* Select Port as EXTI Trigger Source */
  AFIO_EXTISourceConfig(NFC_INT_AFIO_EXTI_CH, NFC_INT_EXTIO_SOURCE);	
	EXTI_InitStruct.EXTI_Channel = NFC_INT_EXTI_CHANNEL;
	EXTI_InitStruct.EXTI_Debounce = EXTI_DEBOUNCE_DISABLE;
	EXTI_InitStruct.EXTI_DebounceCnt = 0;
	EXTI_InitStruct.EXTI_IntType = EXTI_POSITIVE_EDGE;
	EXTI_Init(&EXTI_InitStruct);
	/* Enable EXTI & NVIC line Interrupt */
  EXTI_IntConfig(NFC_INT_EXTI_CHANNEL, ENABLE);
  NVIC_EnableIRQ(NFC_INT_EXTI_IRQn);
#endif
	/***************************  NFC_RST ***************************************//**/
	AFIO_GPxConfig(NFC_RST_PORTNUM, NFC_RST_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(NFC_RST_PORT, NFC_RST_PIN, GPIO_DIR_OUT);
	GPIO_PullResistorConfig(NFC_RST_PORT, NFC_RST_PIN, GPIO_PR_UP);
	GPIO_WriteOutBits(NFC_RST_PORT, NFC_RST_PIN, RESET);
		/***************************  LOG_TX ***************************************//**/
	AFIO_GPxConfig(LOG_TX_PORTNUM, LOG_TX_AFIO_PIN, AFIO_FUN_GPIO);
	GPIO_DirectionConfig(LOG_TX_PORT, LOG_TX_PIN, GPIO_DIR_IN);
  GPIO_PullResistorConfig(LOG_TX_PORT, LOG_TX_PIN, GPIO_PR_UP);
	GPIO_InputConfig(LOG_TX_PORT, LOG_TX_PIN, ENABLE);
}

/*********************************************************************************************************//**
  * @brief  Functions for Output mode.
  * @retval None
  ***********************************************************************************************************/
void Gpio_Toggle_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n)
{
  GPIO_WriteOutBits(HT_GPIOx, GPIO_PIN_n, (FlagStatus)!GPIO_ReadOutBit(HT_GPIOx, GPIO_PIN_n));
}

/*********************************************************************************************************//**
 * @brief   This function handles EXTI interrupt.
 * @retval  None
 ************************************************************************************************************/
//void DETECT_5V_IRQHandler(void)
//{
//	if(EXTI_GetEdgeFlag(DETECT_5V_EXTI_CHANNEL))
//  {
//    EXTI_ClearEdgeFlag(DETECT_5V_EXTI_CHANNEL);
//		gBtnState = 1;

//  }
//}
