/*
 * gpioUser.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 * 
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef GPIOUSER_H
#define GPIOUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define LED_ON 				RESET
#define LED_OFF 			SET

#define CELL_VBAT_ON  SET
#define CELL_VBAT_OFF RESET
#define CELL_PWR_ON  	SET
#define CELL_PWR_OFF 	RESET

#define GNSS_PWR_ON  	SET
#define GNSS_PWR_OFF 	RESET
/***************************  DEBUG_LED  ***************************************//**/
#define DEBUG_LED_GPIOX        	D
#define DEBUG_LED_GPION        	1

#define DEBUG_LED_BIT_CLK      	STRCAT2(P,         				DEBUG_LED_GPIOX)
#define DEBUG_LED_PORTNUM  			STRCAT2(GPIO_P,         	DEBUG_LED_GPIOX)
#define DEBUG_LED_PORT     			STRCAT2(HT_GPIO,        	DEBUG_LED_GPIOX)
#define DEBUG_LED_AFIO_PIN     	STRCAT2(AFIO_PIN_,      	DEBUG_LED_GPION)
#define DEBUG_LED_PIN      			STRCAT2(GPIO_PIN_,      	DEBUG_LED_GPION)	

/***************************  CELL_LED  ***************************************//**/
#define CELL_LED_GPIOX        	D
#define CELL_LED_GPION        	2

#define CELL_LED_BIT_CLK      	STRCAT2(P,         				CELL_LED_GPIOX)
#define CELL_LED_PORTNUM  			STRCAT2(GPIO_P,         	CELL_LED_GPIOX)
#define CELL_LED_PORT     			STRCAT2(HT_GPIO,        	CELL_LED_GPIOX)
#define CELL_LED_AFIO_PIN     	STRCAT2(AFIO_PIN_,      	CELL_LED_GPION)
#define CELL_LED_PIN      			STRCAT2(GPIO_PIN_,      	CELL_LED_GPION)	

/***************************  GNSS_LED  ***************************************//**/
#define GNSS_LED_GPIOX        	D
#define GNSS_LED_GPION        	3

#define GNSS_LED_BIT_CLK      	STRCAT2(P,         				GNSS_LED_GPIOX)
#define GNSS_LED_PORTNUM  			STRCAT2(GPIO_P,         	GNSS_LED_GPIOX)
#define GNSS_LED_PORT     			STRCAT2(HT_GPIO,        	GNSS_LED_GPIOX)
#define GNSS_LED_AFIO_PIN     	STRCAT2(AFIO_PIN_,      	GNSS_LED_GPION)
#define GNSS_LED_PIN      			STRCAT2(GPIO_PIN_,      	GNSS_LED_GPION)	

/***************************  ALERT  ***************************************//**/
#define ALERT_GPIOX        			C
#define ALERT_GPION        			1

#define ALERT_BIT_CLK      			STRCAT2(P,         				ALERT_GPIOX)
#define ALERT_PORTNUM  					STRCAT2(GPIO_P,         	ALERT_GPIOX)
#define ALERT_PORT     					STRCAT2(HT_GPIO,        	ALERT_GPIOX)
#define ALERT_AFIO_PIN     			STRCAT2(AFIO_PIN_,      	ALERT_GPION)
#define ALERT_PIN      					STRCAT2(GPIO_PIN_,      	ALERT_GPION)

/***************************  CELL_VBAT_ON  ***************************************//**/
#define CELL_VBAT_ON_GPIOX        	B
#define CELL_VBAT_ON_GPION        	12

#define CELL_VBAT_ON_BIT_CLK      	STRCAT2(P,         				CELL_VBAT_ON_GPIOX)
#define CELL_VBAT_ON_PORTNUM  			STRCAT2(GPIO_P,         	CELL_VBAT_ON_GPIOX)
#define CELL_VBAT_ON_PORT     			STRCAT2(HT_GPIO,        	CELL_VBAT_ON_GPIOX)
#define CELL_VBAT_ON_AFIO_PIN     	STRCAT2(AFIO_PIN_,      	CELL_VBAT_ON_GPION)
#define CELL_VBAT_ON_PIN      			STRCAT2(GPIO_PIN_,      	CELL_VBAT_ON_GPION)

/***************************  CELL_PWR_ON ***************************************//**/
#define CELL_PWR_ON_GPIOX       D
#define CELL_PWR_ON_GPION       0

#define CELL_PWR_ON_BIT_CLK     STRCAT2(P,         				CELL_PWR_ON_GPIOX)
#define CELL_PWR_ON_PORTNUM  		STRCAT2(GPIO_P,         	CELL_PWR_ON_GPIOX)
#define CELL_PWR_ON_PORT     		STRCAT2(HT_GPIO,        	CELL_PWR_ON_GPIOX)
#define CELL_PWR_ON_AFIO_PIN    STRCAT2(AFIO_PIN_,      	CELL_PWR_ON_GPION)
#define CELL_PWR_ON_PIN      		STRCAT2(GPIO_PIN_,      	CELL_PWR_ON_GPION)

/***************************  CELL_DTR ***************************************//**/
#define CELL_DTR_GPIOX       		A
#define CELL_DTR_GPION       		11

#define CELL_DTR_BIT_CLK     		STRCAT2(P,         				CELL_DTR_GPIOX)
#define CELL_DTR_PORTNUM  			STRCAT2(GPIO_P,         	CELL_DTR_GPIOX)
#define CELL_DTR_PORT     			STRCAT2(HT_GPIO,        	CELL_DTR_GPIOX)
#define CELL_DTR_AFIO_PIN    		STRCAT2(AFIO_PIN_,      	CELL_DTR_GPION)
#define CELL_DTR_PIN      			STRCAT2(GPIO_PIN_,      	CELL_DTR_GPION)

/***************************  CELL_RI ***************************************//**/
#define CELL_RI_GPIOX       		A
#define CELL_RI_GPION       		14

#define CELL_RI_BIT_CLK     		STRCAT2(P,         				CELL_RI_GPIOX)
#define CELL_RI_PORTNUM  				STRCAT2(GPIO_P,         	CELL_RI_GPIOX)
#define CELL_RI_PORT     				STRCAT2(HT_GPIO,        	CELL_RI_GPIOX)
#define CELL_RI_AFIO_PIN    		STRCAT2(AFIO_PIN_,      	CELL_RI_GPION)
#define CELL_RI_PIN      				STRCAT2(GPIO_PIN_,      	CELL_RI_GPION)

#define CELL_RI_AFIO_EXTI_CH 		STRCAT2(AFIO_EXTI_CH_,  CELL_RI_GPION) 
#define CELL_RI_EXTIO_SOURCE 		STRCAT2(AFIO_ESS_P,     CELL_RI_GPIOX)
#define CELL_RI_EXTI_CHANNEL 		STRCAT2(EXTI_CHANNEL_,  CELL_RI_GPION) 
#define CELL_RI_EXTI_IRQn    		STRCAT3(EXTI, CELL_RI_GPION, _IRQn)
#if(CELL_RI_GPION <= 1)
//	#define CELL_RI_IRQHandler   	EXTI0_1_IRQHandler
#elif(CELL_RI_GPION <= 3)
//	#define CELL_RI_IRQHandler   	EXTI2_3_IRQHandler
#else
//	#define CELL_RI_IRQHandler   	EXTI4_15_IRQHandler
#endif

/***************************  CELL_STT ***************************************//**/
#define CELL_STT_GPIOX       		A
#define CELL_STT_GPION       		15

#define CELL_STT_BIT_CLK     		STRCAT2(P,         				CELL_STT_GPIOX)
#define CELL_STT_PORTNUM  			STRCAT2(GPIO_P,         	CELL_STT_GPIOX)
#define CELL_STT_PORT     			STRCAT2(HT_GPIO,        	CELL_STT_GPIOX)
#define CELL_STT_AFIO_PIN    		STRCAT2(AFIO_PIN_,      	CELL_STT_GPION)
#define CELL_STT_PIN      			STRCAT2(GPIO_PIN_,      	CELL_STT_GPION)

/***************************  GNSS_PWR ***************************************//**/
#define GNSS_PWR_GPIOX       		C
#define GNSS_PWR_GPION       		7

#define GNSS_PWR_BIT_CLK     		STRCAT2(P,         				GNSS_PWR_GPIOX)
#define GNSS_PWR_PORTNUM  			STRCAT2(GPIO_P,         	GNSS_PWR_GPIOX)
#define GNSS_PWR_PORT     			STRCAT2(HT_GPIO,        	GNSS_PWR_GPIOX)
#define GNSS_PWR_AFIO_PIN    		STRCAT2(AFIO_PIN_,      	GNSS_PWR_GPION)
#define GNSS_PWR_PIN      			STRCAT2(GPIO_PIN_,      	GNSS_PWR_GPION)

/***************************  GNSS_PPS ***************************************//**/
#define GNSS_PPS_GPIOX       		C
#define GNSS_PPS_GPION       		11

#define GNSS_PPS_BIT_CLK     		STRCAT2(P,         				GNSS_PPS_GPIOX)
#define GNSS_PPS_PORTNUM  			STRCAT2(GPIO_P,         	GNSS_PPS_GPIOX)
#define GNSS_PPS_PORT     			STRCAT2(HT_GPIO,        	GNSS_PPS_GPIOX)
#define GNSS_PPS_AFIO_PIN    		STRCAT2(AFIO_PIN_,      	GNSS_PPS_GPION)
#define GNSS_PPS_PIN      			STRCAT2(GPIO_PIN_,      	GNSS_PPS_GPION)

#define GNSS_PPS_AFIO_EXTI_CH 	STRCAT2(AFIO_EXTI_CH_,  GNSS_PPS_GPION) 
#define GNSS_PPS_EXTIO_SOURCE 	STRCAT2(AFIO_ESS_P,     GNSS_PPS_GPIOX)
#define GNSS_PPS_EXTI_CHANNEL 	STRCAT2(EXTI_CHANNEL_,  GNSS_PPS_GPION) 
#define GNSS_PPS_EXTI_IRQn    	STRCAT3(EXTI, GNSS_PPS_GPION, _IRQn)
#if(GNSS_PPS_GPION <= 1)
//	#define GNSS_PPS_IRQHandler 	EXTI0_1_IRQHandler
#elif(GNSS_PPS_GPION <= 3)
//	#define GNSS_PPS_IRQHandler 	EXTI2_3_IRQHandler
#else
//	#define GNSS_PPS_IRQHandler 	EXTI4_15_IRQHandler
#endif

/***************************  ACC_INT ***************************************//**/
#define ACC_INT_GPIOX       		A
#define ACC_INT_GPION       		2

#define ACC_INT_BIT_CLK     		STRCAT2(P,         				ACC_INT_GPIOX)
#define ACC_INT_PORTNUM  				STRCAT2(GPIO_P,         	ACC_INT_GPIOX)
#define ACC_INT_PORT     				STRCAT2(HT_GPIO,        	ACC_INT_GPIOX)
#define ACC_INT_AFIO_PIN    		STRCAT2(AFIO_PIN_,      	ACC_INT_GPION)
#define ACC_INT_PIN      				STRCAT2(GPIO_PIN_,      	ACC_INT_GPION)

#define ACC_INT_AFIO_EXTI_CH 		STRCAT2(AFIO_EXTI_CH_,  ACC_INT_GPION) 
#define ACC_INT_EXTIO_SOURCE 		STRCAT2(AFIO_ESS_P,     ACC_INT_GPIOX)
#define ACC_INT_EXTI_CHANNEL 		STRCAT2(EXTI_CHANNEL_,  ACC_INT_GPION) 
#define ACC_INT_EXTI_IRQn    		STRCAT3(EXTI, ACC_INT_GPION, _IRQn)
#if(ACC_INT_GPION <= 1)
//	#define ACC_INT_IRQHandler 		EXTI0_1_IRQHandler
#elif(ACC_INT_GPION <= 3)
//	#define ACC_INT_IRQHandler 		EXTI2_3_IRQHandler
#else
//	#define ACC_INT_IRQHandler 		EXTI4_15_IRQHandler
#endif

/***************************  NFC_INT ***************************************//**/
#define NFC_INT_GPIOX       		C
#define NFC_INT_GPION       		15

#define NFC_INT_BIT_CLK     		STRCAT2(P,         				NFC_INT_GPIOX)
#define NFC_INT_PORTNUM  				STRCAT2(GPIO_P,         	NFC_INT_GPIOX)
#define NFC_INT_PORT     				STRCAT2(HT_GPIO,        	NFC_INT_GPIOX)
#define NFC_INT_AFIO_PIN    		STRCAT2(AFIO_PIN_,      	NFC_INT_GPION)
#define NFC_INT_PIN      				STRCAT2(GPIO_PIN_,      	NFC_INT_GPION)

#define NFC_INT_AFIO_EXTI_CH 		STRCAT2(AFIO_EXTI_CH_,  NFC_INT_GPION) 
#define NFC_INT_EXTIO_SOURCE 		STRCAT2(AFIO_ESS_P,     NFC_INT_GPIOX)
#define NFC_INT_EXTI_CHANNEL 		STRCAT2(EXTI_CHANNEL_,  NFC_INT_GPION) 
#define NFC_INT_EXTI_IRQn    		STRCAT3(EXTI, NFC_INT_GPION, _IRQn)
#if(NFC_INT_GPION <= 1)
//	#define NFC_INT_IRQHandler 		EXTI0_1_IRQHandler
#elif(NFC_INT_GPION <= 3)
//	#define NFC_INT_IRQHandler 		EXTI2_3_IRQHandler
#else
//	#define NFC_INT_IRQHandler 		EXTI4_15_IRQHandler
#endif

/***************************  NFC_RST ***************************************//**/
#define NFC_RST_GPIOX       		C
#define NFC_RST_GPION       		14

#define NFC_RST_BIT_CLK     		STRCAT2(P,         				NFC_RST_GPIOX)
#define NFC_RST_PORTNUM  				STRCAT2(GPIO_P,         	NFC_RST_GPIOX)
#define NFC_RST_PORT     				STRCAT2(HT_GPIO,        	NFC_RST_GPIOX)
#define NFC_RST_AFIO_PIN    		STRCAT2(AFIO_PIN_,      	NFC_RST_GPION)
#define NFC_RST_PIN      				STRCAT2(GPIO_PIN_,      	NFC_RST_GPION)

/***************************  LOG_TX ***************************************//**/
#define LOG_TX_GPIOX       		B
#define LOG_TX_GPION       		6

#define LOG_TX_BIT_CLK     		STRCAT2(P,         				LOG_TX_GPIOX)
#define LOG_TX_PORTNUM  			STRCAT2(GPIO_P,         	LOG_TX_GPIOX)
#define LOG_TX_PORT     			STRCAT2(HT_GPIO,        	LOG_TX_GPIOX)
#define LOG_TX_AFIO_PIN    		STRCAT2(AFIO_PIN_,      	LOG_TX_GPION)
#define LOG_TX_PIN      			STRCAT2(GPIO_PIN_,      	LOG_TX_GPION)
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */
extern volatile uint8_t gBtnState;

/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
/*********************************************************************************************************//**
  * @brief  Configure the GPIO ports in/out
  * @retval None
  ***********************************************************************************************************/
void Gpio_Config(void);
	
/*********************************************************************************************************//**
  * @brief  Functions for Input mode.
  * @retval None
  ***********************************************************************************************************/
//GPIO_ReadInBit

/*********************************************************************************************************//**
  * @brief  Functions for Output mode.
  * @retval None
  ***********************************************************************************************************/
void Gpio_Toggle_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n);
#endif
#ifdef __cplusplus
}
#endif
