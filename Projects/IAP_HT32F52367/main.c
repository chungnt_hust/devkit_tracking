/*********************************************************************************************************//**
 * @file    CKCU/Clock_Configuration/main.c
 * @version $Rev:: 4434         $
 * @date    $Date:: 2019-12-20 #$
 * @brief   Main program.
 *************************************************************************************************************
 * @attention
 *
 * Firmware Disclaimer Information
 *
 * 1. The customer hereby acknowledges and agrees that the program technical documentation, including the
 *    code, which is supplied by Holtek Semiconductor Inc., (hereinafter referred to as "HOLTEK") is the
 *    proprietary and confidential intellectual property of HOLTEK, and is protected by copyright law and
 *    other intellectual property laws.
 *
 * 2. The customer hereby acknowledges and agrees that the program technical documentation, including the
 *    code, is confidential information belonging to HOLTEK, and must not be disclosed to any third parties
 *    other than HOLTEK and the customer.
 *
 * 3. The program technical documentation, including the code, is provided "as is" and for customer reference
 *    only. After delivery by HOLTEK, the customer shall use the program technical documentation, including
 *    the code, at their own risk. HOLTEK disclaims any expressed, implied or statutory warranties, including
 *    the warranties of merchantability, satisfactory quality and fitness for a particular purpose.
 *
 * <h2><center>Copyright (C) Holtek Semiconductor Inc. All rights reserved</center></h2>
 ************************************************************************************************************/

/* Includes ------------------------------------------------------------------------------------------------*/
#include "ht32.h"
#include "ht32_board.h"
#include "ht32_board_config.h"
#include "systemClockCfg.h"
#include "uartUser.h"
#include "internalFlash.h"
#include "basicTimer.h"
#include "gpioUser.h"
#include "spiUser.h"
#include "EN25Qxxx.h"
/** @addtogroup HT32_Series_Peripheral_Examples HT32 Peripheral Examples
  * @{
  */

/** @addtogroup CKCU_Examples CKCU
  * @{
  */

/** @addtogroup Clock_Configuration
  * @{
  */

/* Private constants ---------------------------------------------------------------------------------------*/
#define IAP_ADDRESS (0)
#define AP_ADDRESS  (0x2000)

#if defined (__CC_ARM)
/*********************************************************************************************************//**
  * @brief  Jump to user application by change PC.
  * @param  address: Start address of user application
  * @retval None
  ***********************************************************************************************************/
__asm void IAP_GoCMD(u32 address)
{
  LDR R1, [R0]
  MOV SP, R1
  LDR R1, [R0, #4]
  BX R1
}
#elif defined (__ICCARM__)
void IAP_GoCMD(u32 address)
{
  __asm("LDR R1, [R0]");
  __asm("MOV SP, R1");
  __asm("LDR R1, [R0, #4]");
  __asm("BX R1");
}
#endif

/* Private function prototypes -----------------------------------------------------------------------------*/
static void systickInterrupt(void);
static void Hdl_JumpToNewAP(uint32_t add);
/* Private variables ---------------------------------------------------------------------------------------*/
static uint16_t softTimer = 0;
static uint8_t count = 0;
/* Global functions ----------------------------------------------------------------------------------------*/

int main(void)
{	
	NVIC_SetVectorTable(NVIC_VECTTABLE_FLASH, IAP_ADDRESS);
	
	SysClockCfg_RegCallback(systickInterrupt);
	UART_USART_Cfg(USART0_INDEX, 115200, USART_PARITY_NO, USART_WORDLENGTH_8B, USART_STOPBITS_1);
	UART_USART_Cfg(USART1_INDEX, 300, USART_PARITY_EVEN, USART_WORDLENGTH_7B, USART_STOPBITS_1);
	UART_USART_Cfg(UART1_INDEX, 921600, USART_PARITY_NO, USART_WORDLENGTH_8B, USART_STOPBITS_1);
	SysClockGetCK_AHB();
	DEBUG("\r\n---------------IAP---------------------\r\n");
	DEBUG("RST: %s\r\n", SystemClockCfg_GetResetReason());
	
	BasicTimer_Configuration();
	
	Gpio_Interrupt_Configuration();
	Gpio_Out_Configuration();
	
	spiUser_Configuration(SPI1_INDEX, SPI_DATALENGTH_8, 3);
	
	SPI1_SEL_ACTIVE;
	spiUser_SendReceive(SPI1_INDEX, 'C');
	spiUser_SendReceive(SPI1_INDEX, 'h');
	spiUser_SendReceive(SPI1_INDEX, 'u');
	spiUser_SendReceive(SPI1_INDEX, 'n');
	spiUser_SendReceive(SPI1_INDEX, 'g');
	SPI1_SEL_INACTIVE;
	
	WDT_Configuration(4000);
  while (1)
  {    
    if(softTimer >= 1000)
    {
      softTimer = 0;
			WDT_Restart();
			DEBUG("+\r\n");
			if(++count == 5)
			{
				// Jump to AP
				Hdl_JumpToNewAP(AP_ADDRESS);
			}
    }	
  }
}

/*********************************************************************************************************//**
  * @brief  sysTickCallback
  * @retval None
  ***********************************************************************************************************/
static void systickInterrupt(void)
{
  softTimer++;
	if(timeDelay > 0) timeDelay--;
}

/*********************************************************************************************************//**
 * @brief  Hdl_JumpToNewAP.
 * @retval None
 ***********************************************************************************************************/
static void Hdl_JumpToNewAP(uint32_t add)
{
	DEBUG("Jump: add [%d]\r\n", add);
	
	u8 tx = 0xAA;
	InternalFlash_write(254, (u8*)&tx, sizeof(tx));
	DEBUG("TX = %02x\r\n", tx);
	
  NVIC_DisableIRQ(USART0_IRQn);
	USART_DeInit(USART0_PORT);     
	
	NVIC_DisableIRQ(USART1_IRQn);
	USART_DeInit(USART1_PORT);     
	
	NVIC_DisableIRQ(UART1_IRQn);
	USART_DeInit(UART1_PORT);     
	
	NVIC_DisableIRQ(BFTM0_IRQn);
	BFTM_DeInit(HT_BFTM0);  
	
	NVIC_DisableIRQ(BFTM1_IRQn);
	BFTM_DeInit(HT_BFTM1); 
	
	NVIC_DisableIRQ(DETECT_5V_EXTI_IRQn);
	EXTI_DeInit(DETECT_5V_EXTI_CHANNEL);
	
	NVIC_DisableIRQ(SPI1_IRQn);
	SPI_DeInit(SPI1_PORT);
	
  NVIC_SetVectorTable(NVIC_VECTTABLE_FLASH, add); /* Set Vector Table Offset                  */
  IAP_GoCMD(add);
}
#if (HT32_LIB_DEBUG == 1)
/*********************************************************************************************************//**
  * @brief  Report both the error name of the source file and the source line number.
  * @param  filename: pointer to the source file name.
  * @param  uline: error line source number.
  * @retval None
  ***********************************************************************************************************/
void assert_error(u8* filename, u32 uline)
{
  /*
     This function is called by IP library that the invalid parameters has been passed to the library API.
     Debug message can be added here.
     Example: printf("Parameter Error: file %s on line %d\r\n", filename, uline);
  */

  while (1)
  {
  }
}
#endif

/* Private functions ---------------------------------------------------------------------------------------*/

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
